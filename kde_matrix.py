# -*- coding: utf-8 -*-
"""
Created on Tue Jun 24 13:01:38 2014

@author: prconrad
"""

import numpy as np
import plotTools


def kde_matrix(all_data, alpha=0.5, figsize=None, ax=None, grid=False,
                   diagonal='hist', marker='.', density_kwds=None,
                   hist_kwds=None, range_padding=0.1, labels=[], label_separation_x=.5, contour_labels=[],
                 ax_bottom = .13, ax_left=.1,boundaries_list=[], usescatter=True, kde_subsample=1000, scatter_subsample=100,
                 ylabel_offset=-.6, **kwds):
    """
    Draw a matrix of scatter plots.

    Parameters
    ----------
    frame : DataFrame
    alpha : float, optional
        amount of transparency applied
    figsize : (float,float), optional
        a tuple (width, height) in inches
    ax : Matplotlib axis object, optional
    grid : bool, optional
        setting this to True will show the grid
    diagonal : {'hist', 'kde'}
        pick between 'kde' and 'hist' for
        either Kernel Density Estimation or Histogram
        plot in the diagonal
    marker : str, optional
        Matplotlib marker type, default '.'    
    hist_kwds : other plotting keyword arguments
        To be passed to hist function
    density_kwds : other plotting keyword arguments
        To be passed to kernel density estimate plot
    range_padding : float, optional
        relative extension of axis range in x and y
        with respect to (x_max - x_min) or (y_max - y_min),
        default 0.05
    kwds : other plotting keyword arguments
        To be passed to scatter function

    Examples
    --------
    >>> df = DataFrame(np.random.randn(1000, 4), columns=['A','B','C','D'])
    >>> scatter_matrix(df, alpha=0.2)
    """
    import matplotlib.pyplot as plt
    from matplotlib.artist import setp
    import matplotlib

    numContours = len(all_data)

    df = all_data[0].T
    n = df.shape[1]
    numSamples = df.shape[0]
#    print shape(df)
    if labels == []:
        for i in range(n):
            labels.append(str(i))
        
        
    fig, axes = _subplots(nrows=n, ncols=n, figsize=figsize, ax=ax,
                          squeeze=False, usescatter=usescatter)

    # no gaps between subplots
    hspace = 0.02
    fig.subplots_adjust(wspace=0.02, hspace=hspace, bottom=ax_bottom, left=ax_left)




    hist_kwds = hist_kwds or {}
    density_kwds = density_kwds or {}

    # workaround because `c='b'` is hardcoded in matplotlibs scatter method
    kwds.setdefault('c', plt.rcParams['patch.facecolor'])


    if boundaries_list == []:

        for a in range(n):
            rmin_ = float('inf')
            rmax_ = -float('inf')
            for i in range(numContours):
                df = all_data[i].T
                values = df[:,a]
                rmin_, rmax_ = min(rmin_, np.min(values)), max(rmax_, np.max(values))
            rdelta_ext = (rmax_ - rmin_) * range_padding / 2.
            boundaries_list.append((rmin_ - rdelta_ext, rmax_+ rdelta_ext))

    colorsamples = []
#    print boundaries_list
    for k in range(numContours):
        df = all_data[k].T
        numSamples = df.shape[0]


        for j in range(n):
            for i in range(n):

                ax = axes[i,j]

                if ax == []:
                    continue
                print 'Subplot: ', str(i), ', ', str(j)

    #            ax.text(.5,.5, str(i) + ',' + str(j))

                if i == j:
                    values =  df[::kde_subsample,i]

                    from scipy.stats import gaussian_kde
                    y = values
                    gkde = gaussian_kde(y)
                    ind = np.linspace(boundaries_list[i][0], boundaries_list[i][1], 1000)
                    acolorsample= ax.plot(ind, gkde.evaluate(ind), color=plotTools.ColormapBaseColor(k), **density_kwds)


                    ax.set_xlim(boundaries_list[i])

                    if i == 0 and not usescatter:
                        colorsamples.append(acolorsample)
                        ax.legend(contour_labels, loc='best')
                        if k==numContours-1:
                            # ax = axes[0,1]
                            # ax = fig.add_subplot(numContours, numContours, 1)
                            ax.legend(contour_labels, loc='upper left', bbox_to_anchor=(2.3,1))
                            # ax.legend(contour_labels)

                # contour below the axis
                elif i > j:
                    y=np.zeros((numSamples, 2))
                    y[:,0]=df[:,j]
                    y[:,1]=df[:,i]

                    from scipy.stats import gaussian_kde
                    gkde = gaussian_kde(y[::kde_subsample,:].T)

                    gridSize = 500
                    X,Y = np.mgrid[boundaries_list[j][0]:boundaries_list[j][1]:gridSize*1j, boundaries_list[i][0]:boundaries_list[i][1]:gridSize*1j]

                    positions = np.vstack([X.ravel(), Y.ravel()])

                    Z = gkde(positions)
                    Z = Z.reshape(gridSize, gridSize)

                    ax.set_xlim(boundaries_list[j])
                    ax.set_ylim(boundaries_list[i])
                    mycmap = plotTools.Colormap(k)

                    ax.contour(np.array(X),np.array(Y),np.array(Z), cmap=mycmap,  rasterized=True)
    #                ax.scatter(y[:,0], y[:,1])

                #     scatter above the axis
                else:
                    y=np.zeros((numSamples, 2))
                    y[:,0]=df[:,j]
                    y[:,1]=df[:,i]



                    ax.set_xlim(boundaries_list[j])
                    ax.set_ylim(boundaries_list[i])
                    mycmap = plotTools.Colormap(k)
                    ax.scatter(np.array(y[::scatter_subsample,0]),np.array(y[::scatter_subsample,1]),color=plotTools.ColormapBaseColor(k), s=2, alpha=.05, rasterized=True)


                ax.locator_params(nbins = 4)

                ax.set_xlabel('')
                ax.set_ylabel('')
                if not usescatter:
                    ax.xaxis.set_label_coords(0.5, (n-j)*(1+hspace)+label_separation_x)
    #            ax.yaxis.set_label_coords(-label_separation_y, 0.5)



    #            _label_axis(ax, kind='y', label=labels[i], position='left')
                if not usescatter:

                    _label_axis(ax, kind='x', label=labels[j], position='bottom', rotate=True)
                else:
                    _label_axis(ax, kind='x', label=labels[j], position='bottom', rotate=False)
                    _label_axis(ax, kind='y', label=labels[i], position='left', rotate=False)
                    ax.yaxis.set_label_coords(ylabel_offset, .5)

    #            if True: #(i==0 and j == 0) or (j==i-1):
    #                print 'alabel', labels[i]
    #                ax.set_xlabel(labels[i])
    #                ax.xaxis.set_label_position('top')

                if j!= 0 or (j == 0 and i==0 and not usescatter):
                    ax.yaxis.set_visible(False)
                if i != n-1:
                    ax.xaxis.set_visible(False)

    #            fig.tight_layout()


    #            if True: #(i==0 and j == 0) or (j==i-1):
    #                print 'alabel', labels[i]
    #                ax.set_xlabel(labels[i])
    #                ax.xaxis.set_label_position('top')
    #                ax.tick_params(labelbottom='off', labeltop='on')

    #    for ax in axes.flat:
    #        setp(ax.get_xticklabels(), fontsize=8)
    #        setp(ax.get_yticklabels(), fontsize=8)

    return axes
    


def _subplots(nrows=1, ncols=1, sharex=False, sharey=False, squeeze=True,
              subplot_kw=None, ax=None, secondary_y=False, data=None,usescatter=False,
              **fig_kw):
    """Create a figure with a set of subplots already made.

    This utility wrapper makes it convenient to create common layouts of
    subplots, including the enclosing figure object, in a single call.

    Keyword arguments:

    nrows : int
      Number of rows of the subplot grid.  Defaults to 1.

    ncols : int
      Number of columns of the subplot grid.  Defaults to 1.

    sharex : bool
      If True, the X axis will be shared amongst all subplots.

    sharey : bool
      If True, the Y axis will be shared amongst all subplots.

    squeeze : bool

      If True, extra dimensions are squeezed out from the returned axis object:
        - if only one subplot is constructed (nrows=ncols=1), the resulting
        single Axis object is returned as a scalar.
        - for Nx1 or 1xN subplots, the returned object is a 1-d numpy object
        array of Axis objects are returned as numpy 1-d arrays.
        - for NxM subplots with N>1 and M>1 are returned as a 2d array.

      If False, no squeezing at all is done: the returned axis object is always
      a 2-d array containing Axis instances, even if it ends up being 1x1.

    subplot_kw : dict
      Dict with keywords passed to the add_subplot() call used to create each
      subplots.

    ax : Matplotlib axis object, optional

    secondary_y : boolean or sequence of ints, default False
        If True then y-axis will be on the right

    data : DataFrame, optional
        If secondary_y is a sequence, data is used to select columns.

    fig_kw : Other keyword arguments to be passed to the figure() call.
        Note that all keywords not recognized above will be
        automatically included here.


    Returns:

    fig, ax : tuple
      - fig is the Matplotlib Figure object
      - ax can be either a single axis object or an array of axis objects if
      more than one subplot was created.  The dimensions of the resulting array
      can be controlled with the squeeze keyword, see above.

    **Examples:**

    x = np.linspace(0, 2*np.pi, 400)
    y = np.sin(x**2)

    # Just a figure and one subplot
    f, ax = plt.subplots()
    ax.plot(x, y)
    ax.set_title('Simple plot')

    # Two subplots, unpack the output array immediately
    f, (ax1, ax2) = plt.subplots(1, 2, sharey=True)
    ax1.plot(x, y)
    ax1.set_title('Sharing Y axis')
    ax2.scatter(x, y)

    # Four polar axes
    plt.subplots(2, 2, subplot_kw=dict(polar=True))
    """
    import matplotlib.pyplot as plt
    from pandas.core.frame import DataFrame

    if subplot_kw is None:
        subplot_kw = {}

    if ax is None:
        fig = plt.figure(**fig_kw)
    else:
        fig = ax.get_figure()
        fig.clear()
        
    usePlot = np.zeros((nrows, ncols))
    for i in range(nrows):
        for j in range(ncols):
            if (j< i or j == ncols-1) or usescatter:
                usePlot[i,j] = i*10+j+1
            else:
                usePlot[i,j] = 0
#            usePlot[i,j] = i*10+j

    usePlot = usePlot.reshape(nrows*ncols)
    # Create empty object array to hold all axes.  It's easiest to make it 1-d
    # so we can just append subplots upon creation, and then
    nplots = nrows * ncols
    axarr = np.empty(nplots, dtype=object)

    def on_right(i):
        if isinstance(secondary_y, bool):
            return secondary_y
        if isinstance(data, DataFrame):
            return data.columns[i] in secondary_y

    # Create first subplot separately, so we can share it if requested
    ax0 = fig.add_subplot(nrows, ncols, 1, **subplot_kw)
    if on_right(0):
        orig_ax = ax0
        ax0 = ax0.twinx()
        ax0._get_lines.color_cycle = orig_ax._get_lines.color_cycle

        orig_ax.get_yaxis().set_visible(False)
        orig_ax.right_ax = ax0
        ax0.left_ax = orig_ax

    if sharex:
        subplot_kw['sharex'] = ax0
    if sharey:
        subplot_kw['sharey'] = ax0
    axarr[0] = ax0

    # Note off-by-one counting because add_subplot uses the MATLAB 1-based
    # convention.
    import itertools
    for i, use in itertools.izip(range(1, nplots), usePlot):
        if use >0:
            ax = fig.add_subplot(nrows, ncols, i + 1, **subplot_kw)
#            ax.text(.5,.5, str(use))
            if on_right(i):
                orig_ax = ax
                ax = ax.twinx()
                ax._get_lines.color_cycle = orig_ax._get_lines.color_cycle
    
                orig_ax.get_yaxis().set_visible(False)
            axarr[i] = ax
        else:
            axarr[i] = []

    if nplots > 1:
        if sharex and nrows > 1:
            for i, ax in enumerate(axarr):
                if np.ceil(float(i + 1) / ncols) < nrows:  # only last row
                    [label.set_visible(
                        False) for label in ax.get_xticklabels()]
        if sharey and ncols > 1:
            for i, ax in enumerate(axarr):
                if (i % ncols) != 0:  # only first column
                    [label.set_visible(
                        False) for label in ax.get_yticklabels()]

    if squeeze:
        # Reshape the array to have the final desired dimension (nrow,ncol),
        # though discarding unneeded dimensions that equal 1.  If we only have
        # one subplot, just return it instead of a 1-element array.
        if nplots == 1:
            axes = axarr[0]
        else:
            axes = axarr.reshape(nrows, ncols).squeeze()
    else:
        # returned axis array will be always 2-d, even if nrows=ncols=1
        axes = axarr.reshape(nrows, ncols)

    return fig, axes


def _label_axis(ax, kind='x', label='', position='top',
    ticks=True, rotate=False):

    from matplotlib.artist import setp
    if kind == 'x':
        ax.set_xlabel(label, visible=True)
        ax.xaxis.set_visible(True)
        ax.xaxis.set_ticks_position(position)
        ax.xaxis.set_label_position(position)
        if rotate:
            setp(ax.get_xticklabels(), rotation=90)
    elif kind == 'y':
        ax.yaxis.set_visible(True)
        ax.set_ylabel(label, visible=True)
        # ax.set_ylabel(a)
        ax.yaxis.set_ticks_position(position)
        ax.yaxis.set_label_position(position)
    return
#    
#import h5py
#f = h5py.File('results/rosenbrock_mixing.h5')
#data = f['rosenbrock_none_0/chain'][:,::1]
##kde_matrix(data)
#
#import plotTools
#plotTools.MakeFigure(500,.5)
#f = h5py.File('voyager_results/genetics.h5')
#data = f['genetics_none_0/chain'][:,::5000]
#kde_matrix(data)
#
#
##plt.figure()
#from pandas.tools.plotting import scatter_matrix
#from pandas import DataFrame
#df = DataFrame(data.T)
#
#scatter_matrix(df, alpha=0.2, figsize=(10,10))