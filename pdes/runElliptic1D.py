import sys

sys.path.append('/Users/patrick/muq_install/lib')
sys.path.append('/Users/patrick/Dropbox/Development/examples/build')
sys.path.append('/Users/patrick/Dropbox/Development/examples/pdes/build')
sys.path.append('/Users/patrick/Dropbox/Development/examples')

# sys.path.append('/home/prconrad/local/lib')
# sys.path.append('/home/prconrad/local/muq_external/lib')
# sys.path.append('/media/extradata/Dropbox/Development/examples/odes/build')

import libmuqModelling

import libmuqInference

import libmuqUtilities
import libmuqApproximation
import libmuqPde
import libRandomPdes
import numpy as np
import matplotlib.pyplot as plt

#import plotTools

from mpi4py import MPI

def MakeGraph(noiseScale = 0, gridSize=20, plotRef=True, solutionColor = "blue", order=1):

    graph = libmuqModelling.ModGraph()

    paramPde = dict()

    paramPde["mesh.type"] = "LineMesh"
    paramPde["mesh.N"]   = gridSize
    paramPde["mesh.L"]   = 1.0
    paramPde["mesh.B"]   = 0.0


    # paramPde["mesh.type"] = "SquareMesh"
    # paramPde["mesh.Nx"]   = 15
    # paramPde["mesh.Ny"]   = 15
    # paramPde["mesh.Lx"]   = 1.0
    # paramPde["mesh.Ly"]   = 1.0


    system = libmuqPde.GenericEquationSystems(paramPde)

    paramPde["PDE.Unknowns"]                = "u"
    paramPde["PDE.UnknownOrders"]           = str(order)
    paramPde["PDE.GaussianQuadratureOrder"] = 2
    paramPde["PDE.Name"]                    = "PoissonExample"
    paramPde["PDE.Parameters"]              = "DiffusionParam,SpatialForcingExample"
    paramPde["PDE.IsScalarParameter"]       = "false,false"

    paramPde["PDE.NoiseScale"] = 1.0/gridSize*noiseScale;

    paramPde["KINSOL.LinearSolver"] = "Dense"


    paramPde["MeshParameter.SystemName"]     = "SpatialForcingExample"
    paramPde["MeshParameter.VariableNames"]  = "forcing"
    paramPde["MeshParameter.VariableOrders"] = "1"
    # paramPde["MeshParameter.InputNames"]     = "pumpingRates"
    # paramPde["MeshParameter.InputSizes"]     = str(4)

    pressureForcingMod = libmuqPde.MeshParameterCpp(system, paramPde)

    paramPde["MeshParameter.SystemName"]     = "PiecewiseKappa"
    paramPde["MeshParameter.VariableNames"]  = "kappa"
    paramPde["MeshParameter.VariableOrders"] = "1"
    paramPde["MeshParameter.InputNames"]     = "coeff"
    paramPde["MeshParameter.InputSizes"]     = str(4)

    kappaMod = libmuqPde.MeshParameterCpp(system, paramPde)


    inputSizes = [kappaMod.outputSize, pressureForcingMod.outputSize]
    pressureMod = libmuqPde.NontransientPDEModPiece(inputSizes, system, paramPde)


    points = [None]*(gridSize+1)
    points1D = [None]*(gridSize+1)
    for i in range(gridSize+1):
        points[i] = [(i+0.)/(gridSize)]
        points1D[i] = (i+0.)/(gridSize)


    sensorMod = libmuqPde.PointSensor(pressureMod.outputSize, points, "PoissonExample", system)


    graph = libmuqModelling.ModGraph()
    # the logKappa model
    graph.AddNode(kappaMod, "kappa")



    # the forcing inputs
    graph.AddNode(pressureForcingMod, "Pressure Forcing")

    # initial guess for solver
    graph.AddNode(libmuqModelling.VectorPassthroughModel(pressureMod.inputSizes[0]), "initial guess pressure")

    # add the PDE to the graph
    graph.AddNode(pressureMod, "Pressure")

#    graph.writeGraphViz("ModGraph_nodes.pdf")

    # connect the PDE's inputs
    graph.AddEdge("initial guess pressure", "Pressure", 0)

    graph.AddEdge("kappa", "Pressure", 1)
    graph.BindNode("initial guess pressure", [1.0]*pressureMod.inputSizes[0])
    graph.AddEdge("Pressure Forcing", "Pressure", 2)


    forcingObs = libmuqPde.PointSensor(pressureForcingMod.outputSize, points, "SpatialForcingExample", system)
    graph.AddNode(forcingObs, "forcingObs")
    graph.AddEdge("Pressure Forcing", "forcingObs",0)

    kappaObs = libmuqPde.PointSensor(kappaMod.outputSize, points, "DiffusionParam", system)

    graph.AddNode(kappaObs, "kappaObs")
    graph.AddEdge("kappa", "kappaObs",0)


    graph.AddNode(sensorMod, "PressureObs")
    graph.AddEdge("Pressure", "PressureObs",0)


    # return graph
    # graph.writeGraphViz("ModGraph.pdf")

    obs = libmuqModelling.ModGraphPiece(graph, "PressureObs")

    pressurePlot =  obs.Evaluate([])

    obs = libmuqModelling.ModGraphPiece(graph, "initial guess pressure")

    print "guess ", obs.Evaluate([])


    obs = libmuqModelling.ModGraphPiece(graph, "forcingObs")

    forcePlot =  obs.Evaluate([])




    obs = libmuqModelling.ModGraphPiece(graph, "kappaObs")

    kappPlot =  obs.Evaluate([])

    import plotTools
    if plotRef:

        plt.plot(points1D, pressurePlot, color=plotTools.PlotColor(solutionColor), linewidth=3)
        plt.plot(points1D,forcePlot,color=plotTools.PlotColor("red"), linewidth=2)
        plt.plot(points1D, kappPlot, color=plotTools.PlotColor("green"), linewidth=2)
    else:
        plt.plot(points1D, pressurePlot, color=plotTools.PlotColor("blue"), linewidth=1, alpha=.2)
    plt.legend(["pressure", "forcing", "kappa"])
    plt.show()




def RunOneForward(runIndex,mcmcLength,stepInt):
    runName = 'Elliptic1D_'



    runName += "forwardScale_"
    runName += str(stepInt)+'_'

    runName  += str(runIndex)

    print "Computing key ", runName

    params = dict()

    finalTime = 40.0



    if stepInt == 1:
        stepSize = 10
    elif stepInt == 2:
        stepSize = 20
    elif stepInt == 3:
        stepSize = 40
    elif stepInt == 4:
        stepSize = 60
    elif stepInt == 5:
        stepSize = 80
    else:
        print "no step size found, exiting!"
        sys.exit();


    # create MCMC instance
    params["MCMC.Steps"]            = mcmcLength-1

    params["MCMC.Method"]           = "SingleChainMCMC"

    params["MCMC.Kernel"] = "PseudomarginalMHKernel"

    params["MCMC.Proposal"] = "AM"

    params["MCMC.BurnIn"]           = 1
    params["MCMC.MHProposal.PropSize"]      = 1e-2
    params["MCMC.DR.stages"]     = 3
    params["MCMC.DR.scale"]     = 10.0

    params["MCMC.AM.AdaptSteps"] = 50
    params["MCMC.AM.AdaptStart"] = 200
    params["Verbose"]               = 5

    params["MCMC.HDF5OutputDetails"] = 1
    params["MCMC.HDF5OutputGroup"] = "/"+runName
    params["HDF5.OutputGroup"] = "/"+runName


    #else, continue by starting up MUQ
    # start logging
#    libmuqUtilities.InitializeLogging(["geneticsPython"])
    # open an hdf5



    startPoint = [-1]



    # worseGraph = MakeGraph(noiseScale = 1, gridSize=20,  order=1)
    # betterGraph = MakeGraph(noiseScale = 0, gridSize=20,  order=2)



    samplingProblem = libRandomPdes.Elliptic1DForwardScaleProblem(stepSize)



    mcmc = libmuqInference.MCMCBase(samplingProblem, params)
    mcmc.Sample(startPoint)




def RunOneInv(runIndex,mcmcLength,stepInt, noise):
    runName = 'Elliptic1D_'



    runName += "inv_"
    if(noise):
        runName += "rand_"
    else:
        runName += "det_"

    runName += str(stepInt)+'_'

    runName  += str(runIndex)


    print "Computing key ", runName

    params = dict()


    if stepInt == 1:
        stepSize = 10
        noiseVal = pow(10.0, .35)
    elif stepInt == 2:
        stepSize = 20
        noiseVal = pow(10.0, .42)
    elif stepInt == 3:
        stepSize = 40
        noiseVal = pow(10.0, .51)
    elif stepInt == 4:
        stepSize = 60
        noiseVal = pow(10.0, .64)
    elif stepInt == 5:
        stepSize = 80
        noiseVal = pow(10.0, .68)
    else:
        print "no step size found, exiting!"
        sys.exit();


    # create MCMC instance
    params["MCMC.Steps"]            = mcmcLength-1

    params["MCMC.Method"]           = "SingleChainMCMC"

    params["MCMC.Kernel"] = "PseudomarginalMHKernel"

    params["MCMC.Proposal"] = "AM"

    params["MCMC.BurnIn"]           = 1
    params["MCMC.MHProposal.PropSize"]      = 1e-2
    params["MCMC.DR.stages"]     = 3
    params["MCMC.DR.scale"]     = 10.0

    params["MCMC.AM.AdaptSteps"] = 50
    params["MCMC.AM.AdaptStart"] = 200
    params["Verbose"]               = 5

    params["MCMC.HDF5OutputDetails"] = 1
    params["MCMC.HDF5OutputGroup"] = "/"+runName
    params["HDF5.OutputGroup"] = "/"+runName


    #else, continue by starting up MUQ
    # start logging
#    libmuqUtilities.InitializeLogging(["geneticsPython"])
    # open an hdf5



    startPoint = [-1.5061233220e+0 ,  5.9597501480e-1,
 3.8086849630e-1 , -1.1786223030e-1,
-1.5861077760e+0 , -7.3832200240e-2,
 3.1773408260e-1 ,  8.5348964130e-2,
 1.6287352650e+0 ]

    paramDim = len(startPoint)


    # worseGraph = MakeGraph(noiseScale = 1, gridSize=20,  order=1)
    # betterGraph = MakeGraph(noiseScale = 0, gridSize=20,  order=2)


    data = np.loadtxt('data/Elliptic_fine.dat').tolist()
    priorMean = [0]*paramDim
    priorVar = [1]*paramDim
    obsVar = .00001

    if noise:
        numRuns = 10

        samplingProblem = libRandomPdes.Elliptic1DInvProblem(priorMean,  priorVar,  data,  obsVar,
                                                         numRuns,  stepSize,  10, noiseVal)
    else:
        numRuns = 1

        samplingProblem = libRandomPdes.Elliptic1DInvProblem(priorMean,  priorVar,  data,  obsVar,
                                                         numRuns,  stepSize,  10,  0.0)



    mcmc = libmuqInference.MCMCBase(samplingProblem, params)
    mcmc.Sample(startPoint)




def GenerateInvData(noise):


    mod = libRandomPdes.MakeEllipticProblemInv(noise,"1", 200, 200,1)
    # x=mod.Evaluate([[-5.4151366860e-1 , -1.1533465540e+0,
    x=mod.Evaluate([[-1.5061233220e+0 ,  5.9597501480e-1,
 3.8086849630e-1 , -1.1786223030e-1,
-1.5861077760e+0 , -7.3832200240e-2,
 3.1773408260e-1 ,  8.5348964130e-2,
 1.6287352650e+0 ]])
    mod = libRandomPdes.MakeEllipticProblemInv(noise,"1", 200, 10,0)

    x1=mod.Evaluate([[-1.5061233220e+0 ,  5.9597501480e-1,
 3.8086849630e-1 , -1.1786223030e-1,
-1.5861077760e+0 , -7.3832200240e-2,
 3.1773408260e-1 ,  8.5348964130e-2,
 1.6287352650e+0 ]])



    mod = libRandomPdes.MakeEllipticProblemInv(0,"2", 200, 10,0)

    onePath = mod.Evaluate([[-1.5061233220e+0 ,  5.9597501480e-1,
 3.8086849630e-1 , -1.1786223030e-1,
-1.5861077760e+0 , -7.3832200240e-2,
 3.1773408260e-1 ,  8.5348964130e-2,
 1.6287352650e+0 ]])
    import math
    dataCov = math.sqrt(.00001) * np.random.randn(len(onePath))

    print "generate ", onePath
    print "noise", dataCov

    # np.savetxt('data/Elliptic_fine.dat', (np.array(onePath)+dataCov).tolist() )




    # np.savetxt('data/obs_1d.dat', x)
    print x

    import plotTools

    fig = plotTools.MakeFigure(400,.7, False)

    # y = np.linspace(0.0, 1.0, 200+1)
    # plt.plot(y,x, color = plotTools.PlotColor("blue"))
    # y = np.linspace(0.0, 1.0, 10+1)
    # plt.plot(y[1:10],x1, 'o',color=plotTools.PlotColor('medium-dark red'),alpha=.8)

    plt.plot([0,.095, .1,.195, .2,.295, .3,.395, .4,.495, .5,.595, .6,.695, .7,.795, .8,.895, .9,1.0],
             [1,1,-1.5061233220e+0 , -1.5061233220e+0 , 5.9597501480e-1, 5.9597501480e-1,
 3.8086849630e-1 , 3.8086849630e-1 , -1.1786223030e-1,-1.1786223030e-1,
-1.5861077760e+0 ,-1.5861077760e+0 , -7.3832200240e-2, -7.3832200240e-2,
 3.1773408260e-1 , 3.1773408260e-1 , 8.5348964130e-2, 8.5348964130e-2,
 1.6287352650e+0 , 1.6287352650e+0], color = plotTools.PlotColor("blue"))

    plt.xlabel('x')
    # plt.ylabel('u')
    plt.ylabel('log $\kappa$')
    fig.tight_layout(pad=0.1)
    plt.savefig('plots/elliptic1d_kappa_paper.pdf',format='pdf')
    # plt.savefig('plots/elliptic1d_kappa_presentation.pdf',format='pdf')
    # plt.show()




if __name__ == '__main__':

    comm = MPI.COMM_WORLD


    runLength = 100000
    jobs = []

    pdeinit = libmuqPde.LibMeshInit(sys.argv)

    GenerateInvData(0);

    plt.show()
    sys.exit()

    # MakeGraph(noiseScale = 1, gridSize=20, plotRef=True, solutionColor = "blue", order=1)

    # RunOneHydrology(noiseScale = 0, gridSize=20, plotRef=True, solutionColor="orange")
    # RunOneHydrology(noiseScale = 0, gridSize=200, plotRef=True, solutionColor="purple")
    # RunOneHydrology(noiseScale = 0, gridSize=10, plotRef=True, solutionColor="orange")

    # for i in range(40):
    #     RunOneHydrology(noiseScale = 2, gridSize=20, plotRef=False)
    # plt.show()



    for i in range(1):

        # jobs.append(lambda i=i: RunOneForward(i,mcmcLength=runLength,stepInt=1))
        # jobs.append(lambda i=i: RunOneForward(i,mcmcLength=runLength,stepInt=2))
        # jobs.append(lambda i=i: RunOneForward(i,mcmcLength=runLength,stepInt=3))
        # jobs.append(lambda i=i: RunOneForward(i,mcmcLength=runLength,stepInt=4))
        # jobs.append(lambda i=i: RunOneForward(i,mcmcLength=runLength,stepInt=5))


        jobs.append(lambda i=i: RunOneInv(i,mcmcLength=runLength,stepInt=1, noise=False))
        jobs.append(lambda i=i: RunOneInv(i,mcmcLength=runLength,stepInt=2, noise=False))
        jobs.append(lambda i=i: RunOneInv(i,mcmcLength=runLength,stepInt=3, noise=False))
        jobs.append(lambda i=i: RunOneInv(i,mcmcLength=runLength,stepInt=4, noise=False))
        jobs.append(lambda i=i: RunOneInv(i,mcmcLength=runLength,stepInt=5, noise=False))
        jobs.append(lambda i=i: RunOneInv(i,mcmcLength=runLength,stepInt=1, noise=True))
        jobs.append(lambda i=i: RunOneInv(i,mcmcLength=runLength,stepInt=2, noise=True))
        jobs.append(lambda i=i: RunOneInv(i,mcmcLength=runLength,stepInt=3, noise=True))
        jobs.append(lambda i=i: RunOneInv(i,mcmcLength=runLength,stepInt=4, noise=True))
        jobs.append(lambda i=i: RunOneInv(i,mcmcLength=runLength,stepInt=5, noise=True))

        # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=2, randomSolver=True))
        # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=3, randomSolver=True))
        # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=4, randomSolver=True))
        # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=5, randomSolver=True))



#seed = libmuqUtilities.RandomGeneratorTemporarySetSeed(87478)
#libmuqUtilities.HDF5Wrapper.OpenFile('results/genetics_regression_looser.h5_rank_'+ str(comm.Get_rank()))
libmuqUtilities.HDF5Wrapper.OpenFile('results/elliptic1d_paper_inv.h5_rank_'+ str(comm.Get_rank()))
print "I'm", comm.Get_rank(), comm.Get_size()
for aJob in jobs[comm.Get_rank()::comm.Get_size()]:
    aJob()
comm.barrier()
libmuqUtilities.HDF5Wrapper.CloseFile()