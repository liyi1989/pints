#include "MUQ/Pde/PDE.h"

using namespace std;
using namespace muq::Utilities;
using namespace muq::Modelling;
using namespace muq::Pde;

PDE::PDE(Eigen::VectorXi const& inputSizes, shared_ptr<GenericEquationSystems> const& eqnsystem, boost::property_tree::ptree const& para) :
  ModPiece(inputSizes, para.get("PDE.OutputSize", 0), false, true, false, false, false, para.get("PDE.Name", "")), eqnsystem(eqnsystem), timeDependentMassMatrix(para.get("PDE.TimeDependentMassMatrix", false))
{
  // extract the parameters
  ExtractParameterList(para);

  string needGrads = para.get("PDE.NeedSpatialGradient", "");
  needGradient = ListToVector(needGrads);

  // get a constant reference to the mesh
  const libMesh::MeshBase& mesh = GetEquationSystemsPtr()->get_mesh();

  // get a reference to the system we need to solve
  libMesh::System& system = GetEquationSystemsPtr()->get_system<libMesh::System>(GetName());

  // get a constant reference to the degree of freedom map
  const libMesh::DofMap& dofMap = system.get_dof_map();

  // get the number of variables and a list of the variable numbers
  vector<unsigned int> varNums;
  system.get_all_variable_numbers(varNums);

  // compute the boost bimap between the variable names and numbers
  for( unsigned int v=0; v<varNums.size(); ++v ) {
    const string var = system.variable_name(varNums[v]);

    // erase what is currently there 
    vars.right.erase(var);

    // add it
    vars.insert(bimap::value_type(varNums[v], var));
  }

  // get the FE type for each unknown
  feType.resize(varNums.size());
  for (unsigned int i = 0; i<varNums.size(); ++i) {
    feType[i] = dofMap.variable_type(varNums[i]);
  }

  double noiseScale = para.get("PDE.NoiseScale", 0.0);
  
  // precompute the Jacobian, basis functions, ect... for each element
  elemInfo = make_shared<ElementInformation>(feType, para.get<unsigned int>("PDE.GaussianQuadratureOrder", 5), mesh, noiseScale);
}

shared_ptr<PDE> PDE::Create(Eigen::VectorXi const& inSizes, shared_ptr<GenericEquationSystems> const& system, boost::property_tree::ptree& para) 
{
  // variable names 
  vector<string> vars;
  
  // variable orders (on the element)
  vector<unsigned int> orders;

  // system name and extract variable information
  const string name = ExtractSystemInformation(vars, orders, para);

  // if the system exists
  if( system->GetEquationSystemsPtr()->has_system(name) ) {
    // delete and replace 
    system->GetEquationSystemsPtr()->delete_system(name);
  }

  // create a system
  libMesh::System& sys = system->GetEquationSystemsPtr()->add_system<libMesh::System>(name);
  
  // add the unknowns to the system
  for (unsigned int i = 0; i < vars.size(); ++i) { 
    if( orders[i] == 0 ) { // constant
      sys.add_variable(vars[i], libMesh::CONSTANT, libMesh::MONOMIAL); 
    } else { // nonconstant
      sys.add_variable(vars[i], libMesh::Order(orders[i])); 
    }	
  }  
  
  // initialize the system
  sys.init(); 
  
  // get the output size 
  para.put("PDE.OutputSize", sys.n_dofs());

  auto it = GetPdeMap()->find(name);
  
  if( it == GetPdeMap()->end() ) {
    cerr << endl << "ERROR: Could not find PDE system " << name << "." << endl << endl;
    assert(it != GetPdeMap()->end() );
  }

  return (it->second)(inSizes, system, para);
}

shared_ptr<PDE::PdeMap> PDE::GetPdeMap() 
{
  static shared_ptr<PDE::PdeMap> pdeMap;

  if (!pdeMap) {
    pdeMap = make_shared<PDE::PdeMap>();
  }

  return pdeMap;
}

string PDE::ExtractSystemInformation(vector<string>                   & vars,
				   vector<unsigned int>             & orders,
				   boost::property_tree::ptree const& params)
{
  // make a vector of the variable names
  string varList = params.get<string>("PDE.Unknowns", ""); // comma separated
  vars = ListToVector(varList);
  
  // make vector of variable order
  string varOrder = params.get<string>("PDE.UnknownOrders", ""); // comma separated
  boost::algorithm::erase_all(varOrder, " ");
  transform(varOrder.begin(), varOrder.end(), varOrder.begin(), ::tolower);

  // get the variable names
  vector<string> orderStringVec = ListToVector(varOrder);
  orders.resize(orderStringVec.size());
  
  // convert string->int
  for (unsigned int i = 0; i < orderStringVec.size(); ++i) {
    orders[i] = atoi(orderStringVec[i].c_str());
    
    // make sure the order is 0, 1, or 2
    assert((orders[i] == 0) || (orders[i] == 1) || (orders[i] == 2));
  }
  
  // make sure every unknown has an order for the approx.
  assert(orders.size() == vars.size());

  // get the system name
  return params.get<string>("PDE.Name", "");
}

void PDE::ExtractParameterList(boost::property_tree::ptree const& params)
{
  // get a vector of the parameter names
  string paraList = params.get<string>("PDE.Parameters", ""); // comma seperated

  // convert to vector
  vector<string> paraNameVec = ListToVector(paraList);

  // get a vector of bools; Are the parameters mesh parameters?
  string paraTypeStr = params.get<string>("PDE.IsScalarParameter", ""); // comma seperated
  boost::algorithm::erase_all(paraTypeStr, " ");
  transform(paraTypeStr.begin(), paraTypeStr.end(), paraTypeStr.begin(), ::tolower);
  vector<string> paraType = ListToVector(paraTypeStr);

  assert(paraType.size() == paraNameVec.size());

  // make pairs; <is the parameter a scalar?, the parameter name>
  inputNames.resize(paraType.size());
  for (unsigned int i=0; i<paraType.size(); ++i) {
    // this is a scalar
    if (paraType[i].compare("true") == 0) { 
      inputNames[i] = pair<ParameterType, string>(ParameterType::scalarParameter, paraNameVec[i]);
      
      // mesh dependent parameter
    } else if( GetEquationSystemsPtr()->has_system(paraNameVec[i]) ) { 
      assert(paraType[i].compare("false") == 0);
      inputNames[i] = pair<ParameterType, string>(ParameterType::meshDependent, paraNameVec[i]);

      // the parameter system
      libMesh::System& paraSys = GetEquationSystemsPtr()->get_system(paraNameVec[i]);

      // the bimap for the names/numbers 
      bimap paras;

      // get the parameter numbers
      vector<unsigned int> paraNums;
      paraSys.get_all_variable_numbers(paraNums);

      // add the parameters to the bimap 
      for( unsigned int v=0; v<paraNums.size(); ++v ) {
	const string var = paraSys.variable_name(paraNums[v]);

	// erase what is there 
	paras.right.erase(var);

	// add what is new
	paras.insert(bimap::value_type(paraNums[v], var));
      }

      // erase what is there 
      paraVars.erase(paraNameVec[i]);

      // store the bimap 
      paraVars.insert(pair<string, bimap>(paraNameVec[i], paras));

      // nonscalar, not mesh-dependent
    } else {
      assert(paraType[i].compare("false") == 0);
      inputNames[i] = pair<ParameterType, string>(ParameterType::vectorParameter, paraNameVec[i]);
    }
  }
}

void PDE::StoreInputs(std::vector<Eigen::VectorXd> const& inputs)
{
  // make sure we have the right number of inputs
  assert(inputs.size() == inputNames.size());

  // clear the points were the mesh-dependent parameters have been interpolated
  paraVals.clear(); 
  paraGrads.clear();
  
  // make sure they emptied
  assert(paraVals.empty());
  assert(paraGrads.empty());

  // constant reference to the libmesh equation system 
  shared_ptr<libMesh::EquationSystems> eSys = GetEquationSystemsPtr();

  // loop through all of the inputs
  for( unsigned int i=0; i<inputNames.size(); ++i ) {
    if( inputNames[i].first == ParameterType::scalarParameter ) { // if scalar, store as a double in the eqnsystem 
      assert(inputs[i].size() == 1);

      // erase what is currently there 
      scalarParameters.erase(inputNames[i].second);

      // name the parameter after the inputName[i].second
      scalarParameters.insert(pair<string, double>(inputNames[i].second, inputs[i](0)));
    } else if( inputNames[i].first == ParameterType::meshDependent ) { // it is mesh dependent 
      // project the paramter onto the mesh
      ProjectParameter(inputNames[i].second, inputs[i]);
    } else { // the parameter is neither scalar nor mesh-dependent
      // erase what is currently there 
      scalarParameters.erase(inputNames[i].second);

      // store as a vector; name the parameter after the inputName[i].second
      vectorParameters.insert(pair<string, Eigen::VectorXd>(inputNames[i].second, inputs[i]));
    }
  }
}

void PDE::ProjectParameter(string const& paraName, Eigen::VectorXd const& paraVec)
{
  // translate the parameter into an std::vector 
  Translater<Eigen::VectorXd, vector<double> > trans(paraVec);

  // get a reference to the system
  libMesh::System& system = GetEquationSystemsPtr()->get_system(paraName);

  // translate into the libmesh vector and project 
  system.solution->operator=(*trans.GetPtr());

  // update the system 
  system.update();
}

Eigen::VectorXd PDE::SetDirichletConditions(Eigen::VectorXd const& solution, double const time)
{
  // the solution size must be the output size 
  assert(solution.size() == outputSize);

  // get a const reference to the mesh 
  const libMesh::MeshBase& mesh = GetEquationSystemsPtr()->get_mesh();

  // get a const reference to the system 
  const libMesh::System& system = GetEquationSystemsPtr()->get_system(GetName());

  // the system number 
  const int sysNum = system.number();

  // everything should be the same as the current solution 
  Eigen::VectorXd soln = solution;

  // except the current solution 
  for( unsigned int i=0; i<mesh.n_nodes(); ++i ) { // loop though the nodes 
    // get node i 
    const libMesh::Node nd = mesh.node(i);

    // compute the parameters at this point 
    ComputeParametersAtPoint(nd);

    // if this is a Dirichlet node
    if( IsDirichlet(nd) ) {
      // loop through each of the PDE's variables 
      for( bimap::const_iterator iter=vars.begin(), iend = vars.end(); iter!=iend; ++iter ) {
	// get the variable name
	const string varName = iter->right;

	// get the variable number 
	const unsigned int varNum = iter->left;

	// loop through the components associated with this system and variable
	for( unsigned int c=0; c<nd.n_comp(sysNum, varNum); ++c ) {
	  // get the degree of freedom for this component 
	  const unsigned int dof = nd.dof_number(sysNum, varNum, c);

	  // the solution at the degree of freedom is the Dirichlet BC
	  soln(dof) = DirichletBCs(varName, nd, time);
	}
      }
    }
  }

  return soln;
}

void PDE::ComputeFadTypeAtPoint(libMesh::Point const& point, string const& paraSysName, bool const active, int const elNum)
{
  // raw pointer to the element 
  const libMesh::Elem* el = GetEquationSystemsPtr()->get_mesh().elem(elNum);

  // get the system
  const libMesh::System& system = eqnsystem->GetEquationSystemsPtr()->get_system(paraSysName);

  // parameter dofs
  const libMesh::DofMap& dofMapPara = system.get_dof_map();

  // dof local->global map 
  vector<libMesh::dof_id_type> dofIndices;
  dofMapPara.dof_indices(el, dofIndices);

  // the total number of dofs
  const unsigned int totalElemDofs = dofIndices.size();

  // the offset of each parameter 
  unsigned int offset = 0;

  // do we need the spatial gradient for this parameter?
  const bool needGrad = find(needGradient.begin(), needGradient.end(), paraSysName)!=needGradient.end();
  
  // the parameters for this mesh dependent system 
  bimap bmp = paraVars.at(paraSysName);
  
  // loop through each parameter 
  for( bimap::const_iterator iter=bmp.begin(); iter!=bmp.end(); ++iter ) {
    // the variable name 
    const string para = iter->right;
    
    // the parameter's value 
    FadType val = 0.0;

    // the gradient 
    vector<FadType> grad(3, 0.0);
    
    // try to get the value from the maps 
    auto valIt = paraValsFad.find(pair<string, libMesh::Point>(para, point));

    // try to get the gradient from the maps
    auto gradIt = paraGradsFad.find(pair<string, libMesh::Point>(para, point));

    // do we need to compute the parameter at this point?
    const bool computed = valIt!=paraValsFad.end() && needGrad && gradIt!=paraGradsFad.end();

    // the variable number 
    const unsigned int paraNum = iter->left;
    
    // get the dofs for this parameter 
    vector<libMesh::dof_id_type> dofIndicesPara;
    dofMapPara.dof_indices(el, dofIndicesPara, paraNum);

    // Get the no of dofs assciated with this point
    const unsigned int numDofs = dofIndicesPara.size();
    
    // if we have computed the value at this point already 
    if( computed ) {
      // retrieve the value 
      if( active ) { // active FadType 
	val = valIt->second;

	if( needGrad ) {
	  grad = gradIt->second;
	}
      } else { // passive FadType
	val = FadType(valIt->second.val());

	if( needGrad ) {
	  for( unsigned int d=0; d<3; ++d ) {
	    grad[d] = FadType(gradIt->second[d]);
	  }
	}
      }

      // it has not been computed
    } else {
      assert(elNum >= 0);

      // get the type of variable 
      const libMesh::FEType& feType = dofMapPara.variable_type(paraNum);

      // Build a FE so we can calculate u(p)
      libMesh::AutoPtr<libMesh::FEBase> fe(libMesh::FEBase::build(el->dim(), feType));

      // Map the physical co-ordinates to the master co-ordinates using the inverse_map from fe_interface.h build a
      // vector of point co-ordinates to send to reinit
      vector<libMesh::Point> coor(1, libMesh::FEInterface::inverse_map(el->dim(), feType, el, point));
      
      // Get the shape function values
      const vector<vector<libMesh::Real> >& phi = fe->get_phi();
      
      // Get the values of the shape function derivatives
      const std::vector<std::vector<libMesh::RealGradient> >& dphi = fe->get_dphi();
      
      // Reinitialize the element and compute the shape function values at coor
      fe->reinit(el, &coor);

      // get the parameter as FadType
      vector<FadType> paraFad(numDofs);
      for (unsigned int l = 0; l<numDofs; ++l) {
	paraFad[l] = FadType(totalElemDofs, offset + l, system.solution->operator()(dofIndicesPara[l]));
      }

      // accumulate value
      for (unsigned int l = 0; l < numDofs; ++l) {
	val += phi[l][0] * paraFad[l];
      }

      // if we need the gradient 
      if( needGrad ) {
	for (unsigned int l = 0; l < numDofs; ++l) {
	  for (unsigned int d = 0; d < eqnsystem->GetEquationSystemsPtr()->get_mesh().mesh_dimension(); ++d) {
	    grad[d] += dphi[l][0](d) * paraFad[l];
	  }
	}

	// store the parameter and gradient
	StoreParameters(true, para, point, val, grad);
      } else {
	// store the parameter
	StoreParameters(false, para, point, val);
      }
      
    }
    
    // add the offset
    offset += numDofs;

    // erase the current value 
    scalarParametersFad.erase(para);

    // store the value (equation system)
    scalarParametersFad.insert(pair<string, FadType>(para, val));

    if( needGrad ) {
      // erase the current value 
      scalarParametersFad.erase("d" + para + "dx");
      scalarParametersFad.erase("d" + para + "dy");
      scalarParametersFad.erase("d" + para + "dz");

      // store the gradient (equation system)
      scalarParametersFad.insert(pair<string, FadType>("d" + para + "dx", grad[0]));
      scalarParametersFad.insert(pair<string, FadType>("d" + para + "dy", grad[1]));
      scalarParametersFad.insert(pair<string, FadType>("d" + para + "dz", grad[2]));
    }
  }
}

void PDE::ComputeParametersAtPoint(libMesh::Point const& point, int const inputDimWrt)
{
  // set the element number to an invalid number 
  int elNum = -1;

  // if we know the element number where the point is located 
  if( GetEquationSystemsPtr()->parameters.have_parameter<int>("elNum") ) {
    // get the element number 
    elNum = GetEquationSystemsPtr()->parameters.get<int>("elNum");
  }

  // the index of the parameter
  const unsigned int paraIndex = inputDimWrt - (isTransient? 2 : 1);

  // reference to the equation system parameters
  libMesh::Parameters& parameters = GetEquationSystemsPtr()->parameters;

  // loop through all of the parameters 
  for( unsigned int p=0; p<inputNames.size(); ++p ) {

    // the name of the parameter 
    const string name = inputNames[p].second;

    // if it is mesh dependent
    if( inputNames[p].first==meshDependent ) {
      // We know the point; it's possible to make elNum = FindElement(point) (EXPENSIVE)
      assert(elNum >= 0); 

      ComputeFadTypeAtPoint(point, inputNames[p].second, p==paraIndex, elNum);
    } else if( inputNames[p].first==scalarParameter ) { // is scalar
      
      // if it is the parameter we are taking the derivative with respect to 
      if( p==paraIndex ) {
	// erase what is currently there 
	scalarParametersFad.erase(name);

	// the FadType version of the parameter in the parameters (active FadType)
	scalarParametersFad.insert(pair<string, FadType>(name, FadType(1, 0, scalarParameters.at(name))));
      } else {
	// erase what is currently there 
	scalarParametersFad.erase(name);

	// the FadType version of the parameter in the parameters (passive FadType)
	scalarParametersFad.insert(pair<string, FadType>(name, FadType(scalarParameters.at(name))));
      }
	
    } else { // is vector; could also use else if( inputNames[p].first==vectorParameter )
      // get the parameter vector 
      Eigen::VectorXd paraVec = vectorParameters.at(name);
      
      // load them as a fad type
      Eigen::Matrix<FadType, Eigen::Dynamic, 1> paraVecFad(paraVec.size());
      for( unsigned int i=0; i<inputSizes(inputDimWrt); ++i ) {
	// if it is the parameter we are taking the derivative with respect to 
	if( p==paraIndex ) {
	  // active FadType
	  paraVecFad(i) = FadType(inputSizes(inputDimWrt), i, paraVec(i));
	} else {
	  // passive FadType
	  paraVecFad(i) = FadType(paraVec(i));
	}
      }
      
      // erase what is currently there 
      vectorParametersFad.erase(name);

      // the FadType version of the parameter in the parameters
      vectorParametersFad.insert(pair<string, Eigen::Matrix<FadType, Eigen::Dynamic, 1> >(name, paraVecFad));
    }
  }
}

template<>
double PDE::GetScalarParameter(string const& name) const
{
  auto it = scalarParameters.find(name);

  if( it != scalarParameters.end() ){
    return it->second;
  } 

  cerr << endl << "COULD NOT FIND PARAMETER " << name << endl << endl;
  assert(it != scalarParameters.end());

  return 0.0;
}

template<>
FadType PDE::GetScalarParameter(string const& name) const
{
  auto it = scalarParametersFad.find(name);

  if( it != scalarParametersFad.end() ){
    return it->second;
  } 

  cerr << endl << "COULD NOT FIND PARAMETER " << name << endl << endl;
  assert(it != scalarParametersFad.end());

  return 0.0;
}

template<>
Eigen::VectorXd PDE::GetVectorParameter(string const& name) const 
{
  auto it = vectorParameters.find(name);

  if( it != vectorParameters.end() ){
    return it->second;
  } 

  cerr << endl << "COULD NOT FIND PARAMETER " << name << endl << endl;
  assert(it != vectorParameters.end());

  return Eigen::VectorXd();
}

template<>
Eigen::Matrix<FadType, Eigen::Dynamic, 1> PDE::GetVectorParameter(string const& name) const 
{
  auto it = vectorParametersFad.find(name);

  if( it != vectorParametersFad.end() ){
    return it->second;
  } 

  cerr << endl << "COULD NOT FIND PARAMETER " << name << endl << endl;
  assert(it != vectorParametersFad.end());

  return Eigen::Matrix<FadType, Eigen::Dynamic, 1>();
}

libMesh::Point PDE::GetCurrentPoint() const 
{
  return pnt;
}

void PDE::ComputeParametersAtPoint(libMesh::Point const& point)
{
  // set the point 
  pnt = point;

  // constant reference to the mesh
  const libMesh::MeshBase& mesh = GetEquationSystemsPtr()->get_mesh();

  // set the element number to an invalid number 
  int elNum = -1;

  // if we know the element number where the point is located 
  if( GetEquationSystemsPtr()->parameters.have_parameter<int>("elNum") ) {
    // get the element number 
    elNum = GetEquationSystemsPtr()->parameters.get<int>("elNum");
  }

  // loop through the mesh dependent parameters 
  for( map<string, bimap>::const_iterator bmp=paraVars.begin(); bmp!=paraVars.end(); ++bmp ) {
    // parameter system name 
    const string paraSysName = bmp->first;

    // do we need the spatial gradient for this parameter?
    const bool needGrad = find(needGradient.begin(), needGradient.end(), paraSysName)!=needGradient.end();
    
    // loop through each parameters 
    for( bimap::const_iterator iter=bmp->second.begin(); iter!=bmp->second.end(); ++iter ) {
      // get the variable name
      const string para = iter->right;

      // get the variable number 
      const unsigned int paraNum = iter->left;
      
      // the parameter's value 
      double val;

      // the parameter's gradient 
      libMesh::RealGradient grad;

      // try to get the value from the maps 
      auto valIt = paraVals.find(pair<string, libMesh::Point>(para, point));

      // try to get the gradient from the maps
      auto gradIt = paraGrads.find(pair<string, libMesh::Point>(para, point));

      // do we need to compute the parameter at this point?
      const bool computed = valIt!=paraVals.end() && needGrad && gradIt!=paraGrads.end();
      
      // if we have computed the value at this point already
      if( computed ) {
	// retrieve the value
	val = valIt->second;

	if( needGrad ) {
	  grad = gradIt->second;
	}
	
	// we don't know the value but we know the element 
      } else if( elNum != -1 ) {
	// get the const reference parameter system 
	const libMesh::System& system = GetEquationSystemsPtr()->get_system(paraSysName);
	
	// compute the value
	val = system.point_value(paraNum, point, *(mesh.elem(elNum)));
	
	// we don't know the element or the value (this is MUCH slower)
      } else { 
	// get the const reference parameter system 
	const libMesh::System& system = GetEquationSystemsPtr()->get_system(paraSysName);
	
	// compute the value
	val = system.point_value(paraNum, point);
      }
      
      // did the user ask for the gradient information?
      if( needGrad ) {
	// try to get the gradient from the maps 
	auto gradIt = paraGrads.find(pair<string, libMesh::Point>(para, point));

	// if we have computed the gradient at this point already
	if( gradIt != paraGrads.end() ) {
	  // retrive the gradient
	  grad = gradIt->second;
	  
	  // we don't know the value but we know the element 
	} else if( elNum != -1 ) {
	  // get the const reference parameter system 
	  const libMesh::System& system = GetEquationSystemsPtr()->get_system(paraSysName);

	  // compute the gradient 
	  grad = system.point_gradient(paraNum, point, *(mesh.elem(elNum)));
	  
	  // we don't know the element or the value (this is MUCH slower)
	} else { 
	  // get the const reference parameter system 
	  const libMesh::System& system = GetEquationSystemsPtr()->get_system(paraSysName);

	  // compute the gradient 
	  grad = system.point_gradient(paraNum, point);
	} 

	// store the value and gradient 
	StoreParameters(true, para, point, val, grad);
      } else {
	// store the value and gradient 
	StoreParameters(false, para, point, val);
      }

      // combine the point and parameter 
      pair<string, libMesh::Point> varpnt(para, point);

      // erase the currently stored value
      scalarParameters.erase(para);

      // store the value (equation system)
      scalarParameters.insert(pair<string, double>(para, val));

      if( needGrad ) {
	// erase the currently stored value
	scalarParameters.erase("d" + para + "dx");
	scalarParameters.erase("d" + para + "dy");
	scalarParameters.erase("d" + para + "dz");    

	// store the gradient (equation system)
	scalarParameters.insert(pair<string, double>("d" + para + "dx", grad(0)));
	scalarParameters.insert(pair<string, double>("d" + para + "dy", grad(1)));
	scalarParameters.insert(pair<string, double>("d" + para + "dz", grad(2)));    
      }
    }
  }
}

void PDE::StoreParameters(bool const storeGrad, string const& var, libMesh::Point const& point, double const val, libMesh::RealGradient const& grad)
{  
  // get a reference to the equation system parameters 
  libMesh::Parameters& parameters = GetEquationSystemsPtr()->parameters;

  // combine the point and parameter 
  pair<string, libMesh::Point> varpnt(var, point);

  // erase what is currently there 
  paraVals.erase(varpnt);

  // store the value (map)
  paraVals.insert(pair<pair<string, libMesh::Point>, double>(varpnt, val));

  // did the user ask for the gradient information?
  if( storeGrad ) {
    // erase what is currently there 
    paraGrads.erase(varpnt);

    // store the gradient (map)
    paraGrads.insert(pair<pair<string, libMesh::Point>, libMesh::RealGradient>(varpnt, grad));
  }
}

void PDE::StoreParameters(bool const storeGrad, string const& var, libMesh::Point const& point, FadType const val, vector<FadType> const& grad)
{  
  // get a reference to the equation system parameters 
  libMesh::Parameters& parameters = GetEquationSystemsPtr()->parameters;

  // combine the point and parameter 
  pair<string, libMesh::Point> varpnt(var, point);

  // erase what is currently there 
  paraValsFad.erase(varpnt);

  // store the value (map)
  paraValsFad.insert(pair<pair<string, libMesh::Point>, FadType>(varpnt, val));
  
  // did the user ask for the gradient information?
  if( storeGrad ) {
    // erase what is currently there 
    paraGradsFad.erase(varpnt);

    // store the gradient (map)
    paraGradsFad.insert(pair<pair<string, libMesh::Point>, vector<FadType> >(varpnt, grad));
  }
}

bool PDE::IsDirichlet(libMesh::Point const& point) const 
{
  return false;
}

double PDE::DirichletBCs(string const& varName, libMesh::Point const& point, double const time) const 
{
  return 0.0;
}

FadType PDE::DirichletBCs(string const& varName, libMesh::Point const& point, FadType const time) const
{
  return 0.0;
}

Eigen::MatrixXd PDE::JacobianImpl(vector<Eigen::VectorXd> const& inputs, int const inputDimWrt)
{
  // make sure everything is created correctly
  isTransient? assert(inputSizes.size()-2==inputNames.size()) : assert(inputSizes.size()-1==inputNames.size());
  assert(eqnsystem);

  // transient problems need to store the current time 
  if( isTransient ) {
    assert(inputs[0].size() == 1);
    GetEquationSystemsPtr()->parameters.set<double>("time") = inputs[0](0);
  }

  // compute the mass matrix; if it is transient and we have not yet computed it
  if( isTransient && (timeDependentMassMatrix || lumpedSumMassMatrix==boost::none) ) {
    ComputeMassMatrix();
  }

  // store the number of variables
  const unsigned int nVars = vars.size();

  // get a constant reference to the mesh 
  const libMesh::MeshBase& mesh = GetEquationSystemsPtr()->get_mesh();

  // get a reference to the system 
  libMesh::System& system = GetEquationSystemsPtr()->get_system<libMesh::System>(GetName());

  // get a constant reference to the DOF map 
  const libMesh::DofMap& dofMap = system.get_dof_map();

  // element matrices and vectors 
  auto elemDOFs = make_shared<ElementDOFs>(nVars);

  // zero out the jacobian
  Eigen::MatrixXd jacobian = Eigen::MatrixXd::Zero(outputSize, inputSizes(inputDimWrt));

  // first element
  libMesh::MeshBase::const_element_iterator el = mesh.active_local_elements_begin();

  // last element 
  const libMesh::MeshBase::const_element_iterator elEnd = mesh.active_local_elements_end();

  // element counter 
  unsigned int elNum = 0;
  
  // boundary element counter 
  unsigned int faceNum = 0;
  
  // loop through the elements 
  for( ; el!=elEnd; ++el ) {

    // if the jacocian is not wrt state we need the dof parameter mapping 
    boost::optional<vector<unsigned int> > paraDofs = ParameterElementDOFs(el, inputDimWrt); 

    // reset the degrees of freedom
    elemDOFs->Reset(el, vars, dofMap);

    // get information about this element 
    auto elInfo = elemInfo->Get(elNum);

    // set and increment the element number 
    GetEquationSystemsPtr()->parameters.set<int>("elNum") = elNum++;

    // if we want the derivatives wrt state
    if( paraDofs==boost::none ) {
      // compute the element jacobian 
      ComputeJacobian(elInfo, elemDOFs, inputs[isTransient? 1 : 0]);

      // if we want the derivative wrt parameter
    } else { 
      // compute the residual as a FadType
      ComputeResidual(elInfo, elemDOFs, inputs[isTransient? 1: 0], inputDimWrt);
    }

    // loop over the sides
    for (unsigned int side = 0; side < ((libMesh::Elem *)*el)->n_sides(); ++side) {
      // boundary check; are we on a boundary node?
      if( ((libMesh::Elem *)*el)->neighbor(side) == nullptr ) { 
	// get information about this boundary element
	auto faceInfo = elemInfo->GetFace(faceNum++);

	// if we want the derivatives wrt state
	if( paraDofs==boost::none ) {	
	  JacApplyBCs(elemDOFs, faceInfo, inputs[isTransient? 1 : 0]);
	  
	  // if we want the derivative wrt parameter 
	} else {
	  ResidApplyBCs(elemDOFs, faceInfo, inputs[isTransient? 1 : 0], inputDimWrt);
	}
      }
    }

    if( paraDofs==boost::none ) {
      // get local element information
      libMesh::DenseMatrix<libMesh::Number> Je = elemDOFs->GetElementStiffnessMatrix();
      vector<libMesh::dof_id_type> dofIndices  = elemDOFs->GetLocalToGlobalMap();
      
      // sum into global
      for( unsigned int i=0; i<dofIndices.size(); ++i ) {
	for( unsigned int j=0; j<dofIndices.size(); ++j ) {
	  jacobian(dofIndices[i], dofIndices[j]) += Je(i, j);
	}
      }
    } else { 
      // get the local residual
      libMesh::DenseVector<FadType> Re         = elemDOFs->GetElementVectorAD();
      vector<libMesh::dof_id_type> dofIndices  = elemDOFs->GetLocalToGlobalMap();
      // add to global
      for( unsigned int i=0; i<dofIndices.size(); ++i ) {
	for( unsigned int j=0; j<(*paraDofs).size(); ++j ) {
	  jacobian(dofIndices[i], (*paraDofs)[j]) += Re(i).dx(j);
	}
      }
    }
  }

  EnforceMatrixBCs(jacobian, inputDimWrt==(isTransient? 1 : 0));

  if( isTransient ) {
    // invert the lumped sum mass matrix ("*" to get rid of boost::optional)
    const Eigen::VectorXd dlmmInv = 1.0 / (*lumpedSumMassMatrix).array();

    // apply the mass matrix inverse to the jacobian
    jacobian = dlmmInv.asDiagonal() * jacobian;
  }
  return jacobian;
}

Eigen::VectorXd PDE::EvaluateImpl(vector<Eigen::VectorXd> const& inputs) 
{
  // make sure everything is created correctly
  isTransient? assert(inputSizes.size()-2==inputNames.size()) : assert(inputSizes.size()-1==inputNames.size());
  assert(eqnsystem);

  // transient problems need to store the current time 
  if( isTransient ) {
    assert(inputs[0].size() == 1);
    GetEquationSystemsPtr()->parameters.set<double>("time") = inputs[0](0);
  }

  // compute the mass matrix; if it is transient and we have not yet computed it
  if( isTransient && (timeDependentMassMatrix || lumpedSumMassMatrix==boost::none) ) {
    ComputeMassMatrix();
  }

  // store the number of variables
  const unsigned int nVars = vars.size();

  // get a constant reference to the mesh 
  const libMesh::MeshBase& mesh = GetEquationSystemsPtr()->get_mesh();

  // get a reference to the system 
  libMesh::System& system = GetEquationSystemsPtr()->get_system<libMesh::System>(GetName());

  // get a constant reference to the DOF map 
  const libMesh::DofMap& dofMap = system.get_dof_map();

  // element matrices and vectors 
  auto elemDOFs = make_shared<ElementDOFs>(nVars);

  // zero out the residual 
  Eigen::VectorXd residual = Eigen::VectorXd::Zero(outputSize);

  // first element
  libMesh::MeshBase::const_element_iterator el = mesh.active_local_elements_begin();

  // last element 
  const libMesh::MeshBase::const_element_iterator elEnd = mesh.active_local_elements_end();

  // element counter 
  unsigned int elNum = 0;
  
  // boundary element counter 
  unsigned int faceNum = 0;
  
  // loop through the elements 
  for( ; el!=elEnd; ++el ) {
    // reset the degrees of freedom
    elemDOFs->Reset(el, vars, dofMap);

    // get information about this element 
    auto elInfo = elemInfo->Get(elNum);

    // set and increment the element number 
    GetEquationSystemsPtr()->parameters.set<int>("elNum") = elNum++;

    // compute the element residual
    ComputeResidual(elInfo, elemDOFs, inputs[isTransient? 1 : 0]);

    // loop over the sides
    for (unsigned int side = 0; side < ((libMesh::Elem *)*el)->n_sides(); ++side) {
      // boundary check; are we on a boundary node?
      if( ((libMesh::Elem *)*el)->neighbor(side) == nullptr ) { 
	// get information about this boundary element
	auto faceInfo = elemInfo->GetFace(faceNum++);

	ResidApplyBCs(elemDOFs, faceInfo, inputs[isTransient? 1 : 0]);
      }
    }

    // get the local residual
    libMesh::DenseVector<libMesh::Number> Re = elemDOFs->GetElementVector();
    vector<libMesh::dof_id_type> dofIndices  = elemDOFs->GetLocalToGlobalMap();
    
    // add to global
    for( unsigned int i=0; i<dofIndices.size(); ++i ) {
      residual(dofIndices[i]) += Re(i);
    }
  }

  // enforce the Dirichlet BCs
  EnforceDirichletBCs(residual);

  // apply the mass matrix inverse for transient problems
  if( isTransient ) {
    // the "*" is because it is a boost::optional type
    residual = residual.array() / (*lumpedSumMassMatrix).array();
  }
  

  return residual;
}

void PDE::EnforceDirichletBCs(Eigen::VectorXd& residual) const
{
  FadType time = isTransient? FadType(1, 0, GetEquationSystemsPtr()->parameters.get<double>("time")) : 0.0;

  // get a constant reference to the mesh 
  const libMesh::MeshBase& mesh = GetEquationSystemsPtr()->get_mesh();

  // get the system number
  const int sysNum = GetEquationSystemsPtr()->get_system(GetName()).number();

  // loop though the nodes
  for (unsigned int i = 0; i < mesh.n_nodes(); ++i) {
    // the current node
    libMesh::Node nd = mesh.node(i);

    if (IsDirichlet(nd)) { // is the node on the boundary loop through each variable
    // loop over the variables 
      for( bimap::const_iterator iter=vars.begin(), iend=vars.end(); iter!=iend; ++iter ) {
        // get the variable name
	const string varName = iter->right;

	// get the variable number
	const unsigned int varNum = iter->left;

	// the components of the node global dof of
        for (unsigned int c = 0; c < nd.n_comp(sysNum, varNum); ++c) {
	  // the degree of freedom
          const unsigned int dof = nd.dof_number(sysNum, varNum, c);

          // enforce the dirichlet condition
          residual(dof) = isTransient? DirichletBCs(varName, nd, time).dx(0) : 0.0;
        }
      }
    }
  }
}

void PDE::JacApplyBCs(shared_ptr<ElementDOFs> const elemDOFs, shared_ptr<FaceInfo> const faceInfo, Eigen::VectorXd const& soln)
{
  // get the number of variables
  const unsigned int nVars = vars.size();

  // element jacobian
  const vector<libMesh::Real> JxWFace = faceInfo->JxW;

  // element quadruature points
  const vector<libMesh::Point> qFacePoint = faceInfo->qPoint;

  // element normals
  const vector<libMesh::Point> normals = faceInfo->normals;

  // the solutions for each variable
  vector<vector<FadType> > solnDOF;

  // extract the solution for each variable at each DOF
  ExtractVariableSolutions(solnDOF, soln, elemDOFs);

  // loop through the boundary quadrature points 
  for( unsigned int qp=0; qp<qFacePoint.size(); ++qp ) {
    // determine if this is a dirichlet point or not
    if( !IsDirichlet(qFacePoint[qp]) ) {
      // interpolate the parameters at this point
      ComputeParametersAtPoint(qFacePoint[qp]);
      
      // the variables at this quad. point
      vector<FadType> varSoln(nVars);
      
      // the variable gradients at this quad. point 
      vector<vector<FadType> > varGrad(nVars);
      
      // loop over the variables 
      unsigned int v = 0;
      for( bimap::const_iterator iter=vars.begin(), iend=vars.end(); iter!=iend; ++iter ) {
	// current solution and gradient for this variable 
	const pair<FadType, vector<FadType> >& curr = InterpolateToQP<FadType, FaceInfo>(qp, iter->left, solnDOF[v], faceInfo);
	
	// store the variable 
	varSoln[v] = curr.first;
	
	// store the gradient
	varGrad[v] = curr.second;
	
	// increment counter 
	++v;
      }
      
      // loop through the unknowns
      for( bimap::const_iterator iter=vars.begin(); iter!=vars.end(); ++iter ) {
	// the variable number 
	const unsigned int var1 = iter->left; 
	
	// basis functions and their derivatives on element
	const vector<vector<libMesh::Real> > phiFaceV1 = faceInfo->basisMap[feType[var1]]->phi;

	// compute the neumann conditions
        vector<FadType> neumann = NeumannBCs(vars.left.at(var1), qFacePoint[qp], varSoln, varGrad);

	// compute dot(neuman, normal)
	FadType value = 0.0;
        for (unsigned int h = 0; h < neumann.size(); ++h) {
          value += neumann[h] * normals[qp](h);
        }

	// loop through the shape functions
	for( unsigned int i=0; i<phiFaceV1.size(); ++i ) {
	  // offset each variable in the system 
	  unsigned int offset = 0;

	  // loop through the unknowns
	  for( bimap::const_iterator jter=vars.begin(); jter!=vars.end(); ++jter ) {
	    // the variable number 
	    const unsigned int var2 = jter->left; 

	    // basis functions and their derivatives on element
	    const vector<vector<libMesh::Real> > phiFaceV2 = faceInfo->basisMap[feType[var2]]->phi;

	    // loop through the shape functions
	    for( unsigned int j=0; j<phiFaceV2.size(); ++j ) {
              elemDOFs->SumIntoElementStiffnessMatrix(var1, var2, i, j, JxWFace[qp] * value.dx(offset + j) * phiFaceV1[i][qp] * phiFaceV2[j][qp]);
	    }

	    // increment the offset
	    offset += elemDOFs->GetNumDOFs(var2);
	  }
	}
      }
    }
  }
}

void PDE::ResidApplyBCs(shared_ptr<ElementDOFs> const elemDOFs, shared_ptr<FaceInfo> const faceInfo, Eigen::VectorXd const& soln, int const inputDimWrt)
{
  // get the number of variables
  const unsigned int nVars = vars.size();

  // element jacobian
  const vector<libMesh::Real> JxWFace = faceInfo->JxW;

  // element quadruature points
  const vector<libMesh::Point> qFacePoint = faceInfo->qPoint;

  // element normals
  const vector<libMesh::Point> normals = faceInfo->normals;

  // the solutions for each variable
  vector<vector<double> > solnDOF;

  // extract the solution for each variable at each DOF
  ExtractVariableSolutions(solnDOF, soln, elemDOFs);

  // loop through the boundary quadrature points 
  for( unsigned int qp=0; qp<qFacePoint.size(); ++qp ) {
    if( !IsDirichlet(qFacePoint[qp]) ) {
      // interpolate the parameters at this point
      if( inputDimWrt<0 ) {
	ComputeParametersAtPoint(qFacePoint[qp]);
      } else { 
	ComputeParametersAtPoint(qFacePoint[qp], inputDimWrt);
      }
      
      // the variables at this quad. point
      vector<double> varSoln(nVars);
      
      // the variable gradients at this quad. point 
      vector<vector<double> > varGrad(nVars);
      
      // loop over the variables 
      unsigned int v = 0;
      for( bimap::const_iterator iter=vars.begin(), iend=vars.end(); iter!=iend; ++iter ) {
	// current solution and gradient for this variable 
	const pair<double, vector<double> >& curr = InterpolateToQP<double, FaceInfo>(qp, iter->left, solnDOF[v], faceInfo);
	
	// store the variable 
	varSoln[v] = curr.first;
	
	// store the gradient
	varGrad[v] = curr.second;
	
	// increment counter 
	++v;
      }
      
      // loop through the unknowns
      for( bimap::const_iterator iter=vars.begin(); iter!=vars.end(); ++iter ) {
	// the variable number 
	const unsigned int var = iter->left; 
	
	// basis functions and their derivatives on element
	const vector<vector<libMesh::Real> > phiFace = faceInfo->basisMap[feType[var]]->phi;
	if( inputDimWrt<0 ) {
	  vector<double> neumann = NeumannBCs(vars.left.at(var), qFacePoint[qp], varSoln, varGrad);
	
	  double value = 0.0;
	  for (unsigned int h = 0; h < neumann.size(); ++h) {
	    value += neumann[h] * normals[qp](h);
	  }
	  
	  // loop over the shape functions
	  for( unsigned int i=0; i<phiFace.size(); i++ ) { 
	    // sum into the element vector
	    elemDOFs->SumIntoElementVector(var, i, JxWFace[qp] * value * phiFace[i][qp]);
	  }
	} else {
	  vector<FadType> neumann = NeumannBCsPara(vars.left.at(var), qFacePoint[qp], varSoln, varGrad);
	
	  FadType value = 0.0;
	  for (unsigned int h = 0; h < neumann.size(); ++h) {
	    value += neumann[h] * normals[qp](h);
	  }
	  
	  // loop over the shape functions
	  for( unsigned int i=0; i<phiFace.size(); i++ ) { 
	    // sum into the element vector
	    elemDOFs->SumIntoElementVector(var, i, JxWFace[qp] * value * phiFace[i][qp]);
	  }
	}
      }
    }
  }
}

void PDE::ComputeJacobian(std::shared_ptr<ElemInfo> const& elInfo, std::shared_ptr<ElementDOFs> const& elemDOFs, Eigen::VectorXd const& soln)
{
  // get the number of variables
  const unsigned int nVars = vars.size();

  // the solutions for each variable
  vector<vector<FadType> > solnDOF;

  // extract the solution for each variable at each DOF
  ExtractVariableSolutions(solnDOF, soln, elemDOFs);

  // element jacobians (const reference to avoid the copy)
  const vector<libMesh::Real>& JxW = elInfo->JxW;
  
  // quadrature points on each element (const reference to avoid the copy)
  const vector<libMesh::Point>& qPoint = elInfo->qPoint;

  // loop over the quadrature points 
  assert(JxW.size() == qPoint.size());
  for( unsigned int qp=0; qp<qPoint.size(); ++qp ) {
    // interpolate the parameters at this point
    ComputeParametersAtPoint(qPoint[qp]);

    // the variables at this quad. point
    vector<FadType> varSoln(nVars);

    // the variable gradients at this quad. point 
    vector<vector<FadType> > varGrad(nVars);

    // loop over the variables 
    unsigned int v = 0;
    for( bimap::const_iterator iter=vars.begin(), iend=vars.end(); iter!=iend; ++iter ) {
      // current solution and gradient for this variable 
      const pair<FadType, vector<FadType> >& curr = InterpolateToQP<FadType, ElemInfo>(qp, iter->left, solnDOF[v], elInfo);

      // store the variable 
      varSoln[v] = curr.first;
      
      // store the gradient
      varGrad[v] = curr.second;

      // increment counter 
      ++v;
    }

    // loop over the variables 
    for( bimap::const_iterator iter=vars.begin(); iter!=vars.end(); ++iter ) {
      // the variable number 
      const unsigned int var1 = iter->left; 

      // basis functions on the element (const reference to avoid the copy)
      const vector<vector<libMesh::Real> >& phiV = elInfo->basisMap[feType[var1]]->phi;

      // basis function derivatives on the element (const reference to avoid the copy)
      const vector<vector<libMesh::RealGradient> >& dphiV = elInfo->basisMap[feType[var1]]->dphi;

      // loop through the basis functions 
      for( unsigned int i=0; i<phiV.size(); ++i ) {
	// compute the residual at the quad. point
	const FadType resid = ResidualImpl(vars.left.at(var1), phiV[i][qp], dphiV[i][qp], varSoln, varGrad);

	// offset each variable in the system 
	unsigned int offset = 0;
	
	// loop over the variables 
	for( bimap::const_iterator jter=vars.begin(); jter!=vars.end(); ++jter ) {
	  // the variable number 
	  const unsigned int var2 = jter->left; 

	  // loop through the number of degrees of freedom for the second variable
	  for( unsigned int j=0; j<elemDOFs->GetNumDOFs(var2); ++j ) {
	    // sum into element stiffness matrix
            elemDOFs->SumIntoElementStiffnessMatrix(var1, var2, i, j, JxW[qp] * resid.dx(offset + j));
          }

	  // this many DOFs were associated with this variable
	  offset += elemDOFs->GetNumDOFs(var2);
	}
      }
    }
  }
}

void PDE::ComputeResidual(shared_ptr<ElemInfo> const& elInfo, shared_ptr<ElementDOFs> const& elemDOFs, Eigen::VectorXd const& soln, int const inputDimWrt) 
{
  // get the number of variables
  const unsigned int nVars = vars.size();

  // the solutions for each variable
  vector<vector<double> > solnDOF;

  // extract the solution for each variable at each DOF
  ExtractVariableSolutions(solnDOF, soln, elemDOFs);

  // element jacobians (const reference to avoid the copy)
  const vector<libMesh::Real>& JxW = elInfo->JxW;
  
  // quadrature points on each element (const reference to avoid the copy)
  const vector<libMesh::Point>& qPoint = elInfo->qPoint;

  // loop over the quadrature points 
  assert(JxW.size() == qPoint.size());
  for( unsigned int qp=0; qp<qPoint.size(); ++qp ) {
    // compute the parameters as a FadType on the quadrature point
    ComputeParametersAtPoint(qPoint[qp], inputDimWrt);

    // the variables at this quad. point
    vector<double> varSoln(nVars);

    // the variable gradients at this quad. point 
    vector<vector<double> > varGrad(nVars);

    // loop over the variables 
    unsigned int v = 0;
    for( bimap::const_iterator iter=vars.begin(), iend=vars.end(); iter!=iend; ++iter ) {
      // current solution and gradient for this variable 
      const pair<double, vector<double> >& curr = InterpolateToQP<double, ElemInfo>(qp, iter->left, solnDOF[v], elInfo);

      // store the variable 
      varSoln[v] = curr.first;
      
      // store the gradient
      varGrad[v] = curr.second;

      // increment counter 
      ++v;
    }

    // loop over the variables 
    for( bimap::const_iterator iter=vars.begin(); iter!=vars.end(); ++iter ) {
      // the variable number 
      const unsigned int var = iter->left; 

      // basis functions on the element (const reference to avoid the copy)
      const vector<vector<libMesh::Real> >& phiV = elInfo->basisMap[feType[var]]->phi;

      // basis function derivatives on the element (const reference to avoid the copy)
      const vector<vector<libMesh::RealGradient> >& dphiV = elInfo->basisMap[feType[var]]->dphi;

      // loop through the basis functions 
      for( unsigned int j=0; j<phiV.size(); ++j ) {
	// compute the residual at the quad. point
	const FadType resid = ResidualImplPara(vars.left.at(var), phiV[j][qp], dphiV[j][qp], varSoln, varGrad);

	// add to the element residaul 
	elemDOFs->SumIntoElementVector(var, j, JxW[qp] * resid);
      }
    }
  }
}

void PDE::ComputeResidual(shared_ptr<ElemInfo> const& elInfo, shared_ptr<ElementDOFs> const& elemDOFs, Eigen::VectorXd const& soln)
{
  // get the number of variables
  const unsigned int nVars = vars.size();

  // the solutions for each variable
  vector<vector<double> > solnDOF;

  // extract the solution for each variable at each DOF
  ExtractVariableSolutions(solnDOF, soln, elemDOFs);

  // element jacobians (const reference to avoid the copy)
  const vector<libMesh::Real>& JxW = elInfo->JxW;
  
  // quadrature points on each element (const reference to avoid the copy)
  const vector<libMesh::Point>& qPoint = elInfo->qPoint;

  // loop over the quadrature points 
  assert(JxW.size() == qPoint.size());
  for( unsigned int qp=0; qp<qPoint.size(); ++qp ) {
    // interpolate the parameters at this point
    ComputeParametersAtPoint(qPoint[qp]);

    // the variables at this quad. point
    vector<double> varSoln(nVars);

    // the variable gradients at this quad. point 
    vector<vector<double> > varGrad(nVars);

    // loop over the variables 
    unsigned int v = 0;
    for( bimap::const_iterator iter=vars.begin(), iend=vars.end(); iter!=iend; ++iter ) {
      // current solution and gradient for this variable 
      const pair<double, vector<double> >& curr = InterpolateToQP<double, ElemInfo>(qp, iter->left, solnDOF[v], elInfo);

      // store the variable 
      varSoln[v] = curr.first;
      
      // store the gradient
      varGrad[v] = curr.second;

      // increment counter 
      ++v;
    }
    
    // loop over the variables 
    for( bimap::const_iterator iter=vars.begin(); iter!=vars.end(); ++iter ) {
      // the variable number 
      const unsigned int var = iter->left; 

      // basis functions on the element (const reference to avoid the copy)
      const vector<vector<libMesh::Real> >& phiV = elInfo->basisMap[feType[var]]->phi;

      // basis function derivatives on the element (const reference to avoid the copy)
      const vector<vector<libMesh::RealGradient> >& dphiV = elInfo->basisMap[feType[var]]->dphi;

      // loop through the basis functions 
      for( unsigned int j=0; j<phiV.size(); ++j ) {
	// compute the residual at the quad. point
	const double resid = ResidualImpl(vars.left.at(var), phiV[j][qp], dphiV[j][qp], varSoln, varGrad);

	// add to the element residaul 
	elemDOFs->SumIntoElementVector(var, j, JxW[qp] * resid);
      }
    }
  }
}

void PDE::ExtractVariableSolutions(vector<vector<double> > & solnDOF, Eigen::VectorXd const& soln, shared_ptr<ElementDOFs> const& elemDOFs) const 
{
  // get the number of variables 
  const unsigned int nVars = vars.size();

  // we have nVars solution vectors 
  solnDOF.resize(nVars);

    // variables 
  for( bimap::const_iterator iter=vars.begin(), iend=vars.end(); iter!=iend; ++iter ) {
    // the variable number 
    const unsigned int var = iter->left;
    
    // the solution vector for variable var
    solnDOF[var].resize(elemDOFs->GetNumDOFs(var));

    // loop through the degrees of freedom
    for( unsigned int dof=0; dof<elemDOFs->GetNumDOFs(var); ++dof ) {
      // get the solution for variable var at degree of freedom dof
      solnDOF[var][dof] = soln(elemDOFs->GetGlobalDOF(var, dof));
    }
  }
}

void PDE::ExtractVariableSolutions(vector<vector<FadType> > & solnDOF, Eigen::VectorXd const& soln, shared_ptr<ElementDOFs> const& elemDOFs) const 
{
  // get the number of variables 
  const unsigned int nVars = vars.size();

  // we have nVars solution vectors 
  solnDOF.resize(nVars);

  // offset each variable in the system 
  unsigned int offset = 0;

  // variables 
  for( bimap::const_iterator iter=vars.begin(), iend=vars.end(); iter!=iend; ++iter ) {
    // the variable number 
    const unsigned int var = iter->left;
    
    // the solution vector for variable var
    solnDOF[var].resize(elemDOFs->GetNumDOFs(var));

    // loop through the degrees of freedom
    for( unsigned int dof=0; dof<elemDOFs->GetNumDOFs(var); ++dof ) {
      // get the solution for variable var at degree of freedom dof
      solnDOF[var][dof] = FadType(elemDOFs->GetNumDOFs(), offset + dof, soln(elemDOFs->GetGlobalDOF(var, dof)));
    }

    // this many DOFs were associated with this variable
    offset += elemDOFs->GetNumDOFs(var);
  }
}

void PDE::EnforceMatrixBCs(Eigen::MatrixXd& matrix, bool const diag)
{
  // get a constant reference to the mesh 
  const libMesh::MeshBase& mesh = GetEquationSystemsPtr()->get_mesh();

  // get the system number
  const int sysNum = GetEquationSystemsPtr()->get_system(GetName()).number();

  // loop though the nodes
  for( unsigned int i=0; i<mesh.n_nodes(); ++i ) {
    // the current node
    libMesh::Node nd = mesh.node(i);
    
    if( IsDirichlet(nd) ) { // is the node on the boundary
      // loop through each variable
      for( bimap::const_iterator iter=vars.begin(), iend=vars.end(); iter!=iend; ++iter ) {
	// the variable number
	const unsigned int varNum = iter->left;

	// the components of the node
	for (unsigned int c = 0; c < nd.n_comp(sysNum, varNum); ++c) { 
          // global dof of this component
          const unsigned int dof = nd.dof_number(sysNum, varNum, c);
	  
          // enforce the dirichlet condition: zero out the row
          matrix.row(dof) = Eigen::VectorXd::Zero(matrix.cols());

	  if( diag ) { 
	    // the diagonal is one
	    matrix(dof, dof) = 1.0;
	  }
        }
      }
    }
  }
}

vector<double> PDE::NeumannBCs(string const& varName, libMesh::Point const& point, vector<double> const& soln, vector<vector<double> > const& grad) const
{
  return vector<double>(3, 0.0);
}

vector<FadType> PDE::NeumannBCs(string const& varName, libMesh::Point const& point, vector<FadType> const& soln, vector<vector<FadType> > const& grad) const
{
  return vector<FadType>(3, 0.0);
}

vector<FadType> PDE::NeumannBCsPara(string const& varName, libMesh::Point const& point, vector<double> const& soln, vector<vector<double> > const& grad) const
{
  return vector<FadType>(3, 0.0);
}

shared_ptr<libMesh::EquationSystems> PDE::GetEquationSystemsPtr() const
{
  return eqnsystem->GetEquationSystemsPtr();
}

boost::optional<vector<unsigned int> > PDE::ParameterElementDOFs(libMesh::MeshBase::const_element_iterator const& el, int const inputDimWrt) const
{
  // the index of the parameter
  const unsigned int paraIndex = inputDimWrt - (isTransient? 2 : 1);

  // is not the state
  if( inputDimWrt != isTransient? 1 : 0 ) {
    // the local->global DOF map
    vector<libMesh::dof_id_type> dofIndices;

    // is it a mesh dependent parameter?
    if( inputNames[paraIndex].first == meshDependent ) {
      // local-> global DOF mapping for the parameter of interest
      const libMesh::DofMap& dofMapPara = GetEquationSystemsPtr()->get_system(inputNames[paraIndex].second).get_dof_map();
      
      // get the dof indices
      dofMapPara.dof_indices((libMesh::Elem *)*el, dofIndices);
    } else {
      // it is not mesh dependent, initialize the vector
      dofIndices.resize(inputSizes(inputDimWrt));
      
      // a list of 0....N
      generate(dofIndices.begin(), dofIndices.end(), [] { int i {0}; return i++; });
    }
    
    // the number of local DOFs
    return dofIndices;
  }

  return boost::none;
}
