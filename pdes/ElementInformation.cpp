#include "MUQ/Pde/ElementInformation.h"
#include "MUQ/Utilities/RandomGenerator.h"
#include "boost/math/constants/constants.hpp"

using namespace std;
using namespace muq::Pde;



ElementInformation::ElementInformation(vector<libMesh::FEType> const& feType,
                                       unsigned int                   quadOrder,
                                       libMesh::MeshBase const      & mesh,
				       double useNoise
				      )
{
  // get the spatial dimension of the mesh
  const unsigned int dim = mesh.mesh_dimension();

  // create a quad rule
  libMesh::QGauss qrule(dim, libMesh::Order(quadOrder));
  libMesh::QGauss qface(dim - 1, libMesh::Order(quadOrder));

  // create FE base (pointers since the are dynimcally allocated for each element)
  vector<libMesh::AutoPtr<libMesh::FEBase> > feVec(feType.size());
  vector<libMesh::AutoPtr<libMesh::FEBase> > feFaceVec(feType.size());
  for (unsigned int i = 0; i < feType.size(); ++i) {
    // compute the fe base
    feVec[i]     = libMesh::FEBase::build(dim, feType[i]);
    feFaceVec[i] = libMesh::FEBase::build(dim, feType[i]);

    // attach the quad rule
    feVec[i]->attach_quadrature_rule(&qrule);
    feFaceVec[i]->attach_quadrature_rule(&qface);
  }

  libMesh::MeshBase::const_element_iterator el           = mesh.active_local_elements_begin();
  const libMesh::MeshBase::const_element_iterator end_el = mesh.active_local_elements_end();
  for (; el != end_el; ++el) {
    const libMesh::Elem *elem = *el;

    // compute the basis functions
    for (unsigned int i = 0; i < feType.size(); ++i) {
      feVec[i]->reinit(elem);
    } // fe type loop

    // store them
    auto info = make_shared<ElemInfo>(feType, feVec, useNoise, qrule.get_points());

    // push onto element vector
    elemVec.push_back(info);

    // loop over the sides
    for (unsigned int side = 0; side < elem->n_sides(); side++) {
      if (elem->neighbor(side) == NULL) { // is boundary?
        // compute the side basis functions
        for (unsigned int i = 0; i < feType.size(); ++i) {
          feFaceVec[i]->reinit(elem, side);
        }

        // store them
        auto face = make_shared<FaceInfo>(feType, feFaceVec);

        // push onto element vector
        faceVec.push_back(face);
      }
    }
  } // element loop
}

shared_ptr<FaceInfo> ElementInformation::GetFace(unsigned int const i) const
{
  return faceVec[i];
}

shared_ptr<ElemInfo> ElementInformation::Get(unsigned int const i) const
{
  return elemVec[i];
}

ElemInfo::ElemInfo(vector<libMesh::FEType> const& feType, vector<libMesh::AutoPtr<libMesh::FEBase> > const& feVec, double useNoise, std::vector<libMesh::Point> const& referenceQuadPoints)
{
  // the element Jacobian and quad points should be independent of the fe type
  JxW    = feVec[0]->get_JxW();
  qPoint = feVec[0]->get_xyz();

  for (unsigned int i = 0; i < feVec.size(); ++i) {
    // create a BasisInfo to store basis functions and their derivatives 
    auto basis = make_shared<BasisInfo>(feVec[i], referenceQuadPoints, useNoise);

    // put it into the map 
    basisMap[feType[i]] = basis;
  }
}

FaceInfo::FaceInfo(vector<libMesh::FEType> const& feType, vector<libMesh::AutoPtr<libMesh::FEBase> > const& feVec)
{
  // the element Jacobian, quad points, and normalsshould be independent of the fe type
  JxW     = feVec[0]->get_JxW();
  qPoint  = feVec[0]->get_xyz();
  normals = feVec[0]->get_normals();

  for (unsigned int i = 0; i < feVec.size(); ++i) {
    auto basis = make_shared<BasisInfo>(feVec[i], qPoint, 0.0);
    basisMap[feType[i]] = basis;
  }
}

BasisInfo::BasisInfo(libMesh::AutoPtr<libMesh::FEBase> const& fe, std::vector<libMesh::Point> const& qPoint, double useNoise)
{
  // get the shape functions
  phi  = fe->get_phi();

  // get their derivatives
  dphi = fe->get_dphi();
  
//   cout << "noisescale "  << " " << useNoise << " and " << qPoint.at(0).size() << endl;
  
  unsigned dim=2;
  
  if(useNoise != 0.0 && dim == 1){ //1D case
  //loop over the basis
  for(unsigned basis=0; basis<phi.size(); ++basis){

        unsigned numKl = 10;

        //first, draw randomness for this basis
    Eigen::VectorXd perturbation = muq::Utilities::RandomGenerator::GetNormalRandomVector(numKl);

    //loop over each quadrature point
for(unsigned qp =0; qp<qPoint.size(); ++qp){
    
//       cout << "qp " << qPoint.at(qp)(0) << endl;

//       cout << "basis was  " << phi.at(basis).at(qp) << endl;
    
    
//     cout << "perturb " << useNoise*muq::Pde::noiseScale*perturbation(0)*(qPoint.at(qp)(0)*qPoint.at(qp)(0) - 1) << endl;
    
//     phi.at(basis).at(qp) += useNoise*muq::Pde::noiseScale*perturbation(0)*(qPoint.at(qp)(0)*qPoint.at(qp)(0) - 1);
    
//     dphi.at(basis).at(qp)(0) += useNoise*muq::Pde::noiseScale*perturbation(0)*(qPoint.at(qp)(0)*2.0);
    
    
    
    for(unsigned kl=0; kl<numKl; ++kl){
//      cout << "kl " << kl << " " << noiseScale*perturbation(kl) * pow(kl+1.0, -.5) * cos(kl*qPoint.at(qp)(0)*boost::math::constants::pi<double>()) << endl;
      phi.at(basis).at(qp) += useNoise*perturbation(kl) * pow(kl+1.0, -2) * cos(kl*qPoint.at(qp)(0)*boost::math::constants::pi<double>());
      
      dphi.at(basis).at(qp)(0) += useNoise*perturbation(kl) * pow(kl+1.0, -2) * -1.0 * sin(kl*qPoint.at(qp)(0)*boost::math::constants::pi<double>())*kl*boost::math::constants::pi<double>();
      
    }
    
//           cout << "basis now  " << phi.at(basis).at(qp) << endl;

  }
  }
  }
    else if(useNoise != 0.0 && dim == 2){
  //loop over the basis
  for(unsigned basis=0; basis<phi.size(); ++basis){

        unsigned numKl = 10;

        //first, draw randomness for this basis
    Eigen::VectorXd perturbation = muq::Utilities::RandomGenerator::GetNormalRandomVector(numKl*numKl);


    //loop over each quadrature point
for(unsigned qp =0; qp<qPoint.size(); ++qp){
    
//       cout << "qp " << qPoint.at(qp)(0) << endl;

//       cout << "basis was  " << phi.at(basis).at(qp) << endl;
    
    
//     cout << "perturb " << useNoise*muq::Pde::noiseScale*perturbation(0)*(qPoint.at(qp)(0)*qPoint.at(qp)(0) - 1) << endl;
    
//     phi.at(basis).at(qp) += useNoise*muq::Pde::noiseScale*perturbation(0)*(qPoint.at(qp)(0)*qPoint.at(qp)(0) - 1);
    
//     dphi.at(basis).at(qp)(0) += useNoise*muq::Pde::noiseScale*perturbation(0)*(qPoint.at(qp)(0)*2.0);
    
    
    unsigned klIndex =0;
    for(unsigned kl1=0; kl1<numKl; ++kl1){
      for(unsigned kl2=0; kl2<numKl; ++kl2){
//      cout << "kl " << kl << " " << noiseScale*perturbation(kl) * pow(kl+1.0, -.5) * cos(kl*qPoint.at(qp)(0)*boost::math::constants::pi<double>()) << endl;
      
	phi.at(basis).at(qp) += useNoise*perturbation(klIndex) * pow(kl1+1.0, -.5) * cos(kl1*qPoint.at(qp)(0)*boost::math::constants::pi<double>())
							*pow(kl2+1.0, -.5) * cos(kl2*qPoint.at(qp)(0)*boost::math::constants::pi<double>()) ;
      
      dphi.at(basis).at(qp)(0) += useNoise*perturbation(klIndex) * pow(kl1+1.0, -.5) * -1.0 * sin(kl1*qPoint.at(qp)(0)*boost::math::constants::pi<double>())*kl1*boost::math::constants::pi<double>()
											    *pow(kl2+1.0, -.5) * cos(kl2*qPoint.at(qp)(0)*boost::math::constants::pi<double>());
      dphi.at(basis).at(qp)(1) += useNoise*perturbation(klIndex) * pow(kl1+1.0, -.5) *pow(kl1+1.0, -.5) * cos(kl1*qPoint.at(qp)(0)*boost::math::constants::pi<double>())
							      * -1.0 * sin(kl2*qPoint.at(qp)(0)*boost::math::constants::pi<double>())*kl2*boost::math::constants::pi<double>();      
      
      ++klIndex;
    }
    }
    
//           cout << "basis now  " << phi.at(basis).at(qp) << endl;

  }
  }
  }
}

ElementDOFs::ElementDOFs(unsigned int const nVars)
{
  KeSub.resize(nVars);    // unknown interaction submatrices
  MeSub.resize(nVars);    // unknown interaction submatrices
  FeSub.resize(nVars);    // unknown subvectors
  FeSubFad.resize(nVars); // sacado type

  for (unsigned int i = 0; i < nVars; ++i) {
    KeSub[i].resize(nVars);
    MeSub[i].resize(nVars);
    FeSub[i]    = make_shared<libMesh::DenseSubVector<libMesh::Number> >(Fe);
    FeSubFad[i] = make_shared<libMesh::DenseSubVector<FadType> >(FeFad);
    for (unsigned int j = 0; j < nVars; ++j) {
      KeSub[i][j] = make_shared<libMesh::DenseSubMatrix<libMesh::Number> >(Ke);
      MeSub[i][j] = make_shared<libMesh::DenseSubMatrix<libMesh::Number> >(Me);
    }
  }

  // vectors for DOFs
  dofIndicesVar.resize(nVars); // each unknown -> global
}

void ElementDOFs::Reset(libMesh::MeshBase::const_element_iterator const& el, bimap const& vars, libMesh::DofMap const& dofMap)
{
  const libMesh::Elem *elem = *el;

  // get DOFs
  dofMap.dof_indices(elem, dofIndices);               
  // for each unknown 
  unsigned int cnt = 0;
  for( bimap::const_iterator iter=vars.begin(), iend=vars.end(); iter!=iend; ++iter ) {
    dofMap.dof_indices(elem, dofIndicesVar[cnt++], iter->left);
  }

  // reset the element Matrices and RHS
  Ke.resize(dofIndices.size(), dofIndices.size());
  Me.resize(dofIndices.size(), dofIndices.size());
  Fe.resize(dofIndices.size());
  FeFad.resize(dofIndices.size());

  // reposition the submatices/subvectors
  unsigned int offsetV1 = 0;

  for( bimap::const_iterator iter=vars.begin(), iend=vars.end(); iter!=iend; ++iter ) {
    unsigned int v1 = iter->left;
    FeSub[v1]->reposition(offsetV1, dofIndicesVar[v1].size());
    FeSubFad[v1]->reposition(offsetV1, dofIndicesVar[v1].size());
    unsigned int offsetV2 = 0;

    for( bimap::const_iterator jter=vars.begin(), jend=vars.end(); jter!=jend; ++jter ) {
      unsigned int v2 = jter->left;
      KeSub[v1][v2]->reposition(offsetV1, offsetV2, dofIndicesVar[v1].size(), dofIndicesVar[v2].size());
      MeSub[v1][v2]->reposition(offsetV1, offsetV2, dofIndicesVar[v1].size(), dofIndicesVar[v2].size());
      offsetV2 += dofIndicesVar[v2].size();
    }
    offsetV1 += dofIndicesVar[v1].size();
  }
}

void ElementDOFs::SumIntoElementVector(unsigned int const var, unsigned int const dof, double const val)
{
  FeSub[var]->operator()(dof) += val;
}

void ElementDOFs::SumIntoElementVector(unsigned int const var, unsigned int const dof, FadType const val)
{
  FeSubFad[var]->operator()(dof) += val;
}

void ElementDOFs::SumIntoElementStiffnessMatrix(unsigned int const var1,
                                                unsigned int const var2,
                                                unsigned int const dof1,
                                                unsigned int const dof2,
                                                double const       val)
{
  KeSub[var1][var2]->operator()(dof1, dof2) += val;
}

void ElementDOFs::SumIntoElementMassMatrix(unsigned int const var1,
                                           unsigned int const var2,
                                           unsigned int const dof1,
                                           unsigned int const dof2,
                                           double const       val)
{
  MeSub[var1][var2]->operator()(dof1, dof2) += val;
}

libMesh::dof_id_type ElementDOFs::GetGlobalDOF(unsigned int const var, unsigned int const dof) const
{
  return dofIndicesVar[var][dof];
}

unsigned int ElementDOFs::GetNumDOFs(unsigned int const var) const
{
  return dofIndicesVar[var].size();
}

unsigned int ElementDOFs::GetNumDOFs() const
{
  return dofIndices.size();
}

libMesh::DenseMatrix<libMesh::Number> ElementDOFs::GetElementStiffnessMatrix() const
{
  return Ke;
}

libMesh::DenseMatrix<libMesh::Number> ElementDOFs::GetElementMassMatrix() const
{
  return Me;
}

libMesh::DenseVector<libMesh::Number> ElementDOFs::GetElementVector() const
{
  return Fe;
}

libMesh::DenseVector<FadType> ElementDOFs::GetElementVectorAD() const
{
  return FeFad;
}

vector<libMesh::dof_id_type> ElementDOFs::GetLocalToGlobalMap() const
{
  return dofIndices;
}

