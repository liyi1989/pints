# -*- coding: utf-8 -*-
"""
Created on Tue Jan 27 10:16:27 2015

@author: prconrad
"""

import sys

sys.path.append('/Users/patrick/muq_install/lib')
sys.path.append('/Users/patrick/Dropbox/Development/examples/build')
sys.path.append('/Users/patrick/Dropbox/Development/examples/pdes/build')
sys.path.append('/Users/patrick/Dropbox/Development/examples')

# sys.path.append('/home/prconrad/local/lib')
# sys.path.append('/home/prconrad/local/muq_external/lib')
# sys.path.append('/media/extradata/Dropbox/Development/examples/odes/build')
# sys.path.append('/media/extradata/Dropbox/Development/examples')


import libmuqUtilities
import libmuqModelling
import libmuqInference
import libmuqApproximation
import libmuqPde
import libRandomPdes
import numpy as np
import matplotlib.pyplot as plt
import plotTools




def plotRandomPathsFwd(gridSize, noise, N=100, order=1, color='blue' ,alpha=.3, width=1, refGrid=20):
    import plotTools








    x = np.linspace(0.0, 1.0, gridSize+1)





    for j in range(N):
        seed = libmuqUtilities.RandomGeneratorTemporarySetSeed(74768+j)
        print "pos", seed
        solver = libRandomPdes.MakeEllipticProblem(noise, str(order), gridSize, True)
        onePath = solver.Evaluate([])
        print seed
        seed2 = libmuqUtilities.RandomGeneratorTemporarySetSeed(74768+j)
        print "neg", seed
        solver = libRandomPdes.MakeEllipticProblem(-noise, str(order), gridSize, True)
        onePathNeg = solver.Evaluate([])
        print seed2
        seed=[]
        seed2=[]

        print "noisefree"
        solver = libRandomPdes.MakeEllipticProblem(0, str(1), refGrid, True)
        unperturbedPath = solver.Evaluate([])
        print np.array(onePath)
        print np.array(unperturbedPath)[::10]
        if gridSize==120:
            x = np.linspace(0.0, 1.0, refGrid+1)
            # print "a", np.array(unperturbedPath[::])
            # print "b", 120/gridSize, np.array(onePath[::120/gridSize])

            plt.plot(x, np.array(onePath[::120/refGrid])-np.array(unperturbedPath[::]),'-',color=plotTools.PlotColor(color),alpha=alpha, linewidth=width)
        else:
            plt.plot(x, np.array(onePath)-np.array(unperturbedPath),'-',color=plotTools.PlotColor(color),alpha=alpha, linewidth=width)
            plt.plot(x, np.array(onePathNeg)-np.array(unperturbedPath),'-',color=plotTools.PlotColor(color),alpha=alpha, linewidth=width)


        # plt.plot(x, np.array(onePath[::]),'-',color=plotTools.PlotColor(color),alpha=alpha, linewidth=width)
        # plt.plot(x, np.array(onePathNeg),'-',color=plotTools.PlotColor(color),alpha=alpha, linewidth=width)
        # plt.plot(x, np.array(unperturbedPath),'-',color=plotTools.PlotColor(color),alpha=alpha, linewidth=width)



def plotRandomPathsInv(params,  gridSize, noise, N=100, order=1, color='blue' ,alpha=.3, width=1, refGrid=20):
    import plotTools

    randomsubset = np.random.choice(range(params.shape[1]), size=[N])





    x = np.linspace(0.0, 1.0, refGrid+1)

    for i in randomsubset:
        solver = libRandomPdes.MakeEllipticProblemInv(noise, str(order), gridSize, refGrid,  True)

        startPoint = [0,0,0,1,0,0]
        # onePath = solver.Evaluate([params[:,i].tolist()])
        print "eval!"
        onePath = solver.Evaluate([startPoint])
        plt.plot(x, np.array(onePath),'-',color=plotTools.PlotColor(color),alpha=alpha, linewidth=width)



def plotForwardPredictive(stepInt , presentationVersion, saveToFile , N):
    import h5py
    import CompareIncrementalCovariances_2
    # hdf5filename = 'results/elliptic1d_fwd.h5_rank_' + str(stepInt-1)

    # f = h5py.File(hdf5filename,"r")

    if stepInt == 1:
        stepSize = 10
    elif stepInt == 2:
        stepSize = 20
    elif stepInt == 3:
        stepSize = 30
    elif stepInt == 4:
        stepSize = 40
    elif stepInt == 5:
        stepSize = 60


    # chain = np.array(CompareIncrementalCovariances_2.ReferenceChain(hdf5filename,'Elliptic1D'))[:,:]


    # if presentationVersion:
    #     fig = plotTools.MakeFigure(600,.7, presentationVersion)
    # else:
    #     fig = plotTools.MakeFigure(425,.9, presentationVersion)

    # randomsubset = np.random.choice(chain[0,:], size=[N])
    # for i in range(N):


    plotRandomPathsFwd(stepSize, pow(10.0, -.25), N=N, color='medium blue', width=.5 , alpha=.5,refGrid=stepSize)
        # plotRandomPathsFwd(2*stepSize, pow(10.0, randomsubset[i]), N=1, color='purple' )

    # plotRandomPathsFwd(stepSize, 0.0, N=1, color='dark blue', alpha=1, width=2 )
    plotRandomPathsFwd(stepSize, 0.0, N=1, color='red', alpha=1, width=2, order=2, refGrid=stepSize )
    plotRandomPathsFwd(120, 0.0, N=1, color='orange', alpha=1, width=2, order=2, refGrid=stepSize )


    plt.xlabel('$x$')
    plt.ylabel('$u$')
    # plt.legend(['ln', '2h'])
#     fig.subplots_adjust(wspace=.15)
#
#
    # if saveToFile:
    #     if presentationVersion:
    #         plt.savefig('plots/fitz_fwdPredictive' + str(stepInt) + '.pdf', format='pdf')
    #     else:
    #         plt.savefig('plots/fitz_noise_survey_paper.pdf', format='pdf')
       # plt.close(fig)


def plotForwardPredictiveSummary(presentationVersion, saveToFile , N):
    if presentationVersion:
        fig = plotTools.MakeFigure(1000,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(425,.9, presentationVersion)



    plt.subplot(5,1,1)
    plotForwardPredictive(1, True, False, N)
    plt.title("$n=10$")
    # plt.subplot(5,1,2)
    # plotForwardPredictive(2, True, False, N)


    plt.subplot(5,1,2)
    plotForwardPredictive(2, True, False, N)
    plt.title("$n=20$")


    # plt.subplot(5,1,4)
    # plotForwardPredictive(4, True, False, N)


    plt.subplot(5,1,3)
    plotForwardPredictive(3, True, False, N)
    plt.title("$n=30$")

    plt.subplot(5,1,4)
    plotForwardPredictive(4, True, False, N)
    plt.title("$n=40$")

    plt.subplot(5,1,5)
    plotForwardPredictive(5, True, False, N)
    plt.title("$n=60$")
    fig.tight_layout(pad=0.2)


def plotData():
    x = np.linspace(0.0, 1.0, 21)

    y = np.loadtxt('data/obs_1d.dat')

    plt.plot(x[1:20],y, '.')

def plotPosteriorPredictiveSummary(presentationVersion, saveToFile , N, randomVersion):
    if presentationVersion:
        fig = plotTools.MakeFigure(1000,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(425,.9, presentationVersion)

    if randomVersion:
        noise = pow(10, -1.5)
    else:
        noise = 0.;

    import CompareIncrementalCovariances_2

    plt.subplot(4,1,1)
    grid=20
    if randomVersion:
        chain = np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_inv3.h5_rank_4','Elliptic'))[:,::]
    else:
        chain = np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_inv3.h5_rank_0','Elliptic'))[:,::]

    plotRandomPathsInv(chain,  grid, noise, N=N, order=1, color='blue' ,alpha=.3, width=1, refGrid=grid)
    plotData()
    plt.title("$n=" + str(grid)+"$")

    plt.subplot(4,1,2)
    grid=40
    if randomVersion:
        chain = np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_inv3.h5_rank_5','Elliptic'))[:,::]
    else:
        chain = np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_inv3.h5_rank_1','Elliptic'))[:,::]

    plotRandomPathsInv(chain,  grid, noise, N=N, order=1, color='blue' ,alpha=.3, width=1, refGrid=grid)
    plotData()
    plt.title("$n=" + str(grid)+"$")

    plt.subplot(4,1,3)
    grid=60
    if randomVersion:
        chain = np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_inv3.h5_rank_6','Elliptic'))[:,::]
    else:
        chain = np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_inv3.h5_rank_2','Elliptic'))[:,::]

    plotRandomPathsInv(chain,  grid, noise, N=N, order=1, color='blue' ,alpha=.3, width=1, refGrid=grid)
    plotData()
    plt.title("$n=" + str(grid)+"$")


    plt.subplot(4,1,4)
    grid=80
    if randomVersion:
        chain = np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_inv3.h5_rank_7','Elliptic'))[:,::]
    else:
        chain = np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_inv3.h5_rank_3','Elliptic'))[:,::]

    plotRandomPathsInv(chain,  grid, noise, N=N, order=1, color='blue' ,alpha=.3, width=1, refGrid=grid)
    plotData()
    plt.title("$n=" + str(grid)+"$")







def PlotStepSequence(presentationVersion, saveToFile, subsample=100):
    #hdf5File = 'results/' + exampleName + '.h5'
    import CompareIncrementalCovariances_2
    all_data = [np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_paper_fwd.h5_rank_0','Elliptic'))[:,::],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_paper_fwd.h5_rank_1','Elliptic'))[:,::],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_paper_fwdPart2.h5_rank_0','Elliptic'))[:,::],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_paper_fwdPart2.h5_rank_1','Elliptic'))[:,::],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_paper_fwdPart2.h5_rank_2','Elliptic'))[:,::]
                ]
    referenceName = 'fitz_fwd'

    import kde_matrix
    labels = []

    boundaries_list = []
    contour_labels = ['n=10', 'n=20', 'n=40', 'n=60', 'n=80']

    for i in range( all_data[0].shape[0]):
        labels.append('$\\theta_' + str(i+1) + '$')


    label_separation_x = 0.1

    if presentationVersion:
       fig = plotTools.MakeFigure(1200,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(600,.9, presentationVersion)
    kde_matrix.kde_matrix(all_data, labels=labels, ax=plt.gca(), label_separation_x=label_separation_x,contour_labels=contour_labels,
                          boundaries_list=boundaries_list,kde_subsample=10, scatter_subsample=10, usescatter=False)



def PlotStepSequenceInv(presentationVersion, saveToFile, randomVersion):
    #hdf5File = 'results/' + exampleName + '.h5'
    import CompareIncrementalCovariances_2


    if randomVersion:
        all_data = [
            np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_rand10v2.h5_rank_0','Elliptic'))[0:4,::],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_rand10v2.h5_rank_1','Elliptic'))[0:4,::],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_rand10v2.h5_rank_2','Elliptic'))[0:4,::],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_rand10v2.h5_rank_3','Elliptic'))[0:4,::],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_rand10v2.h5_rank_4','Elliptic'))[0:4,::]
                ]
    else:
        all_data = [
            np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_det10.h5_rank_0','Elliptic'))[0:4,::],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_det10.h5_rank_1','Elliptic'))[0:4,::],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_det10.h5_rank_2','Elliptic'))[0:4,::],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_det10.h5_rank_3','Elliptic'))[0:4,::],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/elliptic1d_det10.h5_rank_4','Elliptic'))[0:4,::]
                ]


    referenceName = 'Elliptic'

    import kde_matrix
    labels = []

    boundaries_list = []
    contour_labels = ['n=10', 'n=20', 'n=40', 'n=60', 'n=80']

    for i in range( all_data[0].shape[0]):
        labels.append('$\\theta_' + str(i+1) + '$')


    label_separation_x = 0.2

    boundaries_list = [[-4,1],[-5,2],[-1,3],
        [-2,2],[-4,1],[-5,1],
        [-2,4],[-5,2],[-5,5]]
    if presentationVersion:
       fig = plotTools.MakeFigure(800,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(600,.9, presentationVersion)
    kde_matrix.kde_matrix(all_data, labels=labels, ax=plt.gca(), label_separation_x=label_separation_x,contour_labels=contour_labels,
                          boundaries_list=boundaries_list,kde_subsample=20, scatter_subsample=10, usescatter=False)



#    fig.tight_layout(pad=0.1)


    if saveToFile:
        if presentationVersion:
            plt.savefig('plots/fitz_fwdScalePosterior.pdf', format='pdf')
        else:
            plt.savefig('plots/' + referenceName + '_perfect_density_paper.pdf', format='pdf')
#        plt.close(fig)



def plotStepSurvey( presentationVersion, saveToFile ):

    if presentationVersion:
        fig = plotTools.MakeFigure(600,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(425,.9, presentationVersion)

    steps = [5,10,20,40,80,160]
    for i in range(len(steps)):
        plt.subplot(3,2,i+1)
        # (gridSize, noise, N=100, order=1, color='blue' ,alpha=.3, width=1)
        plotRandomPathsFwd(steps[i], .5, N=10, order=1, color='blue' ,alpha=.3, width=1, refGrid=steps[i])
        # plotRandomPathsFwd(200, 0, N=1, order=1, color='orange' ,alpha=1, width=1)
        plt.xticks([0,.5,1.0])
        # plt.yticks([-.050,.0,.05])
        if steps[i] < .001:
            plt.title('$h=' + str(steps[i]*1000) + '\\times 10^{-3}$' )
        else:
            plt.title('$h=' + str(steps[i]) + '$' )
        if i+1==5 or i+1 == 6:
            plt.xlabel('$x$')
        else:
#            plt.xlim([0,20])
            plt.gca().set_xticklabels([])

#            plt.gca().xaxis.set_visible(False)
        if i+1==1 or i +1== 3 or i+1==5:
            plt.ylabel('$u$')
        # else:
#            plt.ylim([-4,4])
#             plt.gca().set_yticklabels([])
#            plt.gca().yaxis.set_visible(False)

    fig.tight_layout(pad=0.2)
    fig.subplots_adjust(wspace=.15)


    if saveToFile:
        if presentationVersion:
            plt.savefig('plots/elliptic1d_step_survey_presentation.pdf', format='pdf')
        else:
            plt.savefig('plots/elliptic1d_step_survey_paper.pdf', format='pdf')
#        plt.close(fig)



def plotNoiseSurvey( presentationVersion, saveToFile ):

    if presentationVersion:
        fig = plotTools.MakeFigure(600,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(425,.9, presentationVersion)

    sigmas = [8,2,1,.5,.2,.1,.05,.02,.005]
    for i in range(len(sigmas)):
        plt.subplot(3,3,i+1)

        plotRandomPathsFwd(20, sigmas[i], N=5, order=1, color='blue' ,alpha=.3, width=1)
        # plotRandomPathsFwd(200, 0, N=1, order=1, color='orange' ,alpha=1, width=1)
        # plotRandomPathsFwd(5, 0, N=1, order=1, color='red' ,alpha=1, width=1)

        plt.xticks([0,.5,1.0])
        # plt.yticks([0,1,2,3])

        if sigmas[i] < .001:
            plt.title('$\\sigma=' + str(sigmas[i]*1000) + '\\times 10^{-3}$' )
        else:
            plt.title('$\\sigma=' + str(sigmas[i]) + '$' )
        if i+1==7 or i+1 == 8 or i+1==9:
            plt.xlabel('$x$')
        else:
#            plt.xlim([0,20])
            plt.gca().set_xticklabels([])

#            plt.gca().xaxis.set_visible(False)
        if i+1==1 or i +1== 4 or i+1==7:
            plt.ylabel('$u$')
        # else:
#            plt.ylim([-4,4])
#             plt.gca().set_yticklabels([])
#            plt.gca().yaxis.set_visible(False)

    fig.tight_layout(pad=0.2)
    fig.subplots_adjust(wspace=.15)


    if saveToFile:
        if presentationVersion:
            plt.savefig('plots/elliptic1d_noise_survey_presentation.pdf', format='pdf')
        else:
            plt.savefig('plots/elliptic1d_noise_survey_paper.pdf', format='pdf')
#        plt.close(fig)



pdeinit = libmuqPde.LibMeshInit(sys.argv)



# plotRandomPathsFwd(5, .002, N=1, order=1, color='blue' ,alpha=.3, width=1)


# plotStepSurvey(True, False)
# # plotStepSurvey(False, True)
# plotNoiseSurvey(True, False)
# # plotNoiseSurvey(False, True)

PlotStepSequence(True, False, 1)
#
# PlotStepSequenceInv(True, False, False)
# plt.savefig('plots/elliptic1d_inv_det_density.pdf', format='pdf')
# PlotStepSequenceInv(True, False, True)
# plt.savefig('plots/elliptic1d_inv_rand_density.pdf', format='pdf')
# plt.savefig('plots/elliptic_noise_density.pdf', format='pdf')

# plotDataAndAccuracy(True, True)
# plotDataAndAccuracy(False, True)


# plotForwardPredictive(1, True, False, 100)
# plotForwardPredictive(2, True, False, 100)
# plotForwardPredictive(3, True, False, 100)
# plotForwardPredictive(4, True, False, 100)
# plotForwardPredictive(5, True, False, 100)

# plotForwardPredictiveSummary(True, False, 15)
# plt.savefig('plots/elliptic_noise_summary.pdf', format='pdf')

# plotPosteriorPredictiveSummary(True, False , N=1, randomVersion=False)
# plotPosteriorPredictiveSummary(True, False , N=100, randomVersion=True)

plt.show()

