This repository contains the code supplement to "Probability Measures for Numerical Solutions of 
Differential Equations" by Patrick R. Conrad, Mark Girolami, Simo S\"{a}rkk\"{a}, Andrew Stuart,
and Konstantinos Zygalakis.
  
The code is primarily provided for archival purposes. The two main .cpp files have the implementation
of the randomised solvers and the python scripts run the various examples. While these 
may be useful in resolving any questions about the exact experiments performed, they are not easily
run, nor are cleanly implemented. Most of the code is simply scripting for various intermediate 
experiments and plotting.

They rely on the MIT Uncertainty Quantification library, https://bitbucket.org/mituq/muq, which 
needs to be built with PDEs, as appropriate. The pde directory contains two replacements for 
the PDE module, which insert randomness into the basis functions. They're based off of commit 
a9a2d1753463913f7a61ed0e6db62657a986a775.

Since the ODE algorithm is so simple, requiring only the addition of some random numbers, we 
encourage readers to implement it themselves. I would be happy to field implementation questions,
here: p dot conrad at warwick.ac.uk