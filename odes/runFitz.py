import sys

sys.path.append('/Users/patrick/muq_install/lib')
sys.path.append('/Users/patrick/Dropbox/Development/examples/build')
sys.path.append('/Users/patrick/Dropbox/Development/examples/odes/build')
sys.path.append('/Users/patrick/Dropbox/Development/examples')

# sys.path.append('/home/prconrad/local/lib')
# sys.path.append('/home/prconrad/local/muq_external/lib')
# sys.path.append('/media/extradata/Dropbox/Development/examples/odes/build')

import libmuqModelling

import libmuqInference

import libmuqUtilities
import libmuqApproximation
import libmuqPde
import libRandomOdes
import numpy as np
import matplotlib.pyplot as plt

#import plotTools

from mpi4py import MPI






def RunOneFitz(runIndex,mcmcLength,stepInt, randomSolver):
    runName = 'Fitz_'




    runName += randomSolver + '_'

    runName += str(stepInt)+'_'

    runName  += str(runIndex)

    print "Computing key ", runName

    params = dict()
    
    finalTime = 40.0
    
  
    if stepInt == 1:
        stepSize = .1
    elif stepInt == 2:
        stepSize = .05
    elif stepInt == 3:
        stepSize = .02
    elif stepInt == 4:
        stepSize = .01
    elif stepInt == 5:
        stepSize = .005
    else:
        print "no step size found, exiting!"
        sys.exit();

        
    # create MCMC instance
    params["MCMC.Steps"]            = mcmcLength-1

    params["MCMC.Method"]           = "SingleChainMCMC"
    if randomSolver:
        params["MCMC.Kernel"] = "PseudomarginalMHKernel"
    else:
        params["MCMC.Kernel"] = "DR"

    params["MCMC.Proposal"] = "AM"

    params["MCMC.BurnIn"]           = 1
    params["MCMC.MHProposal.PropSize"]      = 1e-2
    params["MCMC.DR.stages"]     = 3
    params["MCMC.DR.scale"]     = 10.0
    
    params["MCMC.AM.AdaptSteps"] = 50
    params["MCMC.AM.AdaptStart"] = 200
    params["Verbose"]               = 5

    params["MCMC.HDF5OutputDetails"] = 1
    params["MCMC.HDF5OutputGroup"] = "/"+runName
    params["HDF5.OutputGroup"] = "/"+runName


    #else, continue by starting up MUQ
    # start logging
#    libmuqUtilities.InitializeLogging(["geneticsPython"])
    # open an hdf5

    # if randomSolver:
    #
    #     startPoint = [.2,.2,3,0]
    #
    #     priorMean = startPoint
    #     priorVar = [1,1,1,10]
    # else:
    startPoint = [.2,.2,3]

    priorMean = startPoint
    priorVar = [1,1,1]

#    stateSize = 4
#    paramDim = stateSize






    finishTime = finalTime

    #numSteps = finishTime/stepSize+1
#    times = np.arange(0, finishTime+1E-14, stepSize).transpose()

    #numRuns = 100;
    obsInterval = 1
#    numObsTimes = int(finalTime/obsInterval)
#    numObs = numObsTimes*stateSize
    #obsTimes = np.arange(obsInterval, finishTime+1E-14, obsInterval).transpose()


    data = np.loadtxt('FitzDataRk4_fine.dat')

    
#    print np.reshape(allData, [stateSize, 40],'F')
#    data = np.reshape(np.reshape(allData, [stateSize, 40],'F')[:,0:numObsTimes], [numObs,1],'F').transpose().tolist()[0]
    dataCov = .001

    if randomSolver == 'rand':
        print 'rand'
        FitzSampling = libRandomOdes.FitzSamplingProblem(priorMean, priorVar, data.tolist(), dataCov, 100, stepSize, obsInterval, finishTime, pow(10, -.2 ))
    elif randomSolver == 'det':
        print 'det'
        FitzSampling = libRandomOdes.FitzSamplingProblemDeterministic(priorMean, priorVar, data.tolist(), dataCov, stepSize, obsInterval, finishTime)
    elif randomSolver == 'ind':
        print 'ind'
        FitzSampling = libRandomOdes.FitzSamplingProblemIndicator(priorMean, priorVar, data.tolist(), dataCov,100, stepSize, obsInterval, finishTime)


   

    mcmc = libmuqInference.MCMCBase(FitzSampling, params)
    mcmc.Sample(startPoint)


    #fig = plotTools.MakeFigure(1200,.7, True)


    # for j in range(numRuns):
    #     onePath = solver.Evaluate(startPoint)
    #
    #     pathMatrix = np.reshape(np.array(onePath), [stateSize, numSteps],'F')
    #
    #     for i in range(stateSize):
    #         plt.subplot(stateSize, 1, i+1)
    #         plt.plot(times, pathMatrix[i,:],plotTools.PlotColor('blue'),alpha=.2)
    #
    #
    # onePath = detsolver.Evaluate(startPoint)
    #
    # pathMatrix = np.reshape(np.array(onePath), [stateSize, finishTime/obsInterval],'F')
    #
    # for i in range(stateSize):
    #     plt.subplot(stateSize, 1, i+1)
    #     plt.plot(obsTimes, pathMatrix[i,:],'o',color=plotTools.PlotColor('red'),alpha=.6)
    # plt.show()



    #detsolver = libRandomOdes.EulerOdeSolver(FitzRhs, stepSize, finishTime, obsInterval)
    #trueData = detsolver.Evaluate([startPoint])
    #print trueData



def RunOneFitzForward(runIndex,mcmcLength,stepInt):
    runName = 'Fitz_'



    runName += "forwardScale_"
    runName += str(stepInt)+'_'

    runName  += str(runIndex)

    print "Computing key ", runName

    params = dict()

    finalTime = 40.0


    if stepInt == 1:
        stepSize = .1
    elif stepInt == 2:
        stepSize = .05
    elif stepInt == 3:
        stepSize = .02
    elif stepInt == 4:
        stepSize = .01
    elif stepInt == 5:
        stepSize = .005
    else:
        print "no step size found, exiting!"
        sys.exit();


    # create MCMC instance
    params["MCMC.Steps"]            = mcmcLength-1

    params["MCMC.Method"]           = "SingleChainMCMC"

    params["MCMC.Kernel"] = "PseudomarginalMHKernel"

    params["MCMC.Proposal"] = "AM"

    params["MCMC.BurnIn"]           = 1
    params["MCMC.MHProposal.PropSize"]      = 1e-3
    params["MCMC.DR.stages"]     = 3
    params["MCMC.DR.scale"]     = 10.0

    params["MCMC.AM.AdaptSteps"] = 50
    params["MCMC.AM.AdaptStart"] = 200
    params["Verbose"]               = 5

    params["MCMC.HDF5OutputDetails"] = 1
    params["MCMC.HDF5OutputGroup"] = "/"+runName
    params["HDF5.OutputGroup"] = "/"+runName


    #else, continue by starting up MUQ
    # start logging
#    libmuqUtilities.InitializeLogging(["geneticsPython"])
    # open an hdf5



    startPoint = [-1]





    finishTime = finalTime

    obsInterval = .2

    FitzSampling = libRandomOdes.FitzForwardScaleProblem(1e-2, 30, stepSize, obsInterval, finishTime)



    mcmc = libmuqInference.MCMCBase(FitzSampling, params)
    mcmc.Sample(startPoint)





#
def PlotPosteriorSurface(randomSolver, stepInt):
    runName = 'fitz_'

    if randomSolver:
        runName += "randomSolver_"
    else:
        runName += "deterministicSolver_"

    runName += str(stepInt)+'_'


    print "Computing key ", runName

    params = dict()

    finalTime = 40.0
    obsInterval = 1.0

    if stepInt == 1:
        stepSize = .1
        noiseExp = -.6
    elif stepInt == 2:
        stepSize = .05
        noiseExp = -.4

    elif stepInt == 3:
        stepSize = .02
        noiseExp = -.4



    if randomSolver:

        startPoint = [.2,.2,3]

        priorMean = [.2,.2,3,0]
        priorVar = [1,1,1,1]
    else:
        startPoint = [.2,.2,3]

        priorMean = startPoint
        priorVar = [1,1,1]


    data = np.loadtxt('FitzDataRk4_fine.dat')


#    print np.reshape(allData, [stateSize, 40],'F')
#    data = np.reshape(np.reshape(allData, [stateSize, 40],'F')[:,0:numObsTimes], [numObs,1],'F').transpose().tolist()[0]
    dataCov = .0001

    if randomSolver:
        FitzSampling = libRandomOdes.FitzSamplingProblem(priorMean, priorVar, data.tolist(), dataCov, 30, stepSize, obsInterval, finalTime)
    else:
        FitzSampling = libRandomOdes.FitzSamplingProblemDeterministic(priorMean, priorVar, data.tolist(), dataCov, stepSize, obsInterval, finalTime)




    delta = .8
    ranges = np.array([[startPoint[0]-delta, startPoint[0]+delta],[startPoint[1]-delta, startPoint[1]+delta],[1.6, 3.2]])
    # print ranges
    plotGrid = 100

   # code to benchmark, apparently they python overhead doesn't matter!
   # import time
   # start = time.time()
   # foo=0.0
   # for i in range(1000):
   #     foo = foo+ posteriorEval.Evaluate([startPoint])[0]
   # end = time.time()
   # print "elapsed", end-start, foo/1000
   #
   # start = time.time()
   #
   # foo = libRandomOdes.AveragePosterior(posteriorEval, startPoint, 1000);
   # end = time.time()
   #
   # print "elapsed2", end-start, foo
   # sys.exit()


    print '1D'


    all_xs = []
    all_ys = []
    all_zs = []

    for i in range(3):
        print i
        x = np.linspace(ranges[i,0], ranges[i,1], (plotGrid-1)*10+1,endpoint=True)
        # print x
        z = np.empty_like(x)
        for k in range(len(x)):
            import copy
            point=copy.deepcopy(startPoint)

            point[i] = x[k]

            if randomSolver:
                z[k] = FitzSampling.DensityHelper(point, noiseExp)
            else:
                z[k] = FitzSampling.DensityHelper(point)

        all_xs.insert(i, [None]*i)
        all_xs[i].insert(i, x)
        all_ys.insert(i, [None]*i)
        all_ys[i].insert(i, None)
        all_zs.insert(i, [None]*i)
        all_zs[i].insert(i, z)

    print '2D'
    for i in range(3):
        for j in range(i):
            print i,j
            x,y = np.mgrid[ranges[i,0]:ranges[i,1]+.0001:(2.*delta/(plotGrid-1)), ranges[j,0]:ranges[j,1]+.0001:(2.*delta/(plotGrid-1))]
            # print "made ", x,y
            # x,y = np.mgrid[np.linspace(ranges[i,0], ranges[i,1], (plotGrid-1)*10+1,endpoint=True),np.linspace(ranges[,0], ranges[i,1], (plotGrid-1)*10+1,endpoint=True)]
            z = np.empty_like(x)
            for k in range(plotGrid):
                for l in range(plotGrid):

                    import copy
                    point=copy.deepcopy(startPoint)

                    point[i] = x[k,l]
                    point[j] = y[k,l]

                    if randomSolver:
                        z[k,l] = FitzSampling.DensityHelper(point, noiseExp)
                    else:
                        z[k,l] = FitzSampling.DensityHelper(point)
            all_xs[i][j]=x
            all_ys[i][j]=y
            all_zs[i][j]=z

    # print all_xs
    # print all_ys
    # print all_zs

    exampleName = "Fitz"
    if exampleName == 'rosenbrock':
       label_separation_x = 0.1
    else:
       label_separation_x = 0.1
    presentationVersion = True
    import plotTools
    if presentationVersion:
       fig = plotTools.MakeFigure(1200,.7, presentationVersion)
    else:
       fig = plotTools.MakeFigure(425,.9, presentationVersion)

    labels = ['x(0)','y(0)','z(0)']
    import surface_matrix
    surface_matrix.surface_matrix(all_xs,all_ys,all_zs, labels=labels, ax=plt.gca(), label_separation_x=label_separation_x, ranges=ranges)

#https://github.com/olgabot/prettyplotlib/wiki/scatter-and-pcolormesh:-Motivating-examples
           #http://bl.ocks.org/mbostock/5577023

   # from prettyplotlib import brewer2mpl
   #
   # blueColormap = brewer2mpl.get_map('PuBu', 'sequential', 9).mpl_colormap
   # ax=plt.subplot2grid((3,3),(i,j))
   #
   #
   # plt.pcolormesh(x,y,z,cmap=blueColormap)

    plt.savefig('plots/'+runName + '_density.pdf', format='pdf')

   # plt.close()

    # plt.show()
   # for i in range(3):
   #     for j in range(i):
   #         plt.subplot2grid(3,3,(i-1)*3+j)
   #         x,y = np.mgrid(np.linspace(ranges[i,1], ranges[i,2], plotGrid), np.linspace(ranges[j,1], ranges[j,2], plotGrid))




def generateData():
    print "starting generate"

    stateSize = 2
    stepSize = .0001
    finishTime = 40

    #noiseScale = 5
    #numSteps = finishTime/stepSize+1
    times = np.arange(0, finishTime+1E-14, stepSize).transpose()

    obsInterval = 1
    numObsTimes = int(finishTime/obsInterval)



    FitzRhs = libRandomOdes.FitzRhs()
    detsolver = libRandomOdes.RandomRk4(FitzRhs, stepSize, finishTime, obsInterval, 0)



    startPoint = [[-1, 1], [.2, .2, 3]]

    onePath = detsolver.Evaluate(startPoint)
    import math
    dataCov = math.sqrt(.001) * np.random.randn(len(onePath))

    print "generate ", onePath
    print "noise", dataCov

    np.savetxt('FitzDataRk4_fine.dat', (np.array(onePath)+dataCov).tolist() )

def plotData(stepInt, useMap = False, newFigure = True):
    print "starting plot"

    stateSize=2

    startPoint = [[-1, 1], [.2, .2, 3]]

    if stepInt == 1:
        stepSize = .1
        if useMap:
            startPoint = [[-1, 1], [.186, .196, 2.86]]

    elif stepInt == 2:
        stepSize = .05
        if useMap:
            startPoint = [[-1, 1], [.194, .204, 2.93]]

    elif stepInt == 3:
        stepSize = .02
        if useMap:
            startPoint = [[-1, 1], [.196, .205, 2.97]]

    elif stepInt == 4:
        stepSize = .01
    elif stepInt == 5:
        stepSize = .005

    finishTime = 40
    #noiseScale = 5
    #numSteps = finishTime/stepSize+1
    times = np.arange(0, finishTime+1E-14, stepSize).transpose()

    #numRuns = 100;
    obsInterval = 1
    numObsTimes = int(finishTime/obsInterval)
    numObs = numObsTimes*stateSize
    obsTimes = np.arange(obsInterval, finishTime+1E-14, obsInterval).transpose()


    allData = np.loadtxt('FitzDataRk4_fine.dat')
#    print np.reshape(allData, [stateSize, 40],'F')
    data = np.reshape(allData, [stateSize, finishTime/obsInterval],'F')


    FitzRhs = libRandomOdes.FitzRhs()
    detsolver = libRandomOdes.EulerOdeSolver(FitzRhs, stepSize, finishTime, stepSize)


    import plotTools
    if newFigure:
        fig = plotTools.MakeFigure(800,.7, True)



    onePath = detsolver.Evaluate(startPoint)

    pathMatrix = np.reshape(np.array(onePath), [stateSize, finishTime/stepSize],'F')

    if newFigure:
        for i in range(stateSize):
            plt.subplot(stateSize, 1, i+1)
            plt.plot(obsTimes, data[i,:],'o',color=plotTools.PlotColor('red'),alpha=.6)
    for i in range(stateSize):
        plt.subplot(stateSize, 1, i+1)
        if useMap:
            plt.plot(times[1:], pathMatrix[i,:],'-',color=plotTools.PlotColor('purple'),alpha=1)
        else:
            plt.plot(times[1:], pathMatrix[i,:],'-',color=plotTools.PlotColor('blue'),alpha=1)


    plt.legend(['Data', 'True parameters',  'Deterministic MAP approx'], loc='left')
    plt.subplot(stateSize,1,1)
    plt.ylabel('x')
    plt.gca().xaxis.set_visible(False)
    plt.subplot(stateSize,1,2)
    plt.ylabel('y')

    plt.xlabel('t')

    # if perfectData:
    #     plt.savefig('plots/Fitz_'+'noiseFree_trajectory.pdf', format='pdf')
    # else:
    # plt.savefig('plots/Fitz_'+'noisy_trajectory.pdf', format='pdf')


def plotRandomPaths(stepInt, useMap, newfig, biggerStep ):
    import plotTools


    stateSize = 2
#    stepSize = .004
    finishTime = 40
    #noiseScale = 5
    #numSteps = finishTime/stepSize+1


    #numRuns = 100;
    obsInterval = 1
    numObsTimes = int(finishTime/obsInterval)
    numObs = numObsTimes*stateSize
    obsTimes = np.arange(obsInterval, finishTime+1E-14, obsInterval).transpose()


    allData = np.loadtxt('FitzDataRk4_fine.dat')

#    print np.reshape(allData, [stateSize, 40],'F')
    data = np.reshape(allData, [stateSize, finishTime/obsInterval],'F')


    startPoint = [[-1, 1], [.2, .2, 3]]
    noise = pow(10.0,0)
    if stepInt == 1:
        stepSize = .1
        if useMap:
            startPoint = [[-1, 1], [.181, .35, 2.82]]
        noise = pow(10.0, -.82);
    elif stepInt == 2:
        stepSize = .05
        if useMap:
            startPoint = [[-1, 1], [.181, .25, 2.92]]
        noise = pow(10.0, -.66);
    elif stepInt == 3:
        stepSize = .02
        if useMap:
            startPoint = [[-1, 1], [.181, .25, 2.96]]
        noise = pow(10.0, -.5);
    elif stepInt == 4:
        stepSize = .01
        noise = pow(10.0,-1)

    elif stepInt == 5:
        stepSize = .005
        noise = pow(10.0,-1)

    if biggerStep:
        stepSize = 2.0*stepSize

    times = np.arange(0, finishTime+1E-14, stepSize).transpose()

    FitzRhs = libRandomOdes.FitzRhs()
    detsolver = libRandomOdes.RandomOdeSolver(FitzRhs, stepSize, finishTime, stepSize, noise)
#    detsolver = libRandomOdes.RandomRk2(FitzRhs, stepSize, finishTime, stepSize, pow(10.0,2.6))    
#    detsolver = libRandomOdes.RandomOdeSolver(FitzRhs, stepSize, finishTime, stepSize, pow(10.0,2))



    if newfig:
        fig = plotTools.MakeFigure(1400,.7, True)










    for j in range(100):
        onePath = detsolver.Evaluate(startPoint)

        pathMatrix = np.reshape(np.array(onePath), [stateSize, finishTime/stepSize],'F')

        for i in range(stateSize):
            plt.subplot(stateSize, 1, i+1)
            if not biggerStep:
                plt.plot(times[1:], pathMatrix[i,:],'-',color=plotTools.PlotColor('blue'),alpha=.1)                
            else:
                plt.plot(times[1:], pathMatrix[i,:],'-',color=plotTools.PlotColor('purple'),alpha=.1)

    if not newfig:
        for i in range(stateSize):
            plt.subplot(stateSize, 1, i+1)
            plt.plot(obsTimes, data[i,:],'o',color=plotTools.PlotColor('red'),alpha=.8)

    # if not newfig:
    #     plt.legend(['Data', 'True', 'MAP'],loc='best')

    plt.subplot(stateSize,1,1)
    plt.ylabel('x')
    plt.gca().xaxis.set_visible(False)
    plt.subplot(stateSize,1,2)
    plt.ylabel('y')
    plt.xlabel('t')




    # if perfectData:
    #     plt.savefig('plots/Fitz_'+'noiseFree_randomTrajectory.pdf', format='pdf')
    # else:
    #     plt.savefig('plots/Fitz_'+'noisy_randomTrajectory.pdf', format='pdf')


comm = MPI.COMM_WORLD

pdeinit = libmuqPde.LibMeshInit(sys.argv)

runLength = 100000
jobs = []

# generateData()
# sys.exit()
#
# plotData(False)
# plt.show()
# sys.exit()
# plotData(1, False, True)
# plotData(1, True, False)
# plt.savefig('plots/deterministic_data_1.pdf', format='pdf')

# plotData(2, False, True)
# plotData(2, True, False)
# plt.savefig('plots/deterministic_data_2.pdf', format='pdf')
#

# plotData(3, False, True)
# plotData(3, True, False)
# plt.savefig('plots/deterministic_data_3.pdf', format='pdf')
#
# plotData(4, False, True)
# plotData(5, False, True)
# plt.savefig('plots/true_data.pdf', format='pdf')
#
# plotRandomPaths(1, False, True, True )
# plotRandomPaths(1, False, False, False )
# plt.savefig('plots/random_data_true_1.pdf', format='pdf')

# # plotRandomPaths(1, True, True )
#
# plotRandomPaths(2, False, True, True )
# plotRandomPaths(2, False, False, False )
# plt.savefig('plots/random_data_true_2.pdf', format='pdf')
# # plotRandomPaths(2, True, True )
#
# plotRandomPaths(3, False, True, True )
# plotRandomPaths(3, False, False, False )
# plt.savefig('plots/random_data_true_3.pdf', format='pdf')
#
#
#
# plotRandomPaths(1, True, True, True )
# plotRandomPaths(1, True, False, False )
# plt.savefig('plots/random_data_infer_1.pdf', format='pdf')

#
# plotRandomPaths(2, True, True, True )
# plotRandomPaths(2, True, False, False )
# plt.savefig('plots/random_data_infer_2.pdf', format='pdf')

#
# plotRandomPaths(3, True, True, True )
# plotRandomPaths(3, True, False, False )
# plt.savefig('plots/random_data_infer_3.pdf', format='pdf')


# plotRandomPaths(4, True, True, True )
# plotRandomPaths(4, True, False, False )

# plotRandomPaths(5, True, True, True )
# plotRandomPaths(5, True, False, False )

# plotRandomPaths(3, True, True )
# plotData(.02)
# plotRandomPaths(True)
#plotRandomPaths(False, .008)
#plotRandomPaths(False, .004)
#plotRandomPaths(False, .002)

#plotRandomPaths(False, .2, True)
#plotRandomPaths(False, .1, False)


# plotRandomPaths(.1, True,pow(10.,-.9))
# plotRandomPaths(.05, False,pow(10.,-.9))
# plt.savefig('plots/Fitz_'+'noisy_trajectory_05.pdf', format='pdf')

#
# plotRandomPaths( .05, True,pow(10.0,-.9))
# plotRandomPaths( .025, False,pow(10.0,-.9))
# plt.savefig('plots/Fitz_'+'noisy_trajectory_025.pdf', format='pdf')

##
# plotRandomPaths( .05, True,pow(10.0,-.9))
# plotRandomPaths( .02, False,pow(10.0,-.9))
# plt.savefig('plots/Fitz_'+'noisy_trajectory_01.pdf', format='pdf')

##
#plotRandomPaths(False, .01, True)
#plotRandomPaths(False, .005, False)


# PlotPosteriorSurface(False, 1)
# PlotPosteriorSurface(False, 2)
# PlotPosteriorSurface(False, 3)

# PlotPosteriorSurface(True, 1)
# PlotPosteriorSurface(True, 2)
# PlotPosteriorSurface(True, 3)
# plt.show()



if False:
    finalTime = 40.0
    obsInterval = 1.0
    stepInt=5
    if stepInt == 1:
        stepSize = .1
        noiseExp = -.6
    elif stepInt == 2:
        stepSize = .05
        noiseExp = -.4

    elif stepInt == 3:
        stepSize = .02
        noiseExp = -.4

    elif stepInt == 4:
        stepSize = .01
    elif stepInt == 5:
        stepSize = .005

    startPoint = [.2,.2,3]

    priorMean = [.2,.2,3,0]
    priorVar = [1,1,1,1]
    data = np.loadtxt('FitzDataRk4_fine.dat')


    #    print np.reshape(allData, [stateSize, 40],'F')
    #    data = np.reshape(np.reshape(allData, [stateSize, 40],'F')[:,0:numObsTimes], [numObs,1],'F').transpose().tolist()[0]
    dataCov = .001
    FitzSampling = libRandomOdes.FitzSamplingProblem(priorMean, priorVar, data.tolist(), dataCov, 30, stepSize, obsInterval, finalTime)
    point = [.2, .2, 3]
    print FitzSampling
    print 'dens' , FitzSampling.DensityHelper(point, -2)

# sys.exit()


#for i in range(10):
#for i in range(30):#[4]:
#    seed = libmuqUtilities.RandomGeneratorTemporarySetSeed(i+87478)
for i in range(1):

    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength, finalTime = 2, perfectData=True, randomSolver=False))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength, finalTime = 4, perfectData=True, randomSolver=False))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength, finalTime = 6, perfectData=True, randomSolver=False))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength, finalTime = 8, perfectData=True, randomSolver=False))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength, finalTime = 10, perfectData=True, randomSolver=False))

#    jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength, finalTime = 2, perfectData=False, randomSolver=False))
#    jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength, finalTime = 4, perfectData=False, randomSolver=False))
#    jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength, finalTime = 6, perfectData=False, randomSolver=False))
#    jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength, finalTime = 8, perfectData=False, randomSolver=False))
#    jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength, finalTime = 10, perfectData=False, randomSolver=False))


    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength, finalTime = 2, perfectData=True, randomSolver=True))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength, finalTime = 4, perfectData=True, randomSolver=True))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength, finalTime = 6, perfectData=True, randomSolver=True))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength, finalTime = 8, perfectData=True, randomSolver=True))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength, finalTime = 10, perfectData=True, randomSolver=True))

#    jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength, finalTime = 10, perfectData=False, randomSolver=True))
#    jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength, finalTime = 8, perfectData=False, randomSolver=True))

#    jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength, finalTime = 2, perfectData=False, randomSolver=True))
#    jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength, finalTime = 4, perfectData=False, randomSolver=True))
#    jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength, finalTime = 6, perfectData=False, randomSolver=True))

    jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=1, randomSolver='rand'))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=1, randomSolver='det'))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=1, randomSolver='ind'))
    jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=2, randomSolver='rand'))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=2, randomSolver='det'))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=2, randomSolver='ind'))
    jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=3, randomSolver='rand'))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=3, randomSolver='det'))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=3, randomSolver='ind'))
    jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=4, randomSolver='rand'))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=4, randomSolver='det'))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=4, randomSolver='ind'))
    jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=5, randomSolver='rand'))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=5, randomSolver='det'))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=5, randomSolver='ind'))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=2, randomSolver=True))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=3, randomSolver=True))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=4, randomSolver=True))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=5, randomSolver=True))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=1, randomSolver=False))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=2, randomSolver=False))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=3, randomSolver=False))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=4, randomSolver=False))
    # jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=5, randomSolver=False))

    # jobs.append(lambda i=i: RunOneFitzForward(i,mcmcLength=runLength,stepInt=1))
    # jobs.append(lambda i=i: RunOneFitzForward(i,mcmcLength=runLength,stepInt=2))
    # jobs.append(lambda i=i: RunOneFitzForward(i,mcmcLength=runLength,stepInt=3))
    # jobs.append(lambda i=i: RunOneFitzForward(i,mcmcLength=runLength,stepInt=4))
    # jobs.append(lambda i=i: RunOneFitzForward(i,mcmcLength=runLength,stepInt=5))

#    jobs.append(lambda i=i: RunOneFitz(i,mcmcLength=runLength,stepInt=4, randomSolver=True))
    

#seed = libmuqUtilities.RandomGeneratorTemporarySetSeed(87478)
#libmuqUtilities.HDF5Wrapper.OpenFile('results/genetics_regression_looser.h5_rank_'+ str(comm.Get_rank()))
libmuqUtilities.HDF5Wrapper.OpenFile('results/fitz_moreNoise.h5_rank_'+ str(comm.Get_rank()))
print "I'm", comm.Get_rank(), comm.Get_size()
for aJob in jobs[comm.Get_rank()::comm.Get_size()]:
    aJob()
comm.barrier()
libmuqUtilities.HDF5Wrapper.CloseFile()



