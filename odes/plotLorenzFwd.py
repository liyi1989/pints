# -*- coding: utf-8 -*-
"""
Created on Tue Jan 27 10:16:27 2015

@author: prconrad
"""

import sys

sys.path.append('/Users/patrick/muq_install/lib')
sys.path.append('/Users/patrick/Dropbox/Development/examples/build')
sys.path.append('/Users/patrick/Dropbox/Development/examples/odes/build')
sys.path.append('/Users/patrick/Dropbox/Development/examples')

# sys.path.append('/home/prconrad/local/lib')
# sys.path.append('/home/prconrad/local/muq_external/lib')
# sys.path.append('/media/extradata/Dropbox/Development/examples/odes/build')
# sys.path.append('/media/extradata/Dropbox/Development/examples')


import libmuqUtilities
import libmuqModelling
import libmuqInference
import libmuqApproximation
import libmuqPde
import libRandomOdes
import numpy as np
import matplotlib.pyplot as plt
import plotTools




def plotRandomPathsFwd(stepSize, noise, N=100, color='blue', order=2, alpha=.3 , width =1):
    import plotTools


    stateSize = 3
#    stepSize = .004
    finishTime = 20
    #noiseScale = 5
    #numSteps = finishTime/stepSize+1


    #numRuns = 100;
    obsInterval = .02
    numObsTimes = int(finishTime/obsInterval)
    numObs = numObsTimes*stateSize
    obsTimes = np.arange(obsInterval, finishTime+1E-14, obsInterval).transpose()

    startPoint = [[ -11, -5, 38],[10,8./3.,28.]]
    

    times = np.arange(0, finishTime+1E-14, stepSize).transpose()

    LorenzRhs = libRandomOdes.LorenzRhs()
    if order==2:
        solver = libRandomOdes.RandomRk2(LorenzRhs, stepSize, finishTime, obsInterval, noise)
    elif order==3:
        solver = libRandomOdes.RandomRk3(LorenzRhs, stepSize, finishTime, obsInterval, noise)
    elif order==4:
        solver = libRandomOdes.RandomRk4(LorenzRhs, stepSize, finishTime, obsInterval, noise)







    for j in range(N):
        onePath = solver.Evaluate(startPoint)

        pathMatrix = np.reshape(np.array(onePath), [stateSize, finishTime/obsInterval],'F')

        for i in range(stateSize):
            ax=plt.subplot(stateSize,1,i+1)
            plt.plot(obsTimes[:], pathMatrix[i,:],'-',color=plotTools.PlotColor(color),alpha=alpha, linewidth=width)
            import pylab
            if i == 0:
                plt.ylabel('$x$')
                pylab.setp( ax.get_xticklabels(), visible=False)

            elif i == 1:
                plt.ylabel('$y$')
                pylab.setp( ax.get_xticklabels(), visible=False)

            elif i == 2:
                plt.ylabel('$z$')
                plt.xlabel('$t$')




def plotRandomPathsHistogram(stepSize, noise, N=100, color='blue', order=2, alpha=.3 , width =1):
    import plotTools
    # ffmpeg -framerate 30 -i lorenz_dist_time_%03d.png -c:v libx264 -r 30 -pix_fmt yuv420p out.mp4

    stateSize = 3
#    stepSize = .004
    finishTime = 30
    #noiseScale = 5
    #numSteps = finishTime/stepSize+1


    #numRuns = 100;
    obsInterval = .05
    numObsTimes = int(finishTime/obsInterval)
    numObs = numObsTimes*stateSize
    obsTimes = np.arange(obsInterval, finishTime+1E-14, obsInterval).transpose()

    startPoint = [[ -11, -5, 38],[10,8./3.,28.]]


    times = np.arange(0, finishTime+1E-14, stepSize).transpose()

    LorenzRhs = libRandomOdes.LorenzRhs()
    if order==2:
        solver = libRandomOdes.RandomRk2(LorenzRhs, stepSize, finishTime, obsInterval, noise)
    elif order==3:
        solver = libRandomOdes.RandomRk3(LorenzRhs, stepSize, finishTime, obsInterval, noise)
    elif order==4:
        solver = libRandomOdes.RandomRk4(LorenzRhs, stepSize, finishTime, obsInterval, noise)



    # print obsTimes.shape

    allPaths = np.zeros([obsTimes.shape[0]*stateSize, N])
    for j in range(N):
        allPaths[:,j] = np.array(solver.Evaluate(startPoint))
        if j % 10 == 0:
            print j
        # print allPaths[:,j]
        #
        # print np.reshape(np.array(allPaths[:,j]), [stateSize, finishTime/obsInterval],'F')

    fig = plt.figure(figsize = [1920/200, 1080/200], dpi=200)

    from matplotlib import rcParams
    from plotTools import PlotColor

    greyColor = PlotColor("grey")
    whiteColor = PlotColor("white")

    rcParams['axes.labelsize'] = 12
    rcParams['xtick.labelsize'] = 12
    rcParams['ytick.labelsize'] = 12
    rcParams['legend.fontsize'] = 12
    rcParams['axes.edgecolor'] = greyColor
    rcParams['axes.facecolor'] = whiteColor
    rcParams['figure.facecolor'] = whiteColor
    rcParams['axes.labelcolor'] = greyColor
    rcParams['text.color'] = greyColor
    rcParams['xtick.color'] = greyColor
    rcParams['ytick.color'] = greyColor

    rcParams['lines.antialiased'] = True

    rcParams['text.usetex'] = False
    rcParams['font.family'] = 'sans-serif'
#        rcParams['font.sans-serif'] = ['Helvetica']
    rcParams['mathtext.fontset'] = 'stixsans'
#        rcParams['mathtext.fallback_to_cm'] = False
    rcParams['lines.linewidth'] = 1.5
#        rcParams['axes.formatter.use_mathtext'] = True


    for time in range(obsTimes.shape[0]):
        fig.clear();

        for i in range(stateSize):
            ax=plt.subplot(stateSize,1,i+1)




            import pylab
            numbins = 100
            if i==1:
                bins = np.linspace(-30, 30, numbins)
            elif i==2:
                bins = np.linspace(0, 50, numbins)
            elif i==0:
                bins = np.linspace(-20, 20, numbins)


            print time, i
            n, bins, patches = pylab.hist(allPaths[time*3+i,:], bins, normed=1, histtype='stepfilled')
            pylab.setp(patches, 'facecolor', plotTools.PlotColor("blue"))

            ax.set_yticks([])
            ax.set_yticklabels([])

            if i==0:
                plt.ylabel('x')
            elif i==1:
                plt.ylabel('y')
            elif i==2:
                plt.ylabel('z')

            if i==0:
                plt.title('$t=$ ' + '{:10.2f}'.format(obsTimes[time]) )

            fig.tight_layout(pad=1)

        plt.savefig('/Users/patrick/Documents/lorenz_movie/lorenz_dist_time_'+ '{0:03}'.format(time) + '.png', format='png')



def plotRandomPathsHistogramPaper(stepSize, noise, N=100, color='blue', order=2, alpha=.3 , width =1):
    import plotTools
    # ffmpeg -framerate 30 -i lorenz_dist_time_%03d.png -c:v libx264 -r 30 -pix_fmt yuv420p out.mp4

    stateSize = 3
#    stepSize = .004
    finishTime = 30
    #noiseScale = 5
    #numSteps = finishTime/stepSize+1


    #numRuns = 100;
    obsInterval = .05
    numObsTimes = int(finishTime/obsInterval)
    numObs = numObsTimes*stateSize
    obsTimes = np.arange(obsInterval, finishTime+1E-14, obsInterval).transpose()

    startPoint = [[ -11, -5, 38],[10,8./3.,28.]]


    times = np.arange(0, finishTime+1E-14, stepSize).transpose()

    LorenzRhs = libRandomOdes.LorenzRhs()
    if order==2:
        solver = libRandomOdes.RandomRk2(LorenzRhs, stepSize, finishTime, obsInterval, noise)
    elif order==3:
        solver = libRandomOdes.RandomRk3(LorenzRhs, stepSize, finishTime, obsInterval, noise)
    elif order==4:
        solver = libRandomOdes.RandomRk4(LorenzRhs, stepSize, finishTime, obsInterval, noise)



    # print obsTimes.shape

    allPaths = np.zeros([obsTimes.shape[0]*stateSize, N])
    for j in range(N):
        allPaths[:,j] = np.array(solver.Evaluate(startPoint))
        if j % 10 == 0:
            print j
        # print allPaths[:,j]
        #
        # print np.reshape(np.array(allPaths[:,j]), [stateSize, finishTime/obsInterval],'F')


    from matplotlib import rcParams
    from plotTools import PlotColor


    # for time in range(obsTimes.shape[0]):
    for time in [99,199,299,399,499,599]:

        fig = plotTools.MakeFigure(500, .9, presentationVersion = False)

        for i in range(stateSize):
            ax=plt.subplot(stateSize,1,i+1)




            import pylab
            # numbins = 100
            # if i==1:
            #     bins = np.linspace(-30, 30, numbins)
            # elif i==2:
            #     bins = np.linspace(0, 50, numbins)
            # elif i==0:
            #     bins = np.linspace(-20, 20, numbins)


            print time, i
            n, bins, patches = pylab.hist(allPaths[time*3+i,:], bins=100, histtype='stepfilled')
            pylab.setp(patches, 'facecolor', plotTools.PlotColor("blue"))
            print n
            print bins
            print patches

            # pylab.setp( ax.get_yticklabels(), visible=False)
            # pylab.setp( ax.get_ys(), visible=False)

            plt.ylim([0,max(n)])
            ax.set_yticks([])
            ax.set_yticklabels([])

            if i==0:
                plt.ylabel('x')
            elif i==1:
                plt.ylabel('y')
            elif i==2:
                plt.ylabel('z')

            if i==0:
                plt.title('$t=$ ' + '{:10.2f}'.format(obsTimes[time]) )

            fig.tight_layout(pad=1)

        plt.savefig('/Users/patrick/Documents/lorenz_movie/lorenz_distPaper_time_'+ '{0:03}'.format(time) + '.pdf', format='pdf')




def plotForwardPredictive(stepInt , presentationVersion, saveToFile , N):
    import h5py
    import CompareIncrementalCovariances_2
    hdf5filename = 'results/lorenz_fwd.h5_rank_' + str(stepInt-3)

    f = h5py.File(hdf5filename,"r")

    if stepInt == 1:
        stepSize = .1
    elif stepInt == 2:
        stepSize = .05
    elif stepInt == 3:
        stepSize = .02
    elif stepInt == 4:
        stepSize = .01
    elif stepInt == 5:
        stepSize = .005


    chain = np.array(CompareIncrementalCovariances_2.ReferenceChain(hdf5filename,'Lorenz'))[:,:]


    # if presentationVersion:
    #     fig = plotTools.MakeFigure(1000,.7, presentationVersion)
    # else:
    #     fig = plotTools.MakeFigure(425,.9, presentationVersion)

    randomsubset = np.random.choice(chain[0,:], size=[N])

    for i in range(N):
        plotRandomPathsFwd(stepSize, pow(10.0, randomsubset[i]), N=1, color='medium blue', alpha=.5, order=3,width=.5)
        # plotRandomPathsFwd(stepSize, pow(10.0, randomsubset[i]), N=1, color='medium blue',alpha=.3, order=2,width=.5 )

    plotRandomPathsFwd(0.001, 0.0, N=1, color='orange', order=4, alpha=1,width=3 )
    # print "order 3"
    plotRandomPathsFwd(stepSize, 0.0, N=1, color='dark blue', order=3, alpha=1 ,width=2)
    # print "order 2"
    plotRandomPathsFwd(stepSize, 0.0, N=1, color='medium-dark red', order=2, alpha=1 ,width=2)

#
#         # plotRandomPathsFwd(.1, sigmas[i])
#         # plotReferencePathFwd()
#         # plt.xticks([0,5,10,15,20])
#         # plt.yticks([-4,-2,0,2,4])
#         if sigmas[i] < .001:
#             plt.title('$\\sigma=' + str(sigmas[i]*1000) + '\\times 10^{-3}$' )
#         else:
#             plt.title('$\\sigma=' + str(sigmas[i]) + '$' )
#         if i+1==7 or i+1 == 8 or i+1==9:
#             plt.xlabel('$t$')
#         else:
# #            plt.xlim([0,20])
#             plt.gca().set_xticklabels([])
#
# #            plt.gca().xaxis.set_visible(False)
#         if i+1==1 or i +1== 4 or i+1==7:
#             plt.ylabel('$R$')
#         else:
# #            plt.ylim([-4,4])
#             plt.gca().set_yticklabels([])
# #            plt.gca().yaxis.set_visible(False)
#
    # plt.xlabel('$t$')
    # plt.xlabel('$V$')
    # plt.legend(['h', '2h'])
#     fig.subplots_adjust(wspace=.15)
#
#
    # if saveToFile:
    #     if presentationVersion:
    #         plt.savefig('plots/lorenz_fwdPredictive' + str(stepInt) + '.pdf', format='pdf')
    #     else:
    #         plt.savefig('plots/lorenz_noise_survey_paper.pdf', format='pdf')
       # plt.close(fig)


def plotForwardPredictiveSummary(presentationVersion, saveToFile , N):
    if presentationVersion:
        fig = plotTools.MakeFigure(1000,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(425,.9, presentationVersion)

    plt.subplot(3,1,1)
    plotForwardPredictive(3, True, False, N)
    plt.title("$h=.02$")
    plt.subplot(3,1,2)
    plotForwardPredictive(4, True, False, N)
    plt.title("$h=.01$")
    plt.subplot(3,1,3)
    plotForwardPredictive(5, True, False, N)
    plt.title("$h=.005$")

    fig.tight_layout(pad=0.2)

    # if saveToFile:
    #     if presentationVersion:
    #         plt.savefig('plots/lorenz_fwdPredictive' + str(stepInt) + '.pdf', format='pdf')
    #     else:
    #         plt.savefig('plots/lorenz_noise_survey_paper.pdf', format='pdf')
       # plt.close(fig)
def PlotStepSequence(presentationVersion, saveToFile, subsample=100):
    #hdf5File = 'results/' + exampleName + '.h5'
    import CompareIncrementalCovariances_2
    all_data = [np.array(CompareIncrementalCovariances_2.ReferenceChain('results/lorenz_fwd.h5_rank_0','Lorenz'))[:,::subsample],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/lorenz_fwd.h5_rank_1','Lorenz'))[:,::subsample],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/lorenz_fwd.h5_rank_2','Lorenz'))[:,::subsample],
                #np.array(CompareIncrementalCovariances_2.ReferenceChain('results/lorenz_fwd.h5_rank_3','Lorenz'))[:,::subsample],
                #np.array(CompareIncrementalCovariances_2.ReferenceChain('results/lorenz_fwd.h5_rank_4','Lorenz'))[:,::subsample]
                ]
    referenceName = 'lorenz_fwd'

    import kde_matrix
    labels = []

    boundaries_list = []
    contour_labels = [ 'h=.02', 'h=.01', 'h=.005']

    for i in range( all_data[0].shape[0]):
        labels.append('$\\theta_' + str(i+1) + '$')


    label_separation_x = 0.1

    if presentationVersion:
       fig = plotTools.MakeFigure(1200,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(600,.9, presentationVersion)
    kde_matrix.kde_matrix(all_data, labels=labels, ax=plt.gca(), label_separation_x=label_separation_x,contour_labels=contour_labels, boundaries_list=boundaries_list)

#    fig.tight_layout(pad=0.1)


    if saveToFile:
        if presentationVersion:
            plt.savefig('plots/lorenz_fwdScalePosterior.pdf', format='pdf')
        else:
            plt.savefig('plots/' + referenceName + '_perfect_density_paper.pdf', format='pdf')
#        plt.close(fig)
    

# PlotStepSequence(True, False, 1)
# plt.savefig('plots/lorenz_noise_density.pdf', format='pdf')


# plotForwardPredictive(1, True, True, 100)
# plotForwardPredictive(2, True, True, 100)
# plotForwardPredictive(3, True, False, 50)
# plotForwardPredictive(4, True, False, 50)
# plotForwardPredictive(5, True, False, 50)

# plotForwardPredictiveSummary(True, False, 50)
# plt.savefig('plots/lorenz_noise_summary.pdf', format='pdf')


def RandomPathsVsDetComparison(presentationVersion):
    if presentationVersion:
       fig = plotTools.MakeFigure(800,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(500,.9, presentationVersion)
    plotRandomPathsFwd(.01, 300, N=50, color='blue', order=4, alpha=.3 , width =1)
    plotRandomPathsFwd(.01, 0, N=1, color='red', order=4, alpha=1 , width =1)

    if presentationVersion:
        plt.savefig('plots/lorenz_rk4_comparison_presentation.pdf', format='pdf')
    else:
        plt.savefig('plots/lorenz_rk4_comparison_paper.pdf', format='pdf')

RandomPathsVsDetComparison(True)
# RandomPathsVsDetComparison(False)
# plotRandomPathsFwd(.01, 0, N=1, color='orange', order=4, alpha=1 , width =2)
# plotRandomPathsFwd(.0001, 0, N=1, color='red', order=4, alpha=1 , width =2)



# plotRandomPathsHistogram(.01, 300, N=50000, color='blue', order=4, alpha=.3 , width =1)
# plotRandomPathsHistogramPaper(.01, 300, N=500000, color='blue', order=4, alpha=.3 , width =1)
plt.show()