# -*- coding: utf-8 -*-
"""
Created on Wed Jun 18 17:08:18 2014

@author: prconrad
"""
#import matplotlib
#matplotlib.use('pdf')

import CompareIncrementalCovariances_2
import plotTools

import numpy as np
import matplotlib.pyplot as plt



def PlotDensity(hdf5File, exampleName, index, presentationVersion, saveToFile, subsample=100):
    #hdf5File = 'results/' + exampleName + '.h5'
    referenceName = exampleName
    referenceChain = np.array(CompareIncrementalCovariances_2.ReferenceChain(hdf5File,referenceName))
    referenceName = exampleName +'_' + index

    from pandas.tools.plotting import scatter_matrix
    from pandas import DataFrame
    df = DataFrame(referenceChain[:,::subsample].T)

    df.plot()
#    plt.title('time = ' + str(finalTime))
#    if saveToFile:

#        plt.savefig('plots/' + referenceName + '_trace.pdf', format='pdf')


    scatter_matrix(df, alpha=0.2, figsize=(10,10))
#    plt.title('time = ' + str(finalTime))


    import kde_matrix
    labels = []


    for i in range( referenceChain.shape[0]):
            labels.append('$\\theta_' + str(i+1) + '$')

    if exampleName == 'rosenbrock':
        label_separation_x = 0.1
    else:
        label_separation_x = 0.45

#    if presentationVersion:
#        fig = plotTools.MakeFigure(600,.7, presentationVersion)
#    else:
#        fig = plotTools.MakeFigure(425,.9, presentationVersion)
#    kde_matrix.kde_matrix(referenceChain[:,::subsample], labels=labels, ax=plt.gca(), label_separation_x=label_separation_x)

#    fig.tight_layout(pad=0.1)


    if saveToFile:
        if presentationVersion:
            plt.savefig('plots/' + referenceName + '_density.pdf', format='pdf')
        else:
            plt.savefig('plots/' + referenceName + '_perfect_density_paper.pdf', format='pdf')
#        plt.close(fig)


def PlotStepSequence(exampleName, type, filename, presentationVersion, saveToFile, subsample=100):
    #hdf5File = 'results/' + exampleName + '.h5'



    import kde_matrix
    labels = []

    if exampleName == 'Fitz':
        print 'messed with Fitz file choices!!!!! '
        all_data = [
            np.array(CompareIncrementalCovariances_2.ReferenceChain('results/fitz_moreNoise.h5_rank_0',exampleName + '_' + type + '_1'))[:,::],
            np.array(CompareIncrementalCovariances_2.ReferenceChain('results/fitz_moreNoise.h5_rank_1',exampleName + '_' + type + '_2'))[:,::],
            np.array(CompareIncrementalCovariances_2.ReferenceChain(filename,exampleName + '_' + type + '_3'))[:,::],
            np.array(CompareIncrementalCovariances_2.ReferenceChain(filename,exampleName + '_' + type + '_4'))[:,::],
            np.array(CompareIncrementalCovariances_2.ReferenceChain(filename,exampleName + '_' + type + '_5'))[:,::],
                ]


         # all_data = [
         #    np.array(CompareIncrementalCovariances_2.ReferenceChain(filename,exampleName + '_' + type + '_1'))[:,::],
         #    np.array(CompareIncrementalCovariances_2.ReferenceChain(filename,exampleName + '_' + type + '_2'))[:,::],
         #    np.array(CompareIncrementalCovariances_2.ReferenceChain(filename,exampleName + '_' + type + '_3'))[:,::],
         #    np.array(CompareIncrementalCovariances_2.ReferenceChain(filename,exampleName + '_' + type + '_4'))[:,::],
         #    np.array(CompareIncrementalCovariances_2.ReferenceChain(filename,exampleName + '_' + type + '_5'))[:,::],
         #        ]
        boundaries_list = [[.05,.3],[-.3,.4],[2.5,3.05]]
        contour_labels = ['h=.1','h=.05', 'h=.02', 'h=.01', 'h=.005']
        label_separation_x = 0.1
    elif exampleName == 'Repress':
        all_data = [
            np.array(CompareIncrementalCovariances_2.ReferenceChain(filename+'_rank_0',exampleName + '_' + type + '_1'))[:,::],
            np.array(CompareIncrementalCovariances_2.ReferenceChain(filename+'_rank_1',exampleName + '_' + type + '_2'))[:,::],
            np.array(CompareIncrementalCovariances_2.ReferenceChain(filename+'_rank_2',exampleName + '_' + type + '_3'))[:,::],
            np.array(CompareIncrementalCovariances_2.ReferenceChain(filename+'_rank_3',exampleName + '_' + type + '_4'))[:,::],
            np.array(CompareIncrementalCovariances_2.ReferenceChain(filename+'_rank_4',exampleName + '_' + type + '_5'))[:,::],
                ]
        boundaries_list = [[.1,1.1],[1.6,2.1],[3,6.5],[600,1100]]
        contour_labels = ['h=.1','h=.05', 'h=.02', 'h=.01', 'h=.005']
        label_separation_x = 0.15
    else:
        print "elliptic rules"
        all_data = [
            np.array(CompareIncrementalCovariances_2.ReferenceChain(filename,exampleName + '_' + type + '_1'))[0:4,::],
            np.array(CompareIncrementalCovariances_2.ReferenceChain(filename,exampleName + '_' + type + '_2'))[0:4,::],
            np.array(CompareIncrementalCovariances_2.ReferenceChain(filename,exampleName + '_' + type + '_3'))[0:4,::],
            np.array(CompareIncrementalCovariances_2.ReferenceChain(filename,exampleName + '_' + type + '_4'))[0:4,::],
            np.array(CompareIncrementalCovariances_2.ReferenceChain(filename,exampleName + '_' + type + '_5'))[0:4,::],
                ]
        boundaries_list = [[-3,1],[-6,2],[-2,4],[-3,2]]
        # boundaries_list = [[-4,8],[-35,4],[2,12],[-10,5]]
#        boundaries_list = []
        contour_labels = ['h=1/10','h=1/20', 'h=1/40', 'h=1/60', 'h=1/80']
        label_separation_x = 0.2
        # print all_data
    # contour_labels = ['rand', 'det', 'ind']

    for i in range( all_data[0].shape[0]):
        labels.append('$\\theta_' + str(i+1) + '$')




    if presentationVersion:
       fig = plotTools.MakeFigure(800,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(600,.9, presentationVersion)
    kde_matrix.kde_matrix(all_data, labels=labels, ax=plt.gca(), label_separation_x=label_separation_x,contour_labels=contour_labels,
                          boundaries_list=boundaries_list,kde_subsample=subsample, scatter_subsample=50, usescatter=False)

#    fig.tight_layout(pad=0.1)


    if saveToFile:
        if presentationVersion:
            plt.savefig('plots/' + exampleName + '_' + type + '_' + str(subsample) + 'AR2_densityPresentation.pdf', format='pdf')
        else:
            plt.savefig('plots/' + exampleName + '_' + type + '_' + str(subsample) + 'AR2_densityPaper.pdf', format='pdf')
#        plt.close(fig)

def PlotStepSequenceRepressRk2(randomVersion, presentationVersion, saveToFile, subsample=100):
    #hdf5File = 'results/' + exampleName + '.h5'
    if randomVersion:
        all_data = [np.array(CompareIncrementalCovariances_2.ReferenceChain('results/Repress_rk2.h5_rank_0','Repress'))[0:5,::subsample],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/Repress_rk2.h5_rank_1','Repress'))[0:5,::subsample],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/Repress_rk2.h5_rank_2','Repress'))[0:5,::subsample],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/Repress_rk2.h5_rank_3','Repress'))[0:5,::subsample/2],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/Repress_rk2.h5_rank_4','Repress'))[0:5,::subsample/2]
                ]
        referenceName = 'Repress_rk2_randomSolver'
    else:
        all_data = [np.array(CompareIncrementalCovariances_2.ReferenceChain('results/Repress_rk2_det.h5_rank_0','Repress'))[:,::subsample],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/Repress_rk2_det.h5_rank_1','Repress'))[:,::subsample],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/Repress_rk2_det.h5_rank_2','Repress'))[:,::subsample],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/Repress_rk2_det.h5_rank_3','Repress'))[:,::subsample],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/Repress_rk2_det.h5_rank_4','Repress'))[:,::subsample]
                ]
        referenceName = 'Repress_rk2_deterministicSolver'
    import kde_matrix
    labels = []
    boundaries_list=[]
    boundaries_list = [[0,1.2], [1.6, 2.2],[3, 6.5],[400, 1200]]
    if randomVersion:
        boundaries_list.append([-6, 3])
    contour_labels = ['h=.1','h=.05', 'h=.02', 'h=.01', 'h=.005']

    for i in range( all_data[0].shape[0]):
        labels.append('$\\theta_' + str(i+1) + '$')


    label_separation_x = 0.1

    if presentationVersion:
       fig = plotTools.MakeFigure(1400,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(600,.9, presentationVersion)
    kde_matrix.kde_matrix(all_data, labels=labels, ax=plt.gca(), label_separation_x=label_separation_x,contour_labels=contour_labels, boundaries_list=boundaries_list)

#    fig.tight_layout(pad=0.1)


    if saveToFile:
        if presentationVersion:
            plt.savefig('plots/' + referenceName + '_' + str(subsample) + '_density.pdf', format='pdf')
        else:
            plt.savefig('plots/' + referenceName + '_perfect_density_paper.pdf', format='pdf')
#        plt.close(fig)

def PlotStepSequenceRepressRk4(randomVersion, presentationVersion, saveToFile, subsample=100):
    #hdf5File = 'results/' + exampleName + '.h5'
    if randomVersion:
        all_data = [np.array(CompareIncrementalCovariances_2.ReferenceChain('results/Repress_rk4.h5_rank_0','Repress'))[0:4,::subsample],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/Repress_rk4.h5_rank_1','Repress'))[0:4,::subsample],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/Repress_rk4.h5_rank_2','Repress'))[0:4,::subsample],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/Repress_rk4.h5_rank_3','Repress'))[0:4,::subsample/2],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/Repress_rk4.h5_rank_4','Repress'))[0:4,::subsample/2]
                ]
        referenceName = 'Repress_rk4_randomSolver'
    else:
        all_data = [np.array(CompareIncrementalCovariances_2.ReferenceChain('results/Repress_rk4.h5_rank_5','Repress'))[:,::subsample],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/Repress_rk4.h5_rank_6','Repress'))[:,::subsample],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/Repress_rk4.h5_rank_7','Repress'))[:,::subsample],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/Repress_rk4.h5_rank_8','Repress'))[:,::subsample],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/Repress_rk4.h5_rank_9','Repress'))[:,::subsample]
                ]
        referenceName = 'Repress_rk4_deterministicSolver'
    import kde_matrix
    labels = []
    boundaries_list=[]
    #boundaries_list = [[.1,.3],[0,.6],[2.7,3.1]]
    contour_labels = ['h=.1','h=.05', 'h=.02', 'h=.01', 'h=.005']

    for i in range( all_data[0].shape[0]):
        labels.append('$\\theta_' + str(i+1) + '$')


    label_separation_x = 0.1

    if presentationVersion:
       fig = plotTools.MakeFigure(1200,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(600,.9, presentationVersion)
    kde_matrix.kde_matrix(all_data, labels=labels, ax=plt.gca(), label_separation_x=label_separation_x,contour_labels=contour_labels, boundaries_list=boundaries_list)

#    fig.tight_layout(pad=0.1)


    if saveToFile:
        if presentationVersion:
            plt.savefig('plots/' + referenceName + '_' + str(subsample) + '_density.pdf', format='pdf')
        else:
            plt.savefig('plots/' + referenceName + '_perfect_density_paper.pdf', format='pdf')
#        plt.close(fig)


def PlotStepSequenceLorenz(randomVersion, presentationVersion, saveToFile, subsample=100):
    #hdf5File = 'results/' + exampleName + '.h5'
    if randomVersion:
        all_data = [np.array(CompareIncrementalCovariances_2.ReferenceChain('results/LorenzParam.h5_rank_0','Lorenz'))[0:3,::subsample],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/LorenzParam.h5_rank_1','Lorenz'))[0:3,::subsample],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/LorenzParam.h5_rank_2','Lorenz'))[0:3,::subsample],
                ]
        referenceName = 'fitz_randomSolver'
    else:
        all_data = [np.array(CompareIncrementalCovariances_2.ReferenceChain('results/LorenzParam.h5_rank_3','Lorenz'))[:,::subsample],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/LorenzParam.h5_rank_4','Lorenz'))[:,::subsample],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/LorenzParam.h5_rank_5','Lorenz'))[:,::subsample]
                ]
        referenceName = 'lorenz_deterministicSolver'
    import kde_matrix
    labels = []


    contour_labels = ['h=.025','h=.01', 'h=.005']

    for i in range( all_data[0].shape[0]):
        labels.append('$\\theta_' + str(i+1) + '$')


    label_separation_x = 0.1

    if presentationVersion:
       fig = plotTools.MakeFigure(800,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(600,.9, presentationVersion)
    kde_matrix.kde_matrix(all_data, labels=labels, ax=plt.gca(), label_separation_x=label_separation_x,contour_labels=contour_labels)

#    fig.tight_layout(pad=0.1)


    if saveToFile:
        if presentationVersion:
            plt.savefig('plots/' + referenceName + '_density.pdf', format='pdf')
        else:
            plt.savefig('plots/' + referenceName + '_perfect_density_paper.pdf', format='pdf')
#        plt.close(fig)

# PlotDensity('results/fitz_euler_multi.h5_rank_0', 'Fitz', '1', True, True, subsample=10)
# PlotDensity('results/fitz_euler_multi.h5_rank_1', 'Fitz', '2', True, True, subsample=10)
# PlotDensity('results/fitz_euler_multi.h5_rank_2', 'Fitz', '3', True, True, subsample=10)
# PlotDensity('results/fitz_euler.h5_rank_3', 'Fitz', '1', True, True, subsample=10)
# PlotDensity('results/fitz_euler.h5_rank_4', 'Fitz', '2', True, True, subsample=10)
# PlotDensity('results/fitz_euler.h5_rank_5', 'Fitz', '3', True, True, subsample=10)

# PlotDensity('results/fitz_euler_smaller.h5_rank_0', 'Fitz', '1', True, True, subsample=10)
# PlotDensity('results/fitz_euler_smaller.h5_rank_1', 'Fitz', '2', True, True, subsample=10)
# PlotDensity('results/fitz_euler_smaller.h5_rank_2', 'Fitz', '3', True, True, subsample=10)
# PlotDensity('results/fitz_euler_smaller.h5_rank_3', 'Fitz', '3', True, True, subsample=10)


# PlotDensity('results/fitz_euler_bats.h5_rank_0', 'Fitz', '1', True, True, subsample=10)
# PlotDensity('results/fitz_euler_bats.h5_rank_1', 'Fitz', '2', True, True, subsample=10)
# PlotDensity('results/fitz_euler_bats.h5_rank_2', 'Fitz', '3', True, True, subsample=10)
# PlotDensity('results/fitz_euler_bats.h5_rank_3', 'Fitz', '3', True, True, subsample=10)
# PlotDensity('results/fitz_euler_bats.h5_rank_4', 'Fitz', '3', True, True, subsample=10)

# PlotDensity('results/LorenzParam.h5_rank_0', 'Lorenz', '1', True, False, subsample=10)
# PlotDensity('results/LorenzParam.h5_rank_1', 'Lorenz', '2', True, False, subsample=10)
# PlotDensity('results/LorenzParam.h5_rank_2', 'Lorenz', '3', True, False, subsample=10)
# PlotDensity('results/LorenzParam.h5_rank_3', 'Lorenz', '1', True, False, subsample=50)
# PlotDensity('results/LorenzParam.h5_rank_4', 'Lorenz', '2', True, False, subsample=50)
# PlotDensity('results/LorenzParam.h5_rank_5', 'Lorenz', '3', True, False, subsample=50)

# PlotStepSequence(False, True, True, subsample=1000)
# PlotStepSequence(True, True, True, subsample=10)
# PlotStepSequence(False, True, True, subsample=10)
# PlotStepSequence(True, True, True, subsample=10)



# PlotDensity('results/Repress.h5_rank_5', 'Repress', '3', True, False, subsample=50)
# PlotStepSequenceRepressRk2(False, True, True, subsample=50)
# PlotStepSequenceRepressRk2(True, True, True, subsample=50)
# PlotStepSequenceRepressRk4(False, True, False, subsample=500)
# PlotStepSequenceRepressRk4(True, True, False, subsample=500)


# PlotStepSequenceLorenz(False, True, True, subsample=100)
# PlotStepSequenceLorenz(True, True, True, subsample=100)


PlotStepSequence('Fitz', 'rand', 'results/fitz_paper_inv.h5', False, False, subsample=1000)
# PlotStepSequence('Fitz', 'det', 'results/fitz_paper_inv.h5', False, True, subsample=100)
# PlotStepSequence('Fitz', 'ind', 'results/fitz_paper_inv.h5', False, True, subsample=100)

# PlotStepSequence('Elliptic1D_inv', 'det', '../pdes/results/elliptic1d_paper_inv.h5', False, True, subsample=100)
# PlotStepSequence('Elliptic1D_inv', 'rand', '../pdes/results/elliptic1d_paper_inv.h5', False, True, subsample=100)
# PlotStepSequence('Elliptic1D_inv', 'det', '../pdes/results/elliptic1d_paper_inv.h5', True, True, subsample=100)
# PlotStepSequence('Elliptic1D_inv', 'rand', '../pdes/results/elliptic1d_paper_inv.h5', True, True, subsample=100)

# PlotStepSequence('Repress', 'det', 'results/Repress_paper_inv.h5', True, True, subsample=100)
# PlotStepSequence('Repress', 'rand', 'results/Repress_paper_inv.h5', True, True, subsample=100)
# PlotStepSequence('Repress', 'ind', 'results/Repress_paper_inv.h5', True, True, subsample=100)
# PlotStepSequence('Repress', 'ind', 'results/Repress_paper_invAr2.h5', True, True, subsample=100)


# PlotStepSequence('Repress', 'det', 'results/Repress_paper_inv.h5', False, True, subsample=100)
# PlotStepSequence('Repress', 'rand', 'results/Repress_paper_inv.h5', False, True, subsample=100)
# PlotStepSequence('Repress', 'ind', 'results/Repress_paper_inv.h5', False, True, subsample=100)




plt.show()

