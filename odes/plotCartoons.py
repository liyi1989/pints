import plotTools

import numpy as np
import matplotlib.pyplot as plt

def GenerateIID(x):
    coeff = np.zeros([6])

    y = np.zeros_like(x)

    import random

    for i in range(coeff.shape[0]):
        coeff[i] = random.gauss(0,1)

    import math

    for i in range(x.shape[0]):
        for j in range(coeff.shape[0]):
            y[i] += coeff[j] * pow(j+1.0, -.5) * math.sin(j*x[i]*math.pi/2)

    return y

def GenerateIID_fem(x):
    coeff = np.zeros([8])

    y = np.zeros_like(x)

    import random

    for i in range(coeff.shape[0]):
        coeff[i] = random.gauss(0,1)

    import math

    for i in range(x.shape[0]):
        for j in range(coeff.shape[0]):
            y[i] += coeff[j] * pow(j+1.0, -.5) * math.sin(j*x[i]*math.pi)

    return y

def EulerRhs(x):
    import math

    return math.cos(x*.7)

def MakeEulerDet(presentationVersion, saveToFile):
    if presentationVersion:
        fig = plotTools.MakeFigure(400,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(600,.9, presentationVersion)


    euler_x = np.linspace(0,4, 5)
    euler_y = np.zeros_like(euler_x)
    euler_y[0] = 0
    for i in range(euler_x.shape[0]-1):
        euler_y[i+1] = euler_y[i]+EulerRhs(euler_x[i])

    plot_x = np.linspace(0,4, 500)
    import scipy.interpolate
    plot_y = scipy.interpolate.interp1d(euler_x, euler_y)(plot_x)

    plt.ylabel('u')
    plt.xlabel('t')
    plt.plot(euler_x,euler_y, 'o-',color=plotTools.PlotColor('blue'))

    plt.ylim([0,2.5])
    plt.xticks([0,1,2,3,4])
    plt.gca().set_xticklabels(['$0$','$h$','$2h$','$3h$','$4h$'])

    # fig.tight_layout(pad=0.2)
    if saveToFile:
        if presentationVersion:
            plt.savefig('plots/euler_det_illustration_presentation.pdf', format='pdf',bbox_inches='tight')
        else:
            plt.savefig('plots/euler_det_illustration_paper.pdf', format='pdf')


def MakeEulerRand(presentationVersion, saveToFile):
    if presentationVersion:
        fig = plotTools.MakeFigure(400,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(600,.9, presentationVersion)

    for rep in range(1):
        euler_x = np.linspace(0,4, 5)
        euler_y = np.zeros_like(euler_x)
        perturb_linear = np.zeros_like(euler_x)

        perturb_x = np.linspace(0,1,100)
        all_perturb = []
        euler_y[0] = 0
        perturb_linear[0]=0
        for i in range(euler_x.shape[0]-1):
            perturb_y = .3*GenerateIID(perturb_x)
            euler_y[i+1] = euler_y[i]+EulerRhs(euler_x[i]) + perturb_y[perturb_y.shape[0]-1]
            perturb_linear[i+1] = perturb_y[perturb_y.shape[0]-1]

            all_perturb = np.concatenate((all_perturb,perturb_y), axis=0)

        plot_x = np.linspace(0,4, 400)
        import scipy.interpolate
        plot_y = scipy.interpolate.interp1d(euler_x, euler_y)(plot_x)

        plt.ylabel('u')
        plt.xlabel('t')
        # plt.plot(euler_x,euler_y, 'o-', color=plotTools.PlotColor('blue'))
        # plt.plot(plot_x,plot_y+all_perturb-plot_perturb_linear, color=plotTools.PlotColor('red'))

        plots = []
        for i in range(euler_x.shape[0]-1):

            plot_perturb_linear = scipy.interpolate.interp1d([0,1],[0,perturb_linear[i+1]] )(perturb_x)

            plots.append(plt.plot(plot_x[i*100:(i+1)*100],plot_y[i*100:(i+1)*100]+all_perturb[i*100:(i+1)*100]-plot_perturb_linear, color=plotTools.PlotColor('red')))
            plots.append(plt.plot([euler_x[i], euler_x[i+1]], [euler_y[i],euler_y[i]+EulerRhs(euler_x[i])], 'o-', color=plotTools.PlotColor('blue')))

    plt.legend((plots[1][0],plots[0][0]), ['Standard Euler', 'Perturbed Euler'], 'lower right')
    # fig.tight_layout(pad=0.2)
    plt.ylim([0,2.5])

    plt.xticks([0,1,2,3,4])
    plt.gca().set_xticklabels(['$0$','$h$','$2h$','$3h$','$4h$'])
    if saveToFile:
        if presentationVersion:
            plt.savefig('plots/euler_rand_illustration_presentation.pdf', format='pdf', bbox_inches='tight')
        else:
            plt.savefig('plots/euler_rand_illustration_paper.pdf', format='pdf')



def MakeHatDet(presentationVersion, saveToFile):
    if presentationVersion:
        fig = plotTools.MakeFigure(400,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(600,.9, presentationVersion)


    euler_x = np.linspace(0,4, 5)
    euler_y = np.zeros_like(euler_x)
    euler_y[0] = 0
    for i in range(euler_x.shape[0]-1):
        euler_y[i+1] = euler_y[i]+EulerRhs(euler_x[i])

    plot_x = np.linspace(0,4, 500)
    import scipy.interpolate
    plot_y = scipy.interpolate.interp1d(euler_x, euler_y)(plot_x)

    plt.ylabel('u')
    plt.xlabel('x')



    for i in range(4):
        plt.plot([i,i+1],[0,1],'o-',color=plotTools.PlotColor('blue'))
        plt.plot([i,i+1],[1,0],'o-',color=plotTools.PlotColor('blue'))
    plt.xticks([0,1,2,3,4])
    plt.gca().set_xticklabels(['$0$','$h$','$2h$','$3h$','$4h$'])
    plt.ylim([-.5,2])
    fig.tight_layout(pad=0.1)

    plt.xticks([0,1,2,3,4])
    plt.gca().set_xticklabels(['$0$','$h$','$2h$','$3h$','$4h$'])
    if saveToFile:
        if presentationVersion:
            plt.savefig('plots/fem_det_illustration_presentation.pdf', format='pdf')
        else:
            plt.savefig('plots/fem_det_illustration_paper.pdf', format='pdf')

def MakeHatRand(presentationVersion, saveToFile):
    if presentationVersion:
        fig = plotTools.MakeFigure(400,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(600,.9, presentationVersion)


    euler_x = np.linspace(0,4, 5)
    euler_y = np.zeros_like(euler_x)
    euler_y[0] = 0
    for i in range(euler_x.shape[0]-1):
        euler_y[i+1] = euler_y[i]+EulerRhs(euler_x[i])

    plot_x = np.linspace(0,4, 500)
    import scipy.interpolate
    plot_y = scipy.interpolate.interp1d(euler_x, euler_y)(plot_x)

    plt.ylabel('u')
    plt.xlabel('x')


    plots = []
    for i in range(4):



        perturb_x = np.linspace(0,1,100)
        plot_perturb_linear = scipy.interpolate.interp1d([0,1],[0,1])(perturb_x)


        perturb_y = .1*GenerateIID_fem(perturb_x)
        print perturb_y
        plots.append(plt.plot(perturb_x+i, plot_perturb_linear + perturb_y, color=plotTools.PlotColor('red')))


        perturb_y = .1*GenerateIID_fem(perturb_x)
        plot_perturb_linear = scipy.interpolate.interp1d([0,1],[1,0])(perturb_x)
        plots.append(plt.plot(perturb_x+i, plot_perturb_linear + perturb_y, color=plotTools.PlotColor('red')))

        plots.append(plt.plot([i,i+1],[0,1],'o-',color=plotTools.PlotColor('blue')))
        plt.plot([i,i+1],[1,0],'o-',color=plotTools.PlotColor('blue'))
    plt.ylim([-.5,2])
    plt.legend((plots[2][0],plots[0][0]), ['Standard hat basis', 'Randomized hat basis'], 'upper right')

    plt.xticks([0,1,2,3,4])
    plt.gca().set_xticklabels(['$0$','$h$','$2h$','$3h$','$4h$'])
    fig.tight_layout(pad=0.1)
    if saveToFile:
        if presentationVersion:
            plt.savefig('plots/fem_rand_illustration_presentation.pdf', format='pdf')
        else:
            plt.savefig('plots/fem_rand_illustration_paper.pdf', format='pdf')

MakeEulerDet(False, True)
MakeEulerDet(True, True)

MakeEulerRand(False, True)
MakeEulerRand(True, True)

MakeHatDet(False, True)
MakeHatDet(True, True)


MakeHatRand(False, True)
MakeHatRand(True, True)

plt.show()