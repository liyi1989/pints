

#include "MUQ/Modelling/ModPieceTemplates.h"

#include "MUQ/Utilities/RandomGenerator.h"
#include "MUQ/Inference/ProblemClasses/SamplingProblem.h"
#include "MUQ/Inference/MCMC/MCMCState.h"
#include "MUQ/Modelling/GaussianDensity.h"
#include "MUQ/Inference/MCMC/MHKernel.h"
#include "MUQ/Inference/MCMC/DRKernel.h"
#include "MUQ/Modelling/GaussianRV.h"

using namespace std;
using namespace Eigen;
using namespace muq::Modelling;
using namespace muq::Utilities;
using namespace muq::Inference;
namespace py = boost::python;


//create a base class for ODEs that implement a different method, so we can reuse space and optimize the inner loop
class OdeRhsPiece : public muq::Modelling::ModPiece{
public:  
  OdeRhsPiece(Eigen::VectorXi const& inputSizes,
           int const              outputSize,
           bool const             hasDirectGradient,
           bool const             hasDirectJacobian,
           bool const             hasDirectJacobianAction,
           bool const             hasDirectHessian,
           bool const             isRandom): ModPiece( inputSizes,
                       outputSize,
                  hasDirectGradient,
                   hasDirectJacobian,
                      hasDirectJacobianAction,
                       hasDirectHessian,
                       isRandom){
	     
	     
  }
  
  virtual ~OdeRhsPiece() = default;
  
  virtual void RhsDirect(VectorXd const& state, VectorXd const& param, VectorXd &result) = 0;
  
  
  //simple impl that 
    virtual VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& input) override{
     VectorXd result(outputSize);
     
     RhsDirect(input.at(0), input.at(1), result);
     return result;
      
      
    }

};


class LorenzRhs : public OdeRhsPiece {
public:
  
    static Eigen::VectorXi LorenzSize(){
    VectorXi size(2);
    size(0) = 3;
    size(1) = 3;
    return size;}

  LorenzRhs() : OdeRhsPiece(LorenzSize(), 3, false, false, false, false, false) {}

  virtual ~LorenzRhs() = default;

private:

  virtual void RhsDirect(VectorXd const& state, VectorXd const& param, VectorXd &result) override
  {
    result.resize(3);
    
    	double sigma = param(0);
    double beta = param(1);
    double rho = param(2);
    

    result(0) = sigma * (state(1) - state(0));
    result(1) = state(0) * (rho - state(2)) - state(1);
    result(2) = state(0) * state(1) - beta * state(2);
  }

};


class FitzRhs : public OdeRhsPiece {
public:

  static Eigen::VectorXi FitzSize(){
    VectorXi size(2);
    size(0) = 2;
    size(1) = 3;
    return size;}
  
  FitzRhs() : OdeRhsPiece(FitzSize(), 2, false, false, false, false, false) {}

  virtual ~FitzRhs() = default;

private:

  virtual void RhsDirect(VectorXd const& state, VectorXd const& param, VectorXd &result) override
  {
    result.resize(2);
        double v = state(0);
         double r = state(1);
	double a = param(0);
    double b = param(1);
    double c = param(2);

    result(0) = c*(v - v*v*v/3 + r);
    result(1) = -(v-a+b*r)/c;
      }

};



class EulerOdeSolver : public muq::Modelling::ModPiece {
public:
  EulerOdeSolver(shared_ptr<OdeRhsPiece> rhs, double stepSize, double finishTime, double obsInterval) : 
  ModPiece(rhs->inputSizes,rhs->inputSizes(0)* (static_cast<unsigned>(finishTime/obsInterval)), false, false,false,false, false),
  rhs(rhs), stateSize(rhs->inputSizes(0)), stepSize(stepSize), numObs(finishTime/obsInterval),stepsPerObs(obsInterval/stepSize) 
  {
    
    
    
  }

  virtual ~EulerOdeSolver() = default;

private:

  virtual VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& allInputs) override
  {
    VectorXd state = allInputs.at(0);
    VectorXd parameters = allInputs.at(1);
    
    VectorXd result(outputSize);

    VectorXd deriv;
    deriv.resizeLike(state);

    for(unsigned i=0; i<numObs; ++i){

		for(unsigned j=0; j<stepsPerObs; ++j){
		  rhs->RhsDirect(state, parameters, deriv);
     state = state + stepSize*deriv;
      }
      
            result.segment(i*stateSize,stateSize) = state; //collect the correct states

  }
  
  return result;
}
  
  shared_ptr<OdeRhsPiece> rhs;

  unsigned stateSize;
  double   stepSize;
  unsigned numObs;
  unsigned stepsPerObs;
};



class RandomOdeSolver : public muq::Modelling::ModPiece {
public:
  RandomOdeSolver(shared_ptr<OdeRhsPiece> rhs, double stepSize, double finishTime, double obsInterval, double noiseScale) : 
  
  ModPiece(rhs->inputSizes,rhs->inputSizes(0)* (static_cast<unsigned>(finishTime/obsInterval)), false, false,false,false, true),
  noiseScale(noiseScale),rhs(rhs), stateSize(rhs->inputSizes(0)), stepSize(stepSize), numObs(finishTime/obsInterval),stepsPerObs(obsInterval/stepSize)
  {
    
    
    
  }

  virtual ~RandomOdeSolver() = default;
  double   noiseScale;

private:

  virtual VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& allInputs) override
  {
    
    VectorXd state = allInputs.at(0);
        VectorXd parameters = allInputs.at(1);

    VectorXd deriv;
    deriv.resizeLike(state);
    
    VectorXd result(outputSize);

    for(unsigned i=0; i<numObs; ++i){

      for(unsigned j=0; j<stepsPerObs; ++j){
	rhs->RhsDirect(state,parameters, deriv); 
     state = state + stepSize*deriv + noiseScale*pow(stepSize, 1.5)*RandomGenerator::GetNormalRandomVector(stateSize);
      }
      
      result.segment(i*stateSize,stateSize) = state; //collect the correct states
  }

  return result;
}
  
  shared_ptr<OdeRhsPiece> rhs;

    unsigned stateSize;
  double   stepSize;
  unsigned numObs;
  unsigned stepsPerObs;
  
  
};


class RandomRk2 : public muq::Modelling::ModPiece {
public:
  RandomRk2(shared_ptr<OdeRhsPiece> rhs, double stepSize, double finishTime, double obsInterval, double noiseScale) : 
  
  ModPiece(rhs->inputSizes,rhs->inputSizes(0)* (static_cast<unsigned>(finishTime/obsInterval)), false, false,false,false, true),
  rhs(rhs), stateSize(rhs->inputSizes(0)), stepSize(stepSize), numObs(finishTime/obsInterval),stepsPerObs(obsInterval/stepSize) , noiseScale(noiseScale)
  {
    
    
    
  }

  virtual ~RandomRk2() = default;

private:

  virtual VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& allInputs) override
  {
    
    VectorXd state = allInputs.at(0);
            VectorXd parameters = allInputs.at(1);

// 	    cout << "rk2 state " << state.transpose() << endl;
// 	    	    	    cout << "rk2 parameters " << parameters.transpose() << endl;

    VectorXd result = VectorXd::Zero(outputSize);
  unsigned stateSize = state.rows();
  
   VectorXd k1(stateSize);
     VectorXd k2(stateSize);
    for(unsigned i=0; i<numObs; ++i){
      for(unsigned j=0; j<stepsPerObs; ++j){
	
	 rhs->RhsDirect(state, parameters, k1);
	rhs->RhsDirect(state + 0.5*stepSize*k1, parameters, k2);


     state = state + stepSize * k2 + noiseScale*pow(stepSize, 2.5)*RandomGenerator::GetNormalRandomVector(stateSize);
      }
      
//        cout << "saving " << state.transpose() << endl;
      result.segment(i*stateSize,stateSize) = state; //collect the correct states
  }

  return result;
}
  
  shared_ptr<OdeRhsPiece> rhs;

    const unsigned stateSize;
  double   stepSize;
  const unsigned numObs;
  const unsigned stepsPerObs;
public:
  double   noiseScale;
  
};




class RandomRk3 : public muq::Modelling::ModPiece {
public:
  RandomRk3(shared_ptr<OdeRhsPiece> rhs, double stepSize, double finishTime, double obsInterval, double noiseScale) : 
  
  ModPiece(rhs->inputSizes,rhs->inputSizes(0)* (static_cast<unsigned>(finishTime/obsInterval)), false, false,false,false, true),
  rhs(rhs), stateSize(rhs->inputSizes(0)), stepSize(stepSize), numObs(finishTime/obsInterval),stepsPerObs(obsInterval/stepSize) , noiseScale(noiseScale)
  {
    
    
    
  }

  virtual ~RandomRk3() = default;

private:

  virtual VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& allInputs) override
  {
    VectorXd state = allInputs.at(0);
            VectorXd parameters = allInputs.at(1);

// 	    	    cout << "rk3 state " << state.transpose() << endl;
// 	    	    cout << "rk3 parameters " << parameters.transpose() << endl;

    VectorXd result = VectorXd::Zero(outputSize);
	
  unsigned stateSize = state.rows();
  
    VectorXd k1;
      VectorXd k2;
	VectorXd k3;
    for(unsigned i=0; i<numObs; ++i){
      for(unsigned j=0; j<stepsPerObs; ++j){
// 		  cout << " ij " << i << " " << j << endl;
	rhs->RhsDirect(state, parameters, k1);
	rhs->RhsDirect(state + 0.5*stepSize*k1, parameters, k2);
	rhs->RhsDirect(state + -stepSize*k1 + 2.0*stepSize*k2, parameters, k3);

	state = state + stepSize/6.0 * (k1 + 4.0*k2 + k3) + noiseScale*pow(stepSize, 3.5)*RandomGenerator::GetNormalRandomVector(stateSize);
	
      }
 
//       cout << "saving " << state.transpose() << endl;
      result.segment(i*stateSize,stateSize) = state; //collect the correct states

		
	}
	


  return result;
}
  shared_ptr<OdeRhsPiece> rhs;

    unsigned stateSize;
  double   stepSize;
  unsigned numObs;
  unsigned stepsPerObs;
  public:
  double   noiseScale;
  
};

class RandomRk4 : public muq::Modelling::ModPiece {
public:
  RandomRk4(shared_ptr<OdeRhsPiece> rhs, double stepSize, double finishTime, double obsInterval, double noiseScale) : 
  
  ModPiece(rhs->inputSizes,rhs->inputSizes(0)* (static_cast<unsigned>(finishTime/obsInterval)), false, false,false,false, true),
  rhs(rhs), stateSize(rhs->inputSizes(0)), stepSize(stepSize), numObs(finishTime/obsInterval),stepsPerObs(obsInterval/stepSize) , noiseScale(noiseScale)
  {
    
    
    
  }

  virtual ~RandomRk4() = default;

private:

  virtual VectorXd EvaluateImpl(std::vector<Eigen::VectorXd> const& allInputs) override
  {
    
    VectorXd state = allInputs.at(0);
            VectorXd parameters = allInputs.at(1);

	
    VectorXd result = VectorXd::Zero(outputSize);
	
  unsigned stateSize = state.rows();
  
    VectorXd k1;
      VectorXd k2;
	VectorXd k3;
	  VectorXd k4;
    for(unsigned i=0; i<numObs; ++i){
      for(unsigned j=0; j<stepsPerObs; ++j){
// 		  cout << " ij " << i << " " << j << endl;
	rhs->RhsDirect(state, parameters, k1);
	rhs->RhsDirect(state + 0.5*stepSize*k1, parameters, k2);
	rhs->RhsDirect(state + 0.5*stepSize*k2, parameters, k3);
	rhs->RhsDirect(state + stepSize*k3, parameters, k4);

	state = state + stepSize/6.0 * (k1 + 2.0*k2 + 2.0*k3 + k4) + noiseScale*pow(stepSize, 4.5)*RandomGenerator::GetNormalRandomVector(stateSize);
      }
 
//       cout << "saving " << state.transpose() << endl;
      result.segment(i*stateSize,stateSize) = state; //collect the correct states

		
	}
	


  return result;
}
  shared_ptr<OdeRhsPiece> rhs;

    unsigned stateSize;
  double   stepSize;
  unsigned numObs;
  unsigned stepsPerObs;
  public:
  double   noiseScale;
  
};




double AveragePosterior(shared_ptr<muq::Modelling::ModPiece> piece, boost::python::list const& input, int num){
  VectorXd result(num);
  VectorXd inputEig = GetEigenVector<Eigen::VectorXd>(input);

  for(int i=0; i<num; ++i){
    result(i) = piece->Evaluate(inputEig)(0);
  }
  return result.sum()/static_cast<double>(num);
};




class FitzSamplingProblem : public muq::Inference::AbstractSamplingProblem{
public:
	FitzSamplingProblem(py::list priorMean, py::list priorVar, py::list data, double obsVar, unsigned numRuns, double stepSize, double obsInterval, double finishTime, double noise):AbstractSamplingProblem(3, nullptr, nullptr),
	numRuns(numRuns)
	{
				VectorXd priorMeanVec = GetEigenVector<VectorXd>(priorMean);

		VectorXd priorVarVec = GetEigenVector<VectorXd>(priorVar);
		prior = make_shared<GaussianDensity>(priorMeanVec, priorVarVec, GaussianSpecification::DiagonalCovariance);
		VectorXd dataVec = GetEigenVector<VectorXd>(data);
		likelihood = make_shared<GaussianDensity>(dataVec, obsVar);
		dataStd = sqrt(obsVar);
// 		discrepancy = make_shared<GaussianDensity>(dataVec*0.0, obsVar);

		// 		cout << "making with " << stepSize << endl;
		betterSolver = make_shared<RandomOdeSolver>( make_shared<FitzRhs>(), stepSize, finishTime, obsInterval, 1.0);
		worseSolver = make_shared<RandomOdeSolver>( make_shared<FitzRhs>(), stepSize*2.0, finishTime, obsInterval, 1.0);
		
		betterSolver->noiseScale = noise;
		initialCondition.resize(2);
		initialCondition(0) = -1;
			initialCondition(1) = 1;
	}
	virtual ~FitzSamplingProblem() = default;
	
	
		double DensityHelper(py::list state, double noise){
	  				VectorXd stateVec = GetEigenVector<VectorXd>(state);
					VectorXd goodState(4);
					goodState << stateVec, noise;
					auto fullState = ConstructState(goodState);
					if(fullState){
					return fullState->GetDensity();
					}
					else{
					 return numeric_limits<double>::quiet_NaN(); 
					}

	}
	
	
	
	  virtual void                                    InvalidateCaches() override{};
	  
  virtual std::shared_ptr<MCMCState> ConstructState(Eigen::VectorXd const& state, int const currIteration = 0, std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry = nullptr) override {
// 	cout << "constructing " << state.transpose() << endl;
	  double logPrior = prior->LogDensity(state);
	  double logLikelihood =0;
	  

MatrixXd forwardModelsBetter(betterSolver->outputSize, numRuns);

	  for(unsigned i=0; i<numRuns; ++i){

// 		  		  	cout << "give model" <<state.head(3)<< endl;

	   
		   forwardModelsBetter.col(i)= betterSolver->Evaluate(initialCondition, state.head(3));	  
		  logLikelihood += likelihood->LogDensity(forwardModelsBetter.col(i));

	  }
	    logLikelihood /= static_cast<double>(numRuns);
// 	  	cout << "logLikelihood " << logLikelihood<< endl;

			

if( std::isfinite( logLikelihood) && std::isfinite( logPrior)){
	  
	
	  return make_shared<muq::Inference::MCMCState>(state, logLikelihood,
            boost::optional<Eigen::VectorXd>(),
            boost::optional<double>(logPrior),
            boost::optional<Eigen::VectorXd>(),
            boost::optional<Eigen::MatrixXd>(),
//             boost::optional<Eigen::VectorXd>(VectorXd::Constant(1, batdist)));
		  boost::optional<Eigen::VectorXd>());
}
else
{
	cout << "bad constructed state, return null: "<< state.transpose() << endl;
	return nullptr;
	
}
	  
  };

  int numRuns;
  double dataStd;
  shared_ptr<GaussianDensity> prior;
  shared_ptr<GaussianDensity> likelihood;	
  shared_ptr<GaussianDensity> discrepancy;
  shared_ptr<RandomOdeSolver> betterSolver;
  shared_ptr<RandomOdeSolver> worseSolver;
  VectorXd initialCondition;
};







class FitzSamplingProblemIndicator : public muq::Inference::AbstractSamplingProblem{
public:
	FitzSamplingProblemIndicator(py::list priorMean, py::list priorVar, py::list data, double obsVar, unsigned numRuns, double stepSize, double obsInterval, double finishTime):AbstractSamplingProblem(3, nullptr, nullptr),
	numRuns(numRuns)
	{
				VectorXd priorMeanVec = GetEigenVector<VectorXd>(priorMean);

		VectorXd priorVarVec = GetEigenVector<VectorXd>(priorVar);
		prior = make_shared<GaussianDensity>(priorMeanVec, priorVarVec, GaussianSpecification::DiagonalCovariance);
		VectorXd dataVec = GetEigenVector<VectorXd>(data);
		likelihood = make_shared<GaussianDensity>(dataVec, obsVar);
		dataStd = sqrt(obsVar);
// 		discrepancy = make_shared<GaussianDensity>(dataVec*0.0, obsVar);

		// 		cout << "making with " << stepSize << endl;
		betterSolver = make_shared<EulerOdeSolver>( make_shared<FitzRhs>(), stepSize, finishTime, obsInterval);
		worseSolver = make_shared<EulerOdeSolver>( make_shared<FitzRhs>(), stepSize*2.0, finishTime, obsInterval);
		
		initialCondition.resize(2);
		initialCondition(0) = -1;
			initialCondition(1) = 1;
	}
	virtual ~FitzSamplingProblemIndicator() = default;
	
	
		double DensityHelper(py::list state, double noise){
	  				VectorXd stateVec = GetEigenVector<VectorXd>(state);
					VectorXd goodState(4);
					goodState << stateVec, noise;
					auto fullState = ConstructState(goodState);
					if(fullState){
					return fullState->GetDensity();
					}
					else{
					 return numeric_limits<double>::quiet_NaN(); 
					}

	}
	
	
	
	  virtual void                                    InvalidateCaches() override{};
	  
  virtual std::shared_ptr<MCMCState> ConstructState(Eigen::VectorXd const& state, int const currIteration = 0, std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry = nullptr) override {
// 	cout << "constructing " << state.transpose() << endl;
	  double logPrior = prior->LogDensity(state);
	  double logLikelihood =0;

MatrixXd forwardModelsBetter(betterSolver->outputSize, numRuns);

	  for(unsigned i=0; i<numRuns; ++i){

// 		  		  	cout << "give model" <<state.head(3)<< endl;

	    
	    VectorXd betterSolution = betterSolver->Evaluate(initialCondition, state.head(3));
	    VectorXd worseSolution = worseSolver->Evaluate(initialCondition, state.head(3));
	   
		   forwardModelsBetter.col(i)= betterSolution + 0.5*((betterSolution-worseSolution).array() * RandomGenerator::GetNormalRandomVector(betterSolution.rows()).array()).matrix();	  
		  logLikelihood += likelihood->LogDensity(forwardModelsBetter.col(i));

	  }
	    logLikelihood /= static_cast<double>(numRuns);

if( std::isfinite( logLikelihood) && std::isfinite( logPrior)){
	  
	
	  return make_shared<muq::Inference::MCMCState>(state, logLikelihood,
            boost::optional<Eigen::VectorXd>(),
            boost::optional<double>(logPrior),
            boost::optional<Eigen::VectorXd>(),
            boost::optional<Eigen::MatrixXd>(),
//             boost::optional<Eigen::VectorXd>(VectorXd::Constant(1, batdist)));
		  boost::optional<Eigen::VectorXd>());
}
else
{
	cout << "bad constructed state, return null: "<< state.transpose() << endl;
	return nullptr;
	
}
	  
  };

  int numRuns;
  double dataStd;
  shared_ptr<GaussianDensity> prior;
  shared_ptr<GaussianDensity> likelihood;	
  shared_ptr<GaussianDensity> discrepancy;
  shared_ptr<EulerOdeSolver> betterSolver;
  shared_ptr<EulerOdeSolver> worseSolver;
  VectorXd initialCondition;
};







class FitzForwardScaleProblem : public muq::Inference::AbstractSamplingProblem{
public:
	FitzForwardScaleProblem(double accuracyFloor, unsigned numRuns, double stepSize, double obsInterval, double finishTime):AbstractSamplingProblem(1, nullptr, nullptr),
	numRuns(numRuns), accuracyFloor(accuracyFloor),stepSize(stepSize)
	{
		betterSolver = make_shared<RandomOdeSolver>( make_shared<FitzRhs>(), stepSize, finishTime, obsInterval, 1.0);
		worseSolver = make_shared<RandomOdeSolver>( make_shared<FitzRhs>(), stepSize*2.0, finishTime, obsInterval, 1.0);
					
		initialCondition.resize(2);
		initialCondition(0) = -1;
			initialCondition(1) = 1;
					
			param.resize(3);
			param(0) = .2;
			param(1) = .2;
			param(2) = 3;
			
	}
	virtual ~FitzForwardScaleProblem() = default;
	
	
	  virtual void                                    InvalidateCaches() override{};
	  
	  
  virtual std::shared_ptr<MCMCState> ConstructState(Eigen::VectorXd const& state, int const currIteration = 0, std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry = nullptr) override {
// 	cout << "constructing " << state.transpose() << endl;
	  double logPrior = 0;
	  double logLikelihood =0;
	  
	  
	    unsigned numParam = 20;
	  for(unsigned j=0; j<numParam; ++j){

	    
	    VectorXd coeff = param + 0.1*RandomGenerator::GetNormalRandomVector(3);
	    
	    
	    
	   worseSolver->noiseScale = 0;
	  betterSolver->noiseScale = 0;
	  VectorXd betterReference =  betterSolver->Evaluate(initialCondition, coeff);	  
VectorXd worseReference =  worseSolver->Evaluate(initialCondition, coeff);	

double noiseScale = pow(10.0,state(0));
	  worseSolver->noiseScale = noiseScale;
	  betterSolver->noiseScale = noiseScale;

MatrixXd forwardModelsBetter(betterSolver->outputSize, numRuns);
MatrixXd forwardModelsWorse(betterSolver->outputSize, numRuns);

	  for(unsigned i=0; i<numRuns; ++i){


		   forwardModelsBetter.col(i)= betterSolver->Evaluate(initialCondition, coeff);	  
		   forwardModelsWorse.col(i) = worseSolver->Evaluate(initialCondition, coeff);	  

	  }
		
		
// 		//single variate battacharyya
	  VectorXd betterMu = forwardModelsBetter.rowwise().mean();
	  VectorXd worseMu = forwardModelsWorse.rowwise().mean();
	  VectorXd betterSigma  = (forwardModelsBetter.colwise() - betterMu).rowwise().squaredNorm()/(numRuns-1.0);
  	  VectorXd worseSigma  = (forwardModelsWorse.colwise() - worseMu).rowwise().squaredNorm()/(numRuns-1.0);
	  	  VectorXd sumSigs = (betterSigma + worseSigma + VectorXd::Constant(betterSigma.rows(), accuracyFloor*accuracyFloor)); //modify bats to add an absolute scale

		  VectorXd floor = VectorXd::Constant(betterSigma.rows(), accuracyFloor*accuracyFloor);

 		  VectorXd refSig = (betterReference - worseReference).array().square();
		  

		   		   VectorXd bats = .25*log(.25*(refSig.array()/betterSigma.array() + betterSigma.array()/refSig.array() +2.0))  + .25*(betterMu-betterReference).array().square()/(refSig + betterSigma).array();

				   logLikelihood -= bats.array().square().sum();// + normalizationConstant.array().abs().log().sum();

;


	  }
	   logLikelihood /= numParam;
if( std::isfinite( logLikelihood) && std::isfinite( logPrior)){
	  
	
	  return make_shared<muq::Inference::MCMCState>(state, logLikelihood,
            boost::optional<Eigen::VectorXd>(),
            boost::optional<double>(logPrior),
            boost::optional<Eigen::VectorXd>(),
            boost::optional<Eigen::MatrixXd>(),
//             boost::optional<Eigen::VectorXd>(VectorXd::Constant(1, batdist)));
		  boost::optional<Eigen::VectorXd>());
}
else
{
	cout << "bad constructed state, return null: "<< state.transpose() << endl;
	return nullptr;
	
}
	  
  };

  int numRuns;
  double accuracyFloor;
  VectorXd param;
  shared_ptr<RandomOdeSolver> betterSolver;
  shared_ptr<RandomOdeSolver> worseSolver;
  VectorXd initialCondition;
  double stepSize;
};




class FitzSamplingProblemDeterministic : public muq::Inference::AbstractSamplingProblem{
public:
	FitzSamplingProblemDeterministic(py::list priorMean, py::list priorVar, py::list data, double obsVar, double stepSize, double obsInterval, double finishTime):AbstractSamplingProblem(3, nullptr, nullptr)
	{
				VectorXd priorMeanVec = GetEigenVector<VectorXd>(priorMean);

		VectorXd priorVarVec = GetEigenVector<VectorXd>(priorVar);
		prior = make_shared<GaussianDensity>(priorMeanVec, priorVarVec, GaussianSpecification::DiagonalCovariance);
		VectorXd dataVec = GetEigenVector<VectorXd>(data);
		likelihood = make_shared<GaussianDensity>(dataVec, obsVar);
		dataStd = sqrt(obsVar);
// 		discrepancy = make_shared<GaussianDensity>(dataVec*0.0, obsVar);

		// 		cout << "making with " << stepSize << endl;
		betterSolver = make_shared<EulerOdeSolver>( make_shared<FitzRhs>(), stepSize, finishTime, obsInterval);
		
		initialCondition.resize(2);
		initialCondition(0) = -1;
			initialCondition(1) = 1;
	}
	
	double DensityHelper(py::list state){
	  				VectorXd stateVec = GetEigenVector<VectorXd>(state);
					auto fullState = ConstructState(stateVec);
					return fullState->GetDensity();

	}
	
	
	virtual ~FitzSamplingProblemDeterministic() = default;
	  virtual void                                    InvalidateCaches() override{};
  virtual std::shared_ptr<MCMCState> ConstructState(Eigen::VectorXd const& state, int const currIteration = 0, std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry = nullptr) override {
// 	cout << "constructing " << state.transpose() << endl;
	  double logPrior = prior->LogDensity(state);
	  double logLikelihood =0;
	  

VectorXd forwardModelBetter(betterSolver->outputSize);

// 		  		  	cout << "give model" <<state.head(3)<< endl;


		   forwardModelBetter= betterSolver->Evaluate(initialCondition, state);	  
		   
// 		  	cout << "model says:" << forwardModel.transpose() << endl;
		  logLikelihood = likelihood->LogDensity(forwardModelBetter);
	  

if( std::isfinite( logLikelihood) && std::isfinite( logPrior)){
	  
	
	  return make_shared<muq::Inference::MCMCState>(state, logLikelihood,
            boost::optional<Eigen::VectorXd>(),
            boost::optional<double>(logPrior),
            boost::optional<Eigen::VectorXd>(),
            boost::optional<Eigen::MatrixXd>(),
            boost::optional<Eigen::VectorXd>());
}
else
{
	cout << "bad constructed state, return null: "<< state.transpose() << endl;
	return nullptr;
	
}
	  
  };

  double dataStd;
  shared_ptr<GaussianDensity> prior;
  shared_ptr<GaussianDensity> likelihood;	
  shared_ptr<EulerOdeSolver> betterSolver;
  VectorXd initialCondition;
};





class PseudomarginalMHKernel : public DR{
public:
	PseudomarginalMHKernel(std::shared_ptr<AbstractSamplingProblem> inferenceProblem, boost::property_tree::ptree& properties):
	DR(inferenceProblem, properties){
// 		cout << "Made pseudo kernel!" << endl;
	};
	
	virtual ~PseudomarginalMHKernel() = default;
	
	
  virtual std::shared_ptr<muq::Inference::MCMCState> ConstructNextState(
    std::shared_ptr<muq::Inference::MCMCState>    currentState,
    int const                                     iteration,
    std::shared_ptr<muq::Utilities::HDF5LogEntry> logEntry) override{
		auto revisedCurrent = samplingProblem->ConstructState(currentState->state, iteration, logEntry);
if(revisedCurrent){
	currentState->CloneFrom(revisedCurrent);}
	else{
		cout << "current state revised bad, forcing a move" << endl;
		//our revision was bad, so force it to step away
		auto revised2 =  make_shared<muq::Inference::MCMCState>(currentState->state, 1e-30,
            boost::optional<Eigen::VectorXd>(),
            boost::optional<double>(-std::numeric_limits<double>::infinity()),
            boost::optional<Eigen::VectorXd>(),
            boost::optional<Eigen::MatrixXd>(),
            boost::optional<Eigen::VectorXd>());
		
		currentState->CloneFrom(revised2);
		
		
		
	}
// 	cout << "density " << currentState->GetDensity() << endl;
	 assert(!std::isnan( currentState->GetDensity())) ;
    
	return DR::ConstructNextState(currentState, iteration, logEntry);
	}	
};

REGISTER_MCMCKERNEL(PseudomarginalMHKernel);




BOOST_PYTHON_MODULE(libRandomOdes)
{
  
    boost::python::class_<OdeRhsPiece, std::shared_ptr<LorenzRhs>,
                        boost::python::bases<ModPiece>, boost::noncopyable> exportOdeRhsPiece(
    "OdeRhsPiece", boost::python::no_init);
  boost::python::implicitly_convertible<std::shared_ptr<OdeRhsPiece>, std::shared_ptr<ModPiece> >();
  
  
  
  boost::python::class_<LorenzRhs, std::shared_ptr<LorenzRhs>,
                        boost::python::bases<OdeRhsPiece>, boost::noncopyable> exportLorenzRhs(
    "LorenzRhs",
    boost::python::init<>());
  boost::python::implicitly_convertible<std::shared_ptr<LorenzRhs>, std::shared_ptr<OdeRhsPiece> >();


  boost::python::class_<FitzRhs, std::shared_ptr<FitzRhs>,
                        boost::python::bases<OdeRhsPiece>, boost::noncopyable> exportFitzRhs(
    "FitzRhs",
    boost::python::init<>());
  boost::python::implicitly_convertible<std::shared_ptr<FitzRhs>, std::shared_ptr<OdeRhsPiece> >();
  
  
  
    boost::python::class_<RandomOdeSolver, std::shared_ptr<RandomOdeSolver>,
                        boost::python::bases<ModPiece>, boost::noncopyable> exportRandomOdeSolver(
    "RandomOdeSolver",
    boost::python::init<shared_ptr<OdeRhsPiece> , double , double , double, double >());
  boost::python::implicitly_convertible<std::shared_ptr<RandomOdeSolver>, std::shared_ptr<ModPiece> >();
  
    boost::python::class_<RandomRk4, std::shared_ptr<RandomRk4>,
                        boost::python::bases<ModPiece>, boost::noncopyable> exportRandomRk4(
    "RandomRk4",
    boost::python::init<shared_ptr<OdeRhsPiece> , double , double , double, double >());
  boost::python::implicitly_convertible<std::shared_ptr<RandomRk4>, std::shared_ptr<ModPiece> >();

  
  
    boost::python::class_<RandomRk2, std::shared_ptr<RandomRk2>,
                        boost::python::bases<ModPiece>, boost::noncopyable> exportRandomRk2(
    "RandomRk2",
    boost::python::init<shared_ptr<OdeRhsPiece> , double , double , double, double >());
  boost::python::implicitly_convertible<std::shared_ptr<RandomRk2>, std::shared_ptr<ModPiece> >();

 
  
      boost::python::class_<RandomRk3, std::shared_ptr<RandomRk3>,
                        boost::python::bases<ModPiece>, boost::noncopyable> exportRandomRk3(
    "RandomRk3",
    boost::python::init<shared_ptr<OdeRhsPiece> , double , double , double, double >());
  boost::python::implicitly_convertible<std::shared_ptr<RandomRk3>, std::shared_ptr<ModPiece> >();
  
  
  
     boost::python::class_<EulerOdeSolver, std::shared_ptr<EulerOdeSolver>,
                        boost::python::bases<ModPiece>, boost::noncopyable> exportEulerOdeSolver(
    "EulerOdeSolver",
    boost::python::init<shared_ptr<OdeRhsPiece> , double , double , double >());
  boost::python::implicitly_convertible<std::shared_ptr<EulerOdeSolver>, std::shared_ptr<ModPiece> >();
  
  
  
  
         boost::python::class_<FitzSamplingProblem, std::shared_ptr<FitzSamplingProblem>,
                        boost::noncopyable> exportFitzSamplingProblem(
    "FitzSamplingProblem",
    boost::python::init<py::list , py::list , py::list , double , unsigned , double , double , double, double >());
			    exportFitzSamplingProblem.def("DensityHelper", &FitzSamplingProblem::DensityHelper);

  boost::python::implicitly_convertible<std::shared_ptr<FitzSamplingProblem>, std::shared_ptr<AbstractSamplingProblem> >();
  
           boost::python::class_<FitzSamplingProblemIndicator, std::shared_ptr<FitzSamplingProblemIndicator>,
                        boost::noncopyable> exportFitzSamplingProblemIndicator(
    "FitzSamplingProblemIndicator",
    boost::python::init<py::list , py::list , py::list , double , unsigned , double , double , double >());

  boost::python::implicitly_convertible<std::shared_ptr<FitzSamplingProblemIndicator>, std::shared_ptr<AbstractSamplingProblem> >();
  
  
  
  
  
  
           boost::python::class_<FitzSamplingProblemDeterministic, std::shared_ptr<FitzSamplingProblemDeterministic>,
                        boost::noncopyable> exportFitzSamplingProblemDeterministic(
    "FitzSamplingProblemDeterministic",
    boost::python::init<py::list , py::list , py::list , double , double , double , double >());\
    exportFitzSamplingProblemDeterministic.def("DensityHelper", &FitzSamplingProblemDeterministic::DensityHelper);
  boost::python::implicitly_convertible<std::shared_ptr<FitzSamplingProblemDeterministic>, std::shared_ptr<AbstractSamplingProblem> >();
  
  
  

     boost::python::class_<FitzForwardScaleProblem, std::shared_ptr<FitzForwardScaleProblem>,
                        boost::noncopyable> exportFitzForwardScaleProblem(
    "FitzForwardScaleProblem",
    boost::python::init<double , unsigned , double , double , double >());
  boost::python::implicitly_convertible<std::shared_ptr<FitzForwardScaleProblem>, std::shared_ptr<AbstractSamplingProblem> >();
  
  
}
