# -*- coding: utf-8 -*-
"""
Created on Tue Jan 27 10:16:27 2015

@author: prconrad
"""

import sys

sys.path.append('/Users/patrick/muq_install/lib')
sys.path.append('/Users/patrick/Dropbox/Development/examples/build')
sys.path.append('/Users/patrick/Dropbox/Development/examples/odes/build')
sys.path.append('/Users/patrick/Dropbox/Development/examples')

# sys.path.append('/home/prconrad/local/lib')
# sys.path.append('/home/prconrad/local/muq_external/lib')
# sys.path.append('/media/extradata/Dropbox/Development/examples/odes/build')
# sys.path.append('/media/extradata/Dropbox/Development/examples')


import libmuqUtilities
import libmuqModelling
import libmuqInference
import libmuqApproximation
import libmuqPde
import libRandomOdes
import numpy as np
import matplotlib.pyplot as plt
import plotTools




def plotRandomPathsFwd(stepSize, noise, N=100, color='blue' ,alpha=.3, width=1):
    import plotTools


    stateSize = 2
#    stepSize = .004
    finishTime = 20
    #noiseScale = 5
    #numSteps = finishTime/stepSize+1


    #numRuns = 100;
    obsInterval = .2
    numObsTimes = int(finishTime/obsInterval)
    numObs = numObsTimes*stateSize
    obsTimes = np.arange(obsInterval, finishTime+1E-14, obsInterval).transpose()

    startPoint = [[-1, 1], [.2, .2, 3]]
    

    times = np.arange(0, finishTime+1E-14, stepSize).transpose()

    FitzRhs = libRandomOdes.FitzRhs()
    solver = libRandomOdes.RandomOdeSolver(FitzRhs, stepSize, finishTime, obsInterval, noise)








    for j in range(N):
        onePath = solver.Evaluate(startPoint)

        pathMatrix = np.reshape(np.array(onePath), [stateSize, finishTime/obsInterval],'F')

            
        plt.plot(obsTimes[0:], pathMatrix[0,:],'-',color=plotTools.PlotColor(color),alpha=alpha, linewidth=width)




def plotRandomPathsError(stepSize,refStep, noise, N=100, color='blue' ,alpha=.3, width=1):
    import plotTools


    stateSize = 2
#    stepSize = .004
    finishTime = 20
    #noiseScale = 5
    #numSteps = finishTime/stepSize+1


    #numRuns = 100;
    obsInterval = .2
    numObsTimes = int(finishTime/obsInterval)
    numObs = numObsTimes*stateSize
    obsTimes = np.arange(obsInterval, finishTime+1E-14, obsInterval).transpose()

    startPoint = [[-1, 1], [.2, .2, 3]]


    times = np.arange(0, finishTime+1E-14, stepSize).transpose()

    FitzRhs = libRandomOdes.FitzRhs()
    solver = libRandomOdes.RandomOdeSolver(FitzRhs, stepSize, finishTime, obsInterval, noise)

    solverRef = libRandomOdes.RandomOdeSolver(FitzRhs, refStep, finishTime, obsInterval, 0)







    for j in range(N):
        onePath = solver.Evaluate(startPoint)
        onePathRef = solverRef.Evaluate(startPoint)
        pathMatrix = np.fabs(np.reshape(np.array(onePath), [stateSize, finishTime/obsInterval],'F')-np.reshape(np.array(onePathRef), [stateSize, finishTime/obsInterval],'F'))


        plotLine = plt.plot(obsTimes[0:], pathMatrix[0,:],'-',color=plotTools.PlotColor(color),alpha=alpha, linewidth=width)
        plt.gca().set_yscale('log')





    return plotLine
            
def plotReferencePathFwd( ):
    import plotTools


    stateSize = 2
#    stepSize = .004
    finishTime = 20
    #noiseScale = 5
    #numSteps = finishTime/stepSize+1


    #numRuns = 100;
    obsInterval = .2
    numObsTimes = int(finishTime/obsInterval)
    numObs = numObsTimes*stateSize
    obsTimes = np.arange(obsInterval, finishTime+1E-14, obsInterval).transpose()

    startPoint = [[-1, 1], [.2, .2, 3]]
    stepSize = .005

    times = np.arange(0, finishTime+1E-14, stepSize).transpose()

    FitzRhs = libRandomOdes.FitzRhs()
    print 'make ref'
    detsolver = libRandomOdes.RandomRk4(FitzRhs, stepSize, finishTime, obsInterval, 0)
    print 'make ref...done'








    onePath = detsolver.Evaluate(startPoint)

    pathMatrix = np.reshape(np.array(onePath), [stateSize, finishTime/obsInterval],'F')

            
    plt.plot(obsTimes[0:], pathMatrix[0,:],'-',color=plotTools.PlotColor('red'),alpha=1)

def plotNoisefreePathInf(stepSize, plotcolor):
    import plotTools


    stateSize = 2
#    stepSize = .004
    finishTime = 40
    #noiseScale = 5
    #numSteps = finishTime/stepSize+1


    #numRuns = 100;
    obsInterval = .2
    numObsTimes = int(finishTime/obsInterval)
    numObs = numObsTimes*stateSize
    obsTimes = np.arange(obsInterval, finishTime+1E-10, obsInterval).transpose()

    startPoint = [[-1, 1], [.2, .2, 3]]

    times = np.arange(0, finishTime+1E-14, stepSize).transpose()

    FitzRhs = libRandomOdes.FitzRhs()
    print 'make ref'
    detsolver = libRandomOdes.RandomOdeSolver(FitzRhs, stepSize, finishTime+1e-10, obsInterval, 0)
    print 'make ref...done'








    onePath = detsolver.Evaluate(startPoint)

    pathMatrix = np.reshape(np.array(onePath), [stateSize, finishTime/obsInterval],'F')
    print obsTimes.shape, pathMatrix.shape
    for i in range(stateSize):
        plt.subplot(stateSize,1,i+1)
        plt.plot(obsTimes[0:], pathMatrix[i,:],'-',color=plotcolor,alpha=1)              

def plotStepSurvey( presentationVersion, saveToFile ):
    
    if presentationVersion:
        fig = plotTools.MakeFigure(600,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(425,.9, presentationVersion)

    steps = [.1, .05, .01, .005,  .001, .0005]
    for i in range(len(steps)):
        plt.subplot(2,3,i+1)
        plotRandomPathsFwd(steps[i], 1)
        plotReferencePathFwd()
        plt.xticks([0,5,10,15,20])
        plt.yticks([-4,-2,0,2,4])
        if steps[i] < .001:
            plt.title('$h=' + str(steps[i]*1000) + '\\times 10^{-3}$' )
        else:
            plt.title('$h=' + str(steps[i]) + '$' )
        if i+1==4 or i+1 == 5 or i+1==6:
            plt.xlabel('$t$')
        else:
#            plt.xlim([0,20])
            plt.gca().set_xticklabels([])

#            plt.gca().xaxis.set_visible(False)
        if i+1==1 or i +1== 4 or i+1==7:
            plt.ylabel('$R$')
        else:
#            plt.ylim([-4,4])
            plt.gca().set_yticklabels([])
#            plt.gca().yaxis.set_visible(False)

    fig.tight_layout(pad=0.2)
    fig.subplots_adjust(wspace=.15)


    if saveToFile:
        if presentationVersion:
            plt.savefig('plots/fitz_step_survey_presentation.pdf', format='pdf')
        else:
            plt.savefig('plots/fitz_step_survey_paper.pdf', format='pdf')
#        plt.close(fig)
    
    

def plotNoiseSurvey( presentationVersion, saveToFile ):
    
    if presentationVersion:
        fig = plotTools.MakeFigure(600,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(425,.9, presentationVersion)

    if presentationVersion:
        sigmas = [2,1,.5,.2,.1,.05]
    else:
        sigmas = [1,.5,.2,.1,.05,.02, .01, .005, .002]
    for i in range(len(sigmas)):

        if presentationVersion:
            plt.subplot(2,3,i+1)
        else:
            plt.subplot(3,3,i+1)
        plotRandomPathsFwd(.1, sigmas[i])
        plotReferencePathFwd()
        plt.xticks([0,5,10,15,20])
        plt.yticks([-4,-2,0,2,4])
        if sigmas[i] < .001:
            plt.title('$\\sigma=' + str(sigmas[i]*1000) + '\\times 10^{-3}$' )
        else:
            plt.title('$\\sigma=' + str(sigmas[i]) + '$' )
        if i+1==4 or i+1 == 5 or i+1==6:
            plt.xlabel('$t$')
        else:
#            plt.xlim([0,20])
            plt.gca().set_xticklabels([])

#            plt.gca().xaxis.set_visible(False)
        if i+1==1 or i +1== 4 or i+1==7:
            plt.ylabel('$R$')
        else:
#            plt.ylim([-4,4])
            plt.gca().set_yticklabels([])
#            plt.gca().yaxis.set_visible(False)

    fig.tight_layout(pad=0.2)
    fig.subplots_adjust(wspace=.15)


    if saveToFile:
        if presentationVersion:
            plt.savefig('plots/fitz_noise_survey_presentation.pdf', format='pdf')
        else:
            plt.savefig('plots/fitz_noise_survey_paper.pdf', format='pdf')
#        plt.close(fig)    


def plotForwardPredictive(stepInt , presentationVersion, saveToFile , N, useLegend=False):
    import h5py
    import CompareIncrementalCovariances_2
    hdf5filename = 'results/fitz_fwd.h5_rank_' + str(stepInt-1)

    f = h5py.File(hdf5filename,"r")

    if stepInt == 1:
        stepSize = .1
    elif stepInt == 2:
        stepSize = .05
    elif stepInt == 3:
        stepSize = .02
    elif stepInt == 4:
        stepSize = .01
    elif stepInt == 5:
        stepSize = .005


    # chain = np.array(CompareIncrementalCovariances_2.ReferenceChain(hdf5filename,'Fitz'))[:,:]


    # if presentationVersion:
    #     fig = plotTools.MakeFigure(600,.7, presentationVersion)
    # else:
    #     fig = plotTools.MakeFigure(425,.9, presentationVersion)

    # randomsubset = np.random.choice(chain[0,:], size=[N])
    # for i in range(N):


    blueLine= plotRandomPathsError(stepSize,stepSize, pow(10.0, -.35), N=N, color='medium blue', width=.5 , alpha=.5)
        # plotRandomPathsFwd(2*stepSize, pow(10.0, randomsubset[i]), N=1, color='purple' )

    # plotRandomPathsFwd(stepSize, 0.0, N=1, color='dark blue', alpha=1, width=2 )
    redLine= plotRandomPathsError(2*stepSize,stepSize,0.0, N=1, color='red', alpha=1, width=2 )

    # plotReferencePathFwd()
#
#         # plotRandomPathsFwd(.1, sigmas[i])
#         # plotReferencePathFwd()
#         # plt.xticks([0,5,10,15,20])
#         # plt.yticks([-4,-2,0,2,4])
#         if sigmas[i] < .001:
#             plt.title('$\\sigma=' + str(sigmas[i]*1000) + '\\times 10^{-3}$' )
#         else:
#             plt.title('$\\sigma=' + str(sigmas[i]) + '$' )
#         if i+1==7 or i+1 == 8 or i+1==9:
#             plt.xlabel('$t$')
#         else:
# #            plt.xlim([0,20])
#             plt.gca().set_xticklabels([])
#
# #            plt.gca().xaxis.set_visible(False)
#         if i+1==1 or i +1== 4 or i+1==7:
#             plt.ylabel('$R$')
#         else:
# #            plt.ylim([-4,4])
#             plt.gca().set_yticklabels([])
# #            plt.gca().yaxis.set_visible(False)
#
    # fig.tight_layout(pad=0.2)
    # plt.xlabel('$V$')


#     fig.subplots_adjust(wspace=.15)
#
#
    # if saveToFile:
    #     if presentationVersion:
    #         plt.savefig('plots/fitz_fwdPredictive' + str(stepInt) + '.pdf', format='pdf')
    #     else:
    #         plt.savefig('plots/fitz_noise_survey_paper.pdf', format='pdf')
       # plt.close(fig)


def plotForwardPredictiveSummary(presentationVersion, saveToFile , N):
    if presentationVersion:
        fig = plotTools.MakeFigure(800,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(500,.9, presentationVersion)



    ax=plt.subplot(3,1,1)
    plotForwardPredictive(1, True, False, N,True)
    plt.title("$h=.1$")
    ax.xaxis.set_visible(False)

    import matplotlib
    blueproxy =plt.Line2D([],[],color=plotTools.PlotColor('medium blue'),alpha=1, linewidth=2,label='Random displacement')
    redproxy = plt.Line2D([],[],color=plotTools.PlotColor('red'),alpha=1, linewidth=2,label='Error indicator')
    plt.legend( [blueproxy, redproxy],['Random','Indicator'] )


    # plt.subplot(5,1,2)
    # plotForwardPredictive(2, True, False, N)
    ax=plt.subplot(3,1,2)
    plotForwardPredictive(3, True, False, N)
    plt.title("$h=.02$")
    ax.xaxis.set_visible(False)

    # plt.subplot(5,1,4)
    # plotForwardPredictive(4, True, False, N)
    plt.subplot(3,1,3)
    plotForwardPredictive(5, True, False, N)
    plt.title("$h=.005$")

    plt.xlabel('$t$')


    fig.tight_layout(pad=0.2)



def plotDataAndAccuracy( presentationVersion, saveToFile ):
    import plotTools




    stateSize = 2
#    stepSize = .004
    finishTime = 40
    #noiseScale = 5
    #numSteps = finishTime/stepSize+1


    #numRuns = 100;
    obsInterval = 1
    numObsTimes = int(finishTime/obsInterval)
    numObs = numObsTimes*stateSize
    obsTimes = np.arange(obsInterval, finishTime+1E-14, obsInterval).transpose()


    allData = np.loadtxt('FitzDataRk4_fine.dat')

#    print np.reshape(allData, [stateSize, 40],'F')
    data = np.reshape(allData, [stateSize, finishTime/obsInterval],'F')



    steps = [.0005]
    
    if presentationVersion:
        fig = plotTools.MakeFigure(800,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(600,.9, presentationVersion)        
        
    plt.subplot(stateSize,1,1)
    
    for i in range(len(steps)):
        plotNoisefreePathInf(steps[i], plotTools.ColormapBaseColor(i))

    for i in range(stateSize):
        plt.subplot(stateSize, 1, i+1)
        plt.plot(obsTimes, data[i,:],'o',color=plotTools.PlotColor('medium-dark red'),alpha=.8)

    # if not newfig:
    #     plt.legend(['Data', 'True', 'MAP'],loc='best')

    plt.subplot(stateSize,1,1)
    plt.xlim([0, 40])
    plt.ylabel('$R$')
    plt.gca().xaxis.set_visible(False)
    plt.subplot(stateSize,1,2)
    plt.xlim([0, 40])
    plt.ylim([-1.5, 1.5])
    plt.ylabel('$V$')
    plt.xlabel('$t$')
    pltlegend=[]
    # for step in steps:
    #     pltlegend.append('$h=' + str(step) + '$')
    pltlegend.append('$u(t)$')
    pltlegend.append('Data')
    plt.legend(pltlegend,loc='center left')

    if saveToFile:
        if presentationVersion:
            plt.savefig('plots/fitz_data_presentation.pdf', format='pdf')
        else:
            plt.savefig('plots/fitz_data_paper.pdf', format='pdf')






def PlotStepSequence(presentationVersion, saveToFile, subsample=100):
    #hdf5File = 'results/' + exampleName + '.h5'
    import CompareIncrementalCovariances_2
    all_data = [np.array(CompareIncrementalCovariances_2.ReferenceChain('results/fitz_paper_fwd.h5_rank_0',''))[:,::],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/fitz_paper_fwd.h5_rank_1',''))[:,::],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/fitz_paper_fwd.h5_rank_2',''))[:,::],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/fitz_paper_fwd.h5_rank_3',''))[:,::],
                np.array(CompareIncrementalCovariances_2.ReferenceChain('results/fitz_paper_fwd.h5_rank_4',''))[:,::]
                ]
    referenceName = 'fitz_fwd'

    import kde_matrix
    labels = []

    boundaries_list = []
    contour_labels = ['h=.1','h=.05', 'h=.02', 'h=.01', 'h=.005']

    for i in range( all_data[0].shape[0]):
        labels.append('')


    label_separation_x = 0.1

    if presentationVersion:
       fig = plotTools.MakeFigure(600,.7, presentationVersion)
    else:
        fig = plotTools.MakeFigure(600,.9, presentationVersion)
    kde_matrix.kde_matrix(all_data, labels=labels, ax=plt.gca(), label_separation_x=label_separation_x,contour_labels=contour_labels,
                          boundaries_list=boundaries_list,kde_subsample=10, scatter_subsample=10, usescatter=False)

#    fig.tight_layout(pad=0.1)


    if saveToFile:
        if presentationVersion:
            plt.savefig('plots/fitz_fwdScalePosterior.pdf', format='pdf')
        else:
            plt.savefig('plots/' + referenceName + '_perfect_density_paper.pdf', format='pdf')
#        plt.close(fig)



# plotStepSurvey(True, True)
#plotStepSurvey(False, True)
# plotNoiseSurvey(True, True)
#plotNoiseSurvey(False, True)

# PlotStepSequence(True, False, 1)
# plt.savefig('plots/fitz_noise_density.pdf', format='pdf')

# plotDataAndAccuracy(True, True)
# plotDataAndAccuracy(False, True)


# plotForwardPredictive(1, True, False, 100)
# plotForwardPredictive(2, True, False, 100)
# plotForwardPredictive(3, True, False, 100)
# plotForwardPredictive(4, True, False, 100)
# plotForwardPredictive(5, True, False, 100)

# plotForwardPredictiveSummary(True, False, 50)
# plt.savefig('plots/fitz_noise_summary_paper.pdf', format='pdf')
plotForwardPredictiveSummary(True, True, 50)
plt.savefig('plots/fitz_noise_summary_presentation.pdf', format='pdf')

plt.show()