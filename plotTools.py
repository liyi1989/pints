# -*- coding: utf-8 -*-
"""
Created on Wed Jun 18 16:56:40 2014

@author: prconrad
"""

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rcParams

def MakeFigure(totalWidthPts, fraction, presentationVersion = False, height_ratio=0.61803):
    fig_width_pt  = totalWidthPts * fraction
    
    inches_per_pt = 1.0 / 72.27
    # golden_ratio  = (np.sqrt(5) - 1.0) / 2.0  # because it looks good
    
    fig_width_in  = fig_width_pt * inches_per_pt  # figure width in inches
    fig_height_in = fig_width_in * height_ratio   # figure height in inches
    fig_dims      = [fig_width_in, fig_height_in] # fig dims as a list
    fig = plt.figure(figsize = fig_dims)
    
    greyColor = PlotColor("grey")
    whiteColor = PlotColor("white")
    if not presentationVersion:
        rcParams['axes.labelsize'] = 9
        rcParams['xtick.labelsize'] = 9
        rcParams['ytick.labelsize'] = 9
        rcParams['legend.fontsize'] = 9
    else:
        rcParams['axes.labelsize'] = 12
        rcParams['xtick.labelsize'] = 12
        rcParams['ytick.labelsize'] = 12
        rcParams['legend.fontsize'] = 12
    rcParams['axes.edgecolor'] = greyColor
    rcParams['axes.facecolor'] = whiteColor
    rcParams['figure.facecolor'] = whiteColor
    rcParams['axes.labelcolor'] = greyColor
    rcParams['text.color'] = greyColor
    rcParams['xtick.color'] = greyColor
    rcParams['ytick.color'] = greyColor
    
    rcParams['lines.antialiased'] = True
    if not presentationVersion:
        rcParams['font.family'] = 'serif'
        rcParams['font.serif'] = ['Computer Modern Roman']
        rcParams['text.usetex'] = True
#        rcParams['axes.formatter.use_mathtext'] = True
    else:
        rcParams['text.usetex'] = False          
        rcParams['font.family'] = 'sans-serif'
#        rcParams['font.sans-serif'] = ['Helvetica']
        rcParams['mathtext.fontset'] = 'stixsans'
#        rcParams['mathtext.fallback_to_cm'] = False
        rcParams['lines.linewidth'] = 1.5
#        rcParams['axes.formatter.use_mathtext'] = True
        
    #rcParams['backend'] = 'pdf'
        
    return fig
    
    
def PlotColor(name):
    allColors = dict()
    allColors['red'] = '#e41a1c'
    allColors['blue'] = '#377eb8'
    allColors['green'] = '#4daf4a'
    allColors['purple'] = '#984ea3'
    allColors['orange'] = '#ff7f00'
    allColors['grey'] = '#808080'
    allColors['white'] = '#ffffff'
    allColors['light blue'] = '#3CA0D0'
    allColors['medium blue'] = '#086FA1'
    allColors['dark blue'] = '#030769'
    
    allColors['light purple'] = '#E276FE'
    allColors['medium purple'] = '#BD63D4'
    allColors['dark purple'] = '#8805A8'

    allColors['light red'] = '#fc9272'
    allColors['medium red'] = '#cb181d'
    allColors['dark red'] = '#67000d'


    allColors['blue1'] =     '#9ecae1'
    allColors['blue2'] = '#6baed6'
    allColors['blue3'] = '#4292c6'
    allColors['blue4'] = '#2171b5'
    allColors['blue5'] = '#084594'

    allColors['purple1'] =     '#bcbddc'
    allColors['purple2'] =     '#9e9ac8'
    allColors['purple3'] =     '#807dba'
    allColors['purple4'] =     '#6a51a3'
    allColors['purple5'] =     '#4a1486'
    
    allColors['medium-dark red'] = '#a50f15'

    return allColors[name]

def Colormap(number):
    colorMaps = dict()
    import matplotlib
    colorMaps[0] = matplotlib.colors.LinearSegmentedColormap.from_list('mine',
                                                                                 [PlotColor('light blue'),
                                                                                  PlotColor('dark blue')],N=256)
    colorMaps[1] = matplotlib.colors.LinearSegmentedColormap.from_list('mine',
                                                                                 ['#9e9ac8',
                                                                                  '#3f007d'],N=256)
    colorMaps[2] = matplotlib.colors.LinearSegmentedColormap.from_list('mine',
                                                                                 ['#74c476',
                                                                                  '#00441b'],N=256)
    colorMaps[3] = matplotlib.colors.LinearSegmentedColormap.from_list('mine',
                                                                                 ['#fdae6b',
                                                                                  '#7f2704'],N=256)
    colorMaps[4] = matplotlib.colors.LinearSegmentedColormap.from_list('mine',
                                                                                 ['#ef3b2c',
                                                                                  '#67000d'],N=256)
    return colorMaps[number]

def ColormapBaseColor(number):
    colorMaps = dict()
    colorMaps[0] = '#08519c'
    colorMaps[1] = '#54278f'
    colorMaps[2] = '#006d2c'
    colorMaps[3] = '#d94801'
    colorMaps[4] = '#cb181d'

    # return PlotColor(colorMaps[number])
    return colorMaps[number]