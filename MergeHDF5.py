# -*- coding: utf-8 -*-
"""
Created on Mon Jun 16 22:16:53 2014

@author: prconrad
"""

import h5py

def MergeFiles(rootname, totalNumber):
    print rootname
    rootfile = h5py.File(rootname,'w')
    
    for i in range(totalNumber):
        print rootname+"_rank_" + str(i)
        otherfilename = rootname+"_rank_" + str(i)
        otherFile = h5py.File(otherfilename,"r")
        for group in otherFile.iterkeys():
            print group
#            rootfile[group] = h5py.ExternalLink(otherfilename, group)
#            rootfile[group] = otherFile[group]
            otherFile.copy(group, rootfile)
    rootfile.close()
#        for group in f.iterkeys():
 #           h5py.
 #           rootFile.
        
        
#MergeFiles('genetics_newcv.h5', 30)    
#MergeFiles('elliptic_newcv.h5', 30)    
# MergeFiles('genetics_regression_looser.h5', 4)
# MergeFiles('../odes/results/lorenz.h5', 4)
MergeFiles('../pdes/results/elliptic1d_paper_inv.h5', 5)